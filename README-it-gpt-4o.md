[[_TOC_]]

# StoryPixAI : Storie della buonanotte per bambini generate e illustrate dall'IA

StoryPixAI è un progetto serverless innovativo di generazione di storie della buonanotte magiche, create dall'IA. Basta scrivere alcune parole sul tema di loro scelta e lasciare che l'intelligenza artificiale tessi una storia della buonanotte unica e avvincente, accompagnata da un'adorabile illustrazione. Il tutto è generato automaticamente nella lingua di vostra scelta grazie ai modelli di IA avanzati di OpenAI e Amazon Bedrock. Offrite ai vostri bambini un momento magico prima di addormentarsi, godendo di un'esperienza utente fluida grazie alla potenza dell'infrastruttura AWS serverless.

## Funzionalità

* **Storie della buonanotte personalizzate create dall'IA:** Trasformate le idee dei vostri bambini in storie della buonanotte originali, grazie alla potenza dei modelli di IA di OpenAI e AWS Bedrock.
* **Illustrazioni adorabili generate dall'IA:** Ogni storia prende vita con immagini carine e colorate, perfette per stimolare l'immaginazione dei vostri bambini.
* **Salvataggio automatico su cloud AWS:** StoryPixAI salva automaticamente le vostre storie e illustrazioni su bucket S3, accessibili tramite un link unico per rileggerle in qualsiasi momento.
* **Accesso sicuro al generatore:** L'interfaccia di creazione delle storie è protetta da un'autenticazione, garantendo così uno spazio di creazione privato e sicuro.
* **Multilingue:** StoryPixAI supporta le seguenti lingue:
    * Francese (principalmente testato)
    * Inglese
    * Spagnolo
    * Tedesco
    * Italiano
    * Portoghese

# Architettura

StoryPixAI si basa su un'architettura serverless:

## Schema di architettura dell'infrastruttura senza server
![alt text](/img/storypixai-architecture.png)

## Frontend

* **Interfaccia di generazione (`index.html`):** L'interfaccia utente di StoryPixAI è una Single Page Application (SPA) sviluppata in HTML, CSS e JavaScript puro (Vanilla JavaScript). Offre un'esperienza utente intuitiva e interattiva per:
    ![alt text](/img/generateur.png)

    * **Autenticazione:** Gli utenti devono accedere con le loro credenziali Cognito per accedere alle funzionalità di generazione. Una finestra modale di accesso è fornita per facilitare questo processo.

    * **Selezione della lingua:** Un menu a tendina consente di scegliere la lingua del generatore e della storia tra quelle supportate: francese, inglese, spagnolo, tedesco, italiano e portoghese. La lingua selezionata è utilizzata per l'interfaccia utente e per guidare la generazione della storia.
        ![alt text](/img/langues.png)
    * **Inserimento del prompt:** Un campo di testo permette all'utente di inserire l'idea o il tema della storia desiderata.

    * **Personalizzazione della generazione (opzionale):** L'interfaccia propone opzioni per personalizzare la generazione:
        * **Scelta del modello di generazione del testo:** OpenAI GPT-4-o, Anthropic Claude 3, o Mistral AI.
            ![alt text](/img/texte_modele.png)
        * **Scelta del modello di generazione dell'immagine:** OpenAI DALL-E 3 o Stable Diffusion XL.
            ![alt text](/img/image_modele.png)
        * **Parametri specifici per il modello di immagine:** Dimensioni, stile (per Stable Diffusion XL) e qualità (per DALL-E 3).
            ![alt text](/img/styles.png)
        * **Seed:** Un campo numerico permette di specificare un valore per inizializzare il generatore casuale.
        * **Attivazione/disattivazione della generazione dell'immagine.**

    * **Avvio della generazione:** Un pulsante "Genera" avvia il processo di generazione della storia e dell'immagine (se attivata).

    * **Monitoraggio del progresso:** Un messaggio di stato informa l'utente del progresso della generazione ("Elaborazione in corso...", "Pronto!", ecc.) e di eventuali errori.

    * **Visualizzazione del risultato:** Un link unico verso la storia e l'immagine generate viene visualizzato una volta completata la generazione. Questo link può essere copiato e condiviso. * **Gestione dell'autenticazione e delle richieste:**
    * Lo script `storypixai.js` gestisce l'autenticazione dell'utente con Amazon Cognito e utilizza il token di accesso ottenuto per autorizzare le richieste all'API Gateway.
    * Gestisce anche la comunicazione con gli endpoint dell'API Gateway (`/generate` e `/check_status/{task_id}`) per avviare la generazione e monitorare i progressi.
    * Sono implementati meccanismi di gestione degli errori e di nuovi tentativi per garantire l'affidabilità dell'applicazione.

* **Hosting e distribuzione:**
    * L'interfaccia di generazione (`index.html`) è ospitata su Amazon S3 e distribuita tramite Amazon CloudFront, una rete di distribuzione di contenuti (CDN) che migliora le prestazioni e la disponibilità dell'applicazione per gli utenti di tutto il mondo.



## Backend

* **Funzioni AWS Lambda (Python):**
    * **`StoryPixAI`:** Questa funzione è il cuore dell'applicazione. Riceve un prompt testuale fornito dall'utente tramite l'interfaccia di generazione e orchestra il processo di creazione della storia e dell'illustrazione. Chiama successivamente le API di OpenAI e/o Bedrock per generare il testo della storia e l'immagine associata. Successivamente, memorizza l'immagine nel bucket S3 e restituisce all'utente un link unico alla storia e all'immagine generate. Lo stato di avanzamento della generazione è archiviato in DynamoDB.

    * **`status_checker`:** Questa funzione è chiamata periodicamente dall'interfaccia di generazione per verificare lo stato di avanzamento della creazione della storia e dell'immagine. Interroga DynamoDB per recuperare lo stato del compito in corso e restituisce questo stato al frontend. Ciò consente all'interfaccia di generazione di tenere informato l'utente sui progressi e di indicargli quando la storia e l'immagine sono pronte.

* **Amazon DynamoDB:** Questo database NoSQL key-value è utilizzato per memorizzare le informazioni relative ai compiti di generazione delle storie e delle immagini. Ogni compito è identificato da un `requestId` univoco. La tabella DynamoDB contiene i seguenti attributi:
    * **`requestId` (chiave di hash):** Identificativo unico del compito.
    * **`status`:** Stato del compito (in corso, completato, errore).
    * **`resultUrl`:** URL della storia generata (quando il compito è completato).

    La tabella DynamoDB possiede anche due indici secondari globali:

    * **`StatusIndex`:** Permette di cercare i compiti in base al loro stato.
    * **`ResultUrlIndex`:** Permette di cercare i compiti in base all'URL della storia generata.

    La funzione Lambda `status_checker` utilizza questi indici per recuperare efficacemente lo stato e l'URL della storia di un dato compito. Lo script JavaScript del frontend interroga successivamente l'endpoint `/check_status/{task_id}` dell'API Gateway per ottenere queste informazioni e aggiornare l'interfaccia utente di conseguenza.

* **Amazon S3:** Questo servizio di archiviazione oggetti ospita l'interfaccia web statica (`index.html`), le immagini generate e le storie generate. Le storie e le immagini sono memorizzate con chiavi uniche generate dalla funzione `StoryPixAI`, e queste chiavi sono restituite all'utente per consentirgli di accedere alle sue creazioni.

* **Amazon CloudWatch:** Questo servizio di monitoraggio raccoglie i log e le metriche delle funzioni Lambda, permettendo di monitorare le prestazioni dell'applicazione, rilevare errori e risolvere i problemi.

# Infrastructure as Code (IaC)

* **Terraform:** L'intera infrastruttura AWS (Lambda, API Gateway, S3, CloudFront, Cognito, DynamoD, ...) è definita e distribuita utilizzando Terraform. Questo consente una gestione automatizzata, riproducibile e versionata dell'infrastruttura, facilitando aggiornamenti e modifiche.

# Sicurezza e autenticazione

* **Amazon Cognito:** Questo servizio gestisce l'autenticazione dell'utente. L'interfaccia di generazione (`index.html`) utilizza l'API Cognito per autenticare l'utente e ottenere un token di accesso. Questo token viene quindi incluso nelle richieste agli endpoint `/generate` e `/check_status/{task_id}` per autorizzare l'accesso rispettivamente alle funzioni `StoryPixAI` e `status_checker`.

# Distribuzione dei contenuti

* **Amazon CloudFront:** Questo servizio di rete di distribuzione di contenuti (CDN) è utilizzato per distribuire l'interfaccia web statica (`index.html`) in modo rapido e affidabile agli utenti di tutto il mondo. Memorizza nella cache i contenuti sui server distribuiti geograficamente, riducendo così la latenza e migliorando l'esperienza dell'utente.

# Modelli di IA

StoryPixAI è compatibile con una combinazione di modelli di IA all'avanguardia per generare storie e illustrazioni di alta qualità:

* **Modelli OpenAI:**
    * **Generazione di storie:**
        * OpenAI GPT-4-o
    * **Generazione di immagini:**
        * OpenAI DALL-E 3

* **Modelli accessibili tramite AWS Bedrock:**
    * **Generazione di storie:**
        * Anthropic Claude 3 Sonnet (20240229-v1)
        * Mistral AI Mistral Large (2402-v1)
    * **Generazione di immagini:**
        * StabilityAI Stable Diffusion XL v1


# Prerequisiti

Prima di iniziare a utilizzare StoryPixAI, assicurati di avere i seguenti elementi:

* **Account AWS:** Avrai bisogno di un account AWS attivo per distribuire e gestire l'infrastruttura serverless. Assicurati che il tuo utente IAM disponga delle seguenti autorizzazioni (o equivalenti). **Nota che questa policy è un esempio e può essere adattata alle tue esigenze specifiche in materia di sicurezza:**

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "cloudwatch:*",
            "ec2:*",
            "lambda:*",
            "s3:*",
            "ssm:*",
            "logs:*",
            "dynamodb:*",
            "cognito-idp:*",
            "cognito-identity:*",
            "apigateway:*",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:AttachRolePolicy",
            "iam:DetachRolePolicy",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:CreatePolicy",
            "iam:DeletePolicy",
            "iam:PassRole",
            "iam:Tag*",
            "iam:Get*",
            "iam:List*",
            "iam:TagRole",
            "kms:TagResource",
            "iam:CreateServiceLinkedRole",
            "budgets:*",
            "events:*"
          ],
          "Resource": "*"
        }
      ]
    }
    ```

* **Abilitazione dei modelli Bedrock:** Assicurati che i seguenti modelli di IA siano abilitati nel tuo account AWS Bedrock:
    * Anthropic Claude 3 Sonnet (20240229-v1)
    * Mistral AI Mistral Large (2402-v1)
    * StabilityAI Stable Diffusion XL v1

[Accesso ai modelli Bedrock - us-east-1 ](https://us-east-1.console.aws.amazon.com/bedrock/home?region=us-east-1#/modelaccess)
![alt text](/img/modele_1.png)
![alt text](/img/modele_2.png)


* **Chiavi API OpenAI (se intendi utilizzare i modelli OpenAI)** Per generare illustrazioni di alta qualità con DALL-E 3, è fortemente raccomandato disporre di una chiave API OpenAI valida. Puoi ottenere la tua chiave dal sito di OpenAI. Nota che StoryPixAI può anche generare immagini con Stable Diffusion XL v1 (tramite AWS Bedrock), ma DALL-E 3 offre generalmente risultati superiori.

**Per distribuire tramite GitLab CI/CD:**

* **Account GitLab:** Avrai bisogno di un account GitLab valido e di accesso al progetto StoryPixAI su GitLab.
* **Variabili di ambiente GitLab:** Dovrai configurare le variabili di ambiente necessarie nel tuo progetto GitLab, compresi le chiavi API AWS e OpenAI (se utilizzi DALL-E 3).

# Installazione e distribuzione

## Distribuzione tramite GitLab CI/CD (Raccomandato)

StoryPixAI è progettato per essere facilmente distribuito e aggiornato grazie a una pipeline GitLab CI/CD integrata. Questo metodo di distribuzione è **fortemente raccomandato**, poiché è automatizzato, affidabile e indipendente dal sistema operativo (Linux, macOS, Windows). La pipeline automatizza le fasi di preparazione dell'ambiente, verifica della configurazione, distribuzione e rimozione dell'infrastruttura.

**Esecuzione della pipeline:**

1. **Forkare il progetto:** Creare una copia del progetto nel proprio spazio GitLab andando all'URL seguente: [https://gitlab.com/jls42/storypixai](https://gitlab.com/jls42/storypixai)
2. **Clonare il repository:**
   ```bash
   git clone https://gitlab.com/tuo_utente/storypixai.git # Sostituisci "tuo_utente" con il tuo nome utente GitLab
   cd storypixai
   ```
3. **Configurare le variabili di ambiente:**
   - Vai nelle impostazioni CI/CD del tuo progetto.
   - Definisci le seguenti variabili:
     * `AWS_ACCESS_KEY_ID`: La tua chiave di accesso AWS.
     * `AWS_SECRET_ACCESS_KEY`: La tua chiave segreta AWS.
     * `OPENAI_KEY` (consigliato): La tua chiave API OpenAI (se usi DALL-E 3 o GPT4-o).
     * `TF_VAR_cognito_auth`: Una variabile contenente la configurazione di Cognito in formato JSON, ad esempio:
       ```json
       {"user": "tuo_nome_utente", "password": "tua_password_sicura"}
       ```

4. **Personalizzare lo script `export.sh`:**
   * Apri il file `export.sh` e modifica le seguenti variabili per adattarle alla tua configurazione:
      ```bash
      # il bucket sarà creato automaticamente se disponibile, scegli un nome unico
      BUCKET_STATE="nome_tuo_bucket_per_stato_terraform"
      AWS_DEFAULT_REGION="tua_regione_aws"
      ```

5. **Configurare Terraform:**
   * Crea un file `terraform.tfvars` nella radice della directory `terraform` e definisci le variabili specifiche del tuo ambiente, tra cui (vedi la sezione Documentazione Terraform-docs più in basso per maggiori dettagli):
      ```
      bucket_name = "nome_tuo_bucket_s3"
      ```

6. **Attivare la pipeline:**
   - Vai nella sezione CI/CD del tuo progetto GitLab.
   - Scegli la fase che vuoi eseguire (ad esempio, `Verifica Terraform` o `Distribuzione Terraform`).
   - Clicca sul pulsante "Play" per avviare la fase selezionata.   
    ![alt text](/img/cicd.png)

**Nota:** Se usi la CI/CD fornita con il progetto, l'installazione di Terraform non è necessaria. La distribuzione avverrà automaticamente tramite GitLab CI/CD dopo aver configurato le variabili di ambiente richieste.

## Installazione su un sistema Linux (Ubuntu 22.04) (Opzionale)

Se preferisci distribuire StoryPixAI manualmente, puoi farlo su un sistema Linux. **Nota che questo metodo è stato testato solo su Ubuntu 22.04.** Tuttavia, si consiglia di utilizzare il metodo di distribuzione tramite GitLab CI/CD, poiché è più semplice e non richiede l'installazione locale di Terraform.

1. **Clonare il repository:**

   ```bash
   git clone https://gitlab.com/jls42/storypixai.git
   cd storypixai
   ```

2. **Configurare le variabili di ambiente:**

   * **AWS:**
        * Esegui `aws configure` e segui le istruzioni per configurare le tue credenziali AWS (chiavi di accesso e chiave segreta) e la regione predefinita.
   * **OpenAI (Opzionale):**
        * Esegui `source export.sh` per caricare le funzioni dello script.
        * Esegui `manage_openai_key put <tua_chiave_openai>` per memorizzare la tua chiave OpenAI in SSM Parameter Store.

## Distribuzione tramite Terraform

1. **Preparare l'ambiente AWS:**
    * Segui gli stessi passaggi della configurazione delle variabili di ambiente AWS sopra.

2. **Personalizzare lo script `export.sh`:**
   * Apri il file `export.sh` e modifica le seguenti variabili per adattarle alla tua configurazione:
      ```bash
      # il bucket sarà creato automaticamente se disponibile, scegli un nome unico
      BUCKET_STATE="nome_tuo_bucket_per_stato_terraform"
      AWS_DEFAULT_REGION="tua_regione_aws"
      ```

3. **Configurare Terraform:**
   * Create un file `terraform.tfvars` alla radice della directory `terraform` e definite le variabili specifiche del vostro ambiente, in particolare (vedere la sezione Documentazione Terraform-docs più in basso per ulteriori dettagli):
      ```
      bucket_name = "vostro_nome_del_bucket_s3"
      cognito_auth = {
        user     = "vostro_nome_utente"
        password = "vostra_password_sicura"
      }
      ```

4. **Distribuire l'infrastruttura:**
   ```bash
   source export.sh
   terraform_plan
   terraform_apply
   ```
# Utilizzo

1. **Autenticazione:**
   - Aprire il browser web e accedere all'URL della vostra applicazione StoryPixAI.
   - Effettuare il login utilizzando il nome utente e la password che avete definito nella variabile `cognito_auth` durante la distribuzione.

2. **Creazione di una storia:**
   - Una volta effettuato il login, accederete all'interfaccia di generazione delle storie.

   - **Scegli la lingua:** Selezionare la lingua desiderata dal menu a tendina in alto a destra dello schermo. Le opzioni disponibili sono: francese, inglese, spagnolo, tedesco, italiano e portoghese.  

    
   - **Inserisci la tua idea di storia:** Nell'apposito campo di testo, digitare alcune parole o una frase che ispirerà la storia. È possibile includere nomi, luoghi, personaggi o qualsiasi altro elemento che ritenete rilevante.
   - **Personalizzare la generazione (opzionale):**
      - **Modello di storia:** Scegliere tra i modelli disponibili (OpenAI GPT-4-o, Anthropic Claude 3, Mistral AI).  
      - **Modello di immagine:** Scegliere tra OpenAI DALL-E 3 o Stable Diffusion XL.  
      - **Dimensioni dell'immagine:** Selezionare la dimensione desiderata per l'illustrazione (le opzioni disponibili dipendono dal modello di immagine scelto).
      - **Stile dell'immagine:** Scegliere uno stile dalla lista proposta per personalizzare l'estetica dell'illustrazione (solo per Stable Diffusion XL).  
      - **Qualità dell'immagine:** Selezionare la qualità dell'immagine generata da DALL-E 3 (standard o HD).
      - **Seed:** Inserire un numero intero per inizializzare il generatore casuale e ottenere risultati diversi a ogni generazione.
      - **Attivare/Disattivare la generazione dell'immagine:** Selezionare o deselezionare la casella per attivare o disattivare la generazione di un'illustrazione per la storia.
   - **Avvia la generazione:** Cliccare sul pulsante "Genera".

3. **Monitoraggio del progresso:**
   - L'interfaccia mostrerà un messaggio "Elaborazione in corso...".

4. **Accesso alla storia generata:**
   - Una volta terminata la generazione, apparirà il messaggio "Pronto!", seguito da un link unico alla storia e all'illustrazione generate.
   - Cliccare sul link per accedere alla vostra creazione.

5. **Consultazione e condivisione:**
   - È possibile consultare la storia e l'illustrazione direttamente nel vostro browser.
   - Potete copiare il link univoco e condividerlo con altre persone affinché possano leggere la vostra creazione.

**Disconnessione:**

* Cliccare sul pulsante "Disconnetti" per effettuare il logout dall'applicazione.

# Documentazione Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requirements

| Nome | Versione |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Providers

| Nome | Versione |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.53.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.2 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## Modules

Nessun modulo.

## Resources | Nome | Tipo |
|------|------|
| [aws_api_gateway_account.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account) | risorsa |
| [aws_api_gateway_authorizer.cognito_authorizer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | risorsa |
| [aws_api_gateway_deployment.api_cors_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | risorsa |
| [aws_api_gateway_deployment.api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | risorsa |
| [aws_api_gateway_deployment.status_api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | risorsa |
| [aws_api_gateway_integration.api_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | risorsa |
| [aws_api_gateway_integration.lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | risorsa |
| [aws_api_gateway_integration.status_lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | risorsa |
| [aws_api_gateway_integration.status_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | risorsa |
| [aws_api_gateway_integration_response.get_status_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | risorsa |
| [aws_api_gateway_integration_response.integration_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | risorsa |
| [aws_api_gateway_integration_response.integration_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | risorsa |
| [aws_api_gateway_integration_response.status_options_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | risorsa |
| [aws_api_gateway_method.api_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | risorsa |
| [aws_api_gateway_method.api_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | risorsa |
| [aws_api_gateway_method.status_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | risorsa |
| [aws_api_gateway_method.status_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | risorsa |
| [aws_api_gateway_method_response.get_status_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | risorsa |
| [aws_api_gateway_method_response.method_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | risorsa |
| [aws_api_gateway_method_response.method_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | risorsa |
| [aws_api_gateway_method_response.status_options_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | risorsa |
| [aws_api_gateway_resource.api_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | risorsa |
| [aws_api_gateway_resource.status_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | risorsa |
| [aws_api_gateway_rest_api.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | risorsa |
| [aws_cloudfront_distributio n.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | risorsa |
| [aws_cloudfront_response_headers_policy.cors_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_response_headers_policy) | risorsa |
| [aws_cloudwatch_log_group.api_gw_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | risorsa |
| [aws_cloudwatch_log_group.lambda_ia](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | risorsa |
| [aws_cloudwatch_log_group.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | risorsa |
| [aws_cognito_identity_pool.identity_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool) | risorsa |
| [aws_cognito_identity_pool_roles_attachment.identity_pool_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool_roles_attachment) | risorsa |
| [aws_cognito_user.example_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user) | risorsa |
| [aws_cognito_user_pool.user_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool) | risorsa |
| [aws_cognito_user_pool_client.user_pool_client](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool_client) | risorsa |
| [aws_dynamodb_table.task_status](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | risorsa |
| [aws_iam_policy.lambda_dynamodb_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | risorsa |
| [aws_iam_policy.lambda_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | risorsa |
| [aws_iam_role.api_cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | risorsa |
| [aws_iam_role.auth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | risorsa |
| [aws_iam_role.lambda_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | risorsa |
| [aws_iam_role.status_checker_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | risorsa |
| [aws_iam_role.unauth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | risorsa |
| [aws_iam_role_policy.auth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | risorsa |
| [aws_iam_role_policy.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | risorsa |
| [aws_iam_role_policy.unauth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | risorsa |
| [aws_iam_role_policy_attachment.lambda_dynamodb_access_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | risorsa |
| [aws_iam_role_policy_attachment.lambda_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | risorsa |
| [aws_lambda_function.StoryPixAI](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | risorsa |
| [aws_lambda_function.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | risorsa |
| [aws_lambda_layer_version.openai_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version) | risorsa |
| [aws_lambda_permission.api_gateway_permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | risorsa |
| [aws_lambda_permission.status_api_gateway_permission]( https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | risorsa |
| [aws_s3_bucket_acl.storypixai_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | risorsa |
| [aws_s3_bucket_cors_configuration.storypixai_cors](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | risorsa |
| [aws_s3_bucket_ownership_controls.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_ownership_controls) | risorsa |
| [aws_s3_bucket_policy.storypixai_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | risorsa |
| [aws_s3_bucket_public_access_block.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | risorsa |
| [aws_s3_bucket_website_configuration.storypixai_website_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | risorsa |
| [aws_s3_object.index_html](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | risorsa |
| [aws_s3_object.script_js](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | risorsa |
| [null_resource.prepare_lambda_deployment](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | risorsa |
| [archive_file.StoryPixAI_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [template_file.script_js](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Nome | Descrizione | Tipo | Default | Richiesto |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Nome del bucket S3 per archiviare le immagini | `string` | n/a | sì |
| <a name="input_cognito_auth"></a> [cognito\_auth](#input\_cognito\_auth) | Login e password per autenticarsi sul generatore | `map(string)` | <pre>{<br>  "password": "_Ch4ng3M3_42",<br>  "user": "storypixia"<br>}</pre> | no |
| <a name="input_ia_environment_variables"></a> [ia\_environment\_variables](#input\_ia\_environment\_variables) | Variabili d'ambiente per la funzione Lambda | `map(string)` | <pre>{<br>  "ANTHROPIC_STORY_MODEL": "anthropic.claude-3-sonnet-20240229-v1:0",<br>  "META_STORY_MODEL": "meta.llama3-70b-instruct-v1:0",<br>  "MISTRAL_STORY_MODEL": "mistral.mistral-large-2402-v1:0",<br>  "OPENAI_IMAGE_MODEL": "dall-e-3",<br>  "OPENAI_STORY_MODEL": "gpt-4o",<br>  "STABLE_DIFFUSION_IMAGE_MODEL": "stability.stable-diffusion-xl-v1",<br>  "TITAN_IMAGE_MODEL": "amazon.titan-image-generator-v1"<br>}</pre> | no |
| <a name="input_projet"></a> [projet](#input\_projet) | Nome del progetto | `string` | `"StoryPixAI"` | no |
| <a name="input_python_runtime"></a> [python\_runtime](#input\_python\_runtime) | Runtime per la funzione Lambda | `string` | `"python3.10"` | no |
| <a name="input_region"></a> [region](#input\_region) | Regione AWS | `string` | `"us-east-1"` | no |

## Outputs | Nome | Descrizione |
|------|-------------|
| <a name="output_cloudfront"></a> [cloudfront](#output\_cloudfront) | n/a |
| <a name="output_s3_bucket_website_url"></a> [s3\_bucket\_website\_url](#output\_s3\_bucket\_website\_url) | Definizione dell'output per l'URL del sito web ospitato su S3 |
<!-- END_TF_DOCS -->

# Limitazioni

StoryPixAI è un progetto che può presentare alcune limitazioni:

* **Qualità variabile:** La qualità delle storie e delle illustrazioni generate può variare in base ai modelli di intelligenza artificiale utilizzati, ai prompt forniti e ad altri fattori.
* **Costo:** L'uso delle API di OpenAI e Bedrock può comportare costi, in base al vostro utilizzo.

# Contribuire

StoryPixAI è un progetto open source. Se desideri contribuire, ecco alcune idee:

* **Segnalare un bug:** Se incontri un problema o un bug, apri una segnalazione nella sezione "Issues" del repository su GitLab.
* **Proporre un miglioramento:** Se hai un'idea per un miglioramento o una nuova funzionalità, non esitare a inviare una "Merge Request" con il tuo codice.
* **Migliorare la documentazione:** Se trovi errori o imprecisioni nel README, correggili pure.

Grazie per il tuo interesse e sostegno a StoryPixAI!

# Autore

Julien LE SAUX  
Sito web: https://jls42.org  
Email: contact@jls42.org  

# Manleva

L'uso di questo software è a proprio rischio. L'autore non può essere ritenuto responsabile per costi, danni o perdite risultanti dal suo uso. Si prega di assicurarsi di comprendere le spese associate all'uso dei servizi AWS prima di distribuire questo progetto.

# Licenza

Questo progetto è sotto licenza MIT con Clausola Comune. Ciò significa che sei libero di utilizzare, modificare e distribuire il software, ma non puoi venderlo o utilizzarlo per offrire servizi commerciali. Per maggiori dettagli, consulta il file [LICENSE](LICENSE).

# Esempi di storie generate

## Francese
### Zoé e Tom
![alt text](/img/zoe_tom_fr.png)

## Inglese
### Léa
![alt text](/img/lea_en.png)

## Spagnolo
### Roger
![alt text](/img/roger_es.png)

**Questo documento è stato tradotto dalla versione fr alla lingua it utilizzando il modello gpt-4o. Per maggiori informazioni sul processo di traduzione, consultare https://gitlab.com/jls42/ai-powered-markdown-translator**

