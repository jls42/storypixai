# Configuration du fournisseur AWS avec la région spécifiée et l'ajout de tags par défaut.
provider "aws" {
  region = var.region # Utilise la région définie par la variable 'region'.

  # Configuration des tags par défaut à appliquer à toutes les ressources créées.
  default_tags {
    tags = {
      "Project" = "StoryPixAI"
    }
  }
}
