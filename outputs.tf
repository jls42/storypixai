# # Définition de la sortie pour l'URL du site web hébergé sur S3
# output "s3_bucket_website_url" {
#   value = "https://${aws_s3_bucket.storypixai.bucket}.s3.amazonaws.com/index.html"
# }

output "url_du_generateur" {
  value = "https://aws_cloudfront_distribution.storypixai.domain_name"
}
