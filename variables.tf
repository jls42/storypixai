# Définition de la variable pour la région AWS
variable "region" {
  description = "Région AWS"
  type        = string
  default     = "us-east-1"
}

# Définition de la variable pour le nom du bucket S3
variable "bucket_name" {
  description = "Nom du bucket S3 pour stocker les images"
  type        = string
}

# Définition de la variable pour le runtime de la fonction Lambda
variable "python_runtime" {
  description = "Runtime pour la fonction Lambda"
  type        = string
  default     = "python3.10"
}

# Définition de la variable pour le nom du projet
variable "projet" {
  description = "Nom du projet"
  type        = string
  default     = "StoryPixAI"
}

# Définition des variables d'environnement pour la fonction Lambda
variable "ia_environment_variables" {
  description = "Variables d'environnement pour la fonction Lambda"
  type        = map(string)
  default = {
    MISTRAL_STORY_MODEL          = "mistral.mistral-large-2402-v1:0"
    ANTHROPIC_STORY_MODEL        = "anthropic.claude-3-sonnet-20240229-v1:0"
    META_STORY_MODEL             = "meta.llama3-70b-instruct-v1:0"
    OPENAI_STORY_MODEL           = "gpt-4o"
    OPENAI_IMAGE_MODEL           = "dall-e-3"
    TITAN_IMAGE_MODEL            = "amazon.titan-image-generator-v1"
    STABLE_DIFFUSION_IMAGE_MODEL = "stability.stable-diffusion-xl-v1"
  }
}

# Définition des variables d'authentification pour Cognito
variable "cognito_auth" {
  description = "Login et mot de passe pour s'authentifier sur le générateur"
  type        = map(string)
  default = {
    user     = "storypixia"
    password = "_Ch4ng3M3_42"
  }
}

variable "deploy_site" {
  description = "Boolean to control if site should be deployed"
  type        = bool
  default     = true
}
