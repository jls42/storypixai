# Création de la table DynamoDB pour stocker le statut des tâches
resource "aws_dynamodb_table" "task_status" {
  name         = "TaskStatus"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "requestId"

  # Définition des attributs de la table
  attribute {
    name = "requestId"
    type = "S"
  }

  attribute {
    name = "status"
    type = "S"
  }

  attribute {
    name = "resultUrl"
    type = "S"
  }

  # Définition d'un index secondaire global basé sur le statut
  global_secondary_index {
    name            = "StatusIndex"
    hash_key        = "status"
    projection_type = "ALL"
  }

  # Définition d'un index secondaire global basé sur l'URL du résultat
  global_secondary_index {
    name            = "ResultUrlIndex"
    hash_key        = "resultUrl"
    projection_type = "ALL"
  }
}
