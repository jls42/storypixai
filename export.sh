#!/bin/bash

# Configuration initiale et définition des variables globales.
set -a  # Active l'exportation de toutes les variables définies à partir de ce point.

# Définition des variables pour configurer l'environnement AWS et Terraform.
BUCKET_STATE="heychangeme"  # Nom du bucket S3 pour stocker l'état Terraform.
AWS_DEFAULT_REGION="us-east-1"  # Région AWS utilisée par défaut.
echo "Région par défaut : $AWS_DEFAULT_REGION"
TF_VAR_region="${AWS_DEFAULT_REGION}"  # Définit la région AWS comme variable d'environnement pour Terraform.

# Vérifie si awscli est installé, sinon il l'installe.
if ! command -v aws &> /dev/null; then
    echo "awscli n'est pas installé. Installation en cours..."
    pip install awscli  # Utilise pip pour installer awscli.
fi

# Crée un bucket S3 pour stocker l'état de Terraform, si ce n'est pas déjà fait, et active le versioning du bucket.
echo "Vérification du bucket S3 pour l'état de Terraform..."
aws s3api head-bucket --bucket ${BUCKET_STATE} || aws s3 mb s3://${BUCKET_STATE}
aws s3api put-bucket-versioning --bucket ${BUCKET_STATE} --versioning-configuration Status=Enabled

# Initialise Terraform avec les configurations nécessaires pour utiliser le bucket S3 comme backend.
echo "Initialisation de Terraform..."
terraform init -input=false -backend-config="bucket=${BUCKET_STATE}" -backend-config="region=${AWS_DEFAULT_REGION}" -backend-config="key=terraform.tfstate"

# Fonction pour planifier les modifications Terraform.
terraform_plan() {
  export TF_VAR_region="${AWS_DEFAULT_REGION}"
  terraform get -update  # Met à jour les modules Terraform.
  terraform validate  # Valide la configuration Terraform.
  terraform plan -out=tfplan -input=false  # Crée un plan d'exécution Terraform.
}

# Fonction pour appliquer les modifications Terraform.
terraform_apply() {
  export TF_VAR_region="${AWS_DEFAULT_REGION}"
  terraform apply -input=false tfplan  # Applique le plan d'exécution Terraform.
}

# Fonction pour détruire les ressources Terraform.
terraform_destroy() {
  terraform destroy -input=false -auto-approve  # Détruit toutes les ressources Terraform configurées.
}

# Suppression d'un paramètre spécifique dans AWS SSM Parameter Store.
delete_parameter() {
  local parameter_path="$1"  # Chemin du paramètre à supprimer.
  echo "Suppression de la clé $parameter_path dans AWS SSM Parameter Store..."
  if aws ssm delete-parameter --name "$parameter_path" --region $AWS_DEFAULT_REGION 2>/dev/null; then
    echo "$parameter_path supprimé avec succès."
  else
    echo "$parameter_path n'existe pas ou ne pouvait pas être supprimé."
  fi
}

# Fonction pour gérer la clé OpenAI dans AWS SSM Parameter Store.
manage_openai_key() {
  local action=$1  # Action à effectuer ('put' pour ajouter, 'delete' pour supprimer).
  local key_value=$2  # Valeur de la clé à gérer.
  local parameter_path="/openaikey"  # Chemin du paramètre dans SSM Parameter Store.
  case "$action" in
    put)
      if [ -z "$key_value" ]; then
        echo "Erreur : Une clé OpenAI doit être fournie pour l'action 'put'."
        return 1
      fi
      echo "Ajout de la clé OpenAI dans AWS SSM Parameter Store..."
      aws ssm put-parameter --name "$parameter_path" --value "$key_value" --type "SecureString" --overwrite
      ;;
    delete)
      delete_parameter "$parameter_path"
      ;;
    *)
      echo "Action non reconnue. Les actions valides sont 'put' et 'delete'."
      return 1
      ;;
  esac
}

# Affiche les commandes disponibles pour l'utilisateur.
display_help() {
  echo "Commandes disponibles :"
  echo "  terraform_plan                  - Valide et planifie les modifications Terraform."
  echo "  terraform_apply                 - Applique les modifications Terraform."
  echo "  terraform_destroy               - Détruit toutes les ressources Terraform après confirmation."
  echo "  manage_openai_key [put/delete] [key] - Ajoute ou supprime la clé OpenAI dans AWS SSM Parameter Store."
}

# Exporte les fonctions pour qu'elles soient disponibles pour une utilisation ultérieure.
export -f terraform_plan
export -f terraform_apply
export -f terraform_destroy
export -f manage_openai_key
export -f display_help

echo "L'initialisation est terminée."
display_help  # Affiche l'aide à l'utilisateur.
