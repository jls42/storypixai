document.addEventListener('DOMContentLoaded', function() {
    const translations = {
        fr: {
            title: "Génération d'histoires avec l'IA",
            loginButton: "Se connecter",
            logoutButton: "Se déconnecter",
            generateButton: "Générer",
            statusText: "En attente du lancement de la génération de l'histoire",
            resultLink: "Cliquez ici pour voir le résultat",
            storyGenerationModelLabel: "Modèle de génération de texte :",
            imageGenerationModelLabel: "Modèle d'image :",
            sizeLabel: "Taille de l'image :",
            styleLabel: "Style de l'image :",
            qualityLabel: "Qualité de l'image :",
            enableImageGenerationLabel: "Activer la génération d'image",
            loginModalTitle: "Connexion",
            usernameLabel: "Nom d'utilisateur:",
            passwordLabel: "Mot de passe:",
            submitLoginButton: "Se connecter",
            textInputPlaceholder: "Entrez votre texte ici",
            storyLanguage: "fr",
            statusWaiting: "Traitement en cours...",
            statusFailed: "Échec du traitement.",
            statusReady: "Prêt !",
            statusNoConnection: "Pas de connexion Internet. Veuillez vérifier votre réseau.",
            statusRetrying: "Problème de réseau. Tentative de reconnexion...",
            styles: {
                cartoon: "Dessin animé",
                watercolor: "Aquarelle",
                pop_art: "Pop Art",
                vintage: "Style Vintage",
                fairy_tale: "Illustration de Conte de Fées",
                black_white: "Noir et blanc",
                comic_book: "Bande Dessinée",
                pencil_sketch: "Croquis au crayon",
                digital_art: "Art numérique",
                chibi: "Chibi",
                steampunk: "Steampunk",
                pixel_art: "Pixel Art",
                anime: "Anime",
                pastel: "Pastel",
                "3d_render": "Rendu 3D",
                surrealism: "Surréalisme",
                impressionism: "Impressionnisme",
                minimalist: "Minimaliste",
                retro: "Rétro",
                abstract: "Abstrait",
                flat_design: "Flat Design",
                cartoon_retro: "Cartoon Retro",
                kawaii: "Kawaii",
                collage: "Collage",
                mixed_media: "Mixed Media",
                naif: "Naïf",
                stained_glass: "Vitrail",
                grunge: "Grunge",
                vintage_childrens_book: "Livre pour enfants vintage",
                silhouette: "Silhouette",
                fantasy_art: "Art fantastique",
                graffiti: "Graffiti"
            }
        },
        en: {
            title: "AI Story Generation",
            loginButton: "Log In",
            logoutButton: "Log Out",
            generateButton: "Generate",
            statusText: "Waiting to start story generation",
            resultLink: "Click here to view the result",
            storyGenerationModelLabel: "Text Generation Model:",
            imageGenerationModelLabel: "Image Model:",
            sizeLabel: "Image Size:",
            styleLabel: "Image Style:",
            qualityLabel: "Image Quality:",
            enableImageGenerationLabel: "Enable Image Generation",
            loginModalTitle: "Log In",
            usernameLabel: "Username:",
            passwordLabel: "Password:",
            submitLoginButton: "Log In",
            textInputPlaceholder: "Enter your text here",
            storyLanguage: "en",
            statusWaiting: "Processing...",
            statusFailed: "Processing failed.",
            statusReady: "Ready!",
            statusNoConnection: "No Internet connection. Please check your network.",
            statusRetrying: "Network issue. Retrying...",
            styles: {
                cartoon: "Cartoon",
                watercolor: "Watercolor",
                pop_art: "Pop Art",
                vintage: "Vintage Style",
                fairy_tale: "Fairy Tale Illustration",
                black_white: "Black and White",
                comic_book: "Comic Book",
                pencil_sketch: "Pencil Sketch",
                digital_art: "Digital Art",
                chibi: "Chibi",
                steampunk: "Steampunk",
                pixel_art: "Pixel Art",
                anime: "Anime",
                pastel: "Pastel",
                "3d_render": "3D Render",
                surrealism: "Surrealism",
                impressionism: "Impressionism",
                minimalist: "Minimalist",
                retro: "Retro",
                abstract: "Abstract",
                flat_design: "Flat Design",
                cartoon_retro: "Cartoon Retro",
                kawaii: "Kawaii",
                collage: "Collage",
                mixed_media: "Mixed Media",
                naif: "Naïve",
                stained_glass: "Stained Glass",
                grunge: "Grunge",
                vintage_childrens_book: "Vintage Children’s Book",
                silhouette: "Silhouette",
                fantasy_art: "Fantasy Art",
                graffiti: "Graffiti"
            }
        },
        es: {
            title: "Generación de historias con IA",
            loginButton: "Iniciar sesión",
            logoutButton: "Cerrar sesión",
            generateButton: "Generar",
            statusText: "Esperando para comenzar la generación de la historia",
            resultLink: "Haz clic aquí para ver el resultado",
            storyGenerationModelLabel: "Modelo de generación de texto:",
            imageGenerationModelLabel: "Modelo de imagen:",
            sizeLabel: "Tamaño de la imagen:",
            styleLabel: "Estilo de la imagen:",
            qualityLabel: "Calidad de la imagen:",
            enableImageGenerationLabel: "Habilitar la generación de imágenes",
            loginModalTitle: "Iniciar sesión",
            usernameLabel: "Nombre de usuario:",
            passwordLabel: "Contraseña:",
            submitLoginButton: "Iniciar sesión",
            textInputPlaceholder: "Ingrese su texto aquí",
            storyLanguage: "es",
            statusWaiting: "Procesando...",
            statusFailed: "El procesamiento falló.",
            statusReady: "¡Listo!",
            statusNoConnection: "No hay conexión a Internet. Por favor, verifique su red.",
            statusRetrying: "Problema de red. Reintentando...",
            styles: {
                cartoon: "Dibujos animados",
                watercolor: "Acuarela",
                pop_art: "Arte pop",
                vintage: "Estilo vintage",
                fairy_tale: "Ilustración de cuentos de hadas",
                black_white: "Blanco y negro",
                comic_book: "Cómic",
                pencil_sketch: "Boceto a lápiz",
                digital_art: "Arte digital",
                chibi: "Chibi",
                steampunk: "Steampunk",
                pixel_art: "Pixel Art",
                anime: "Anime",
                pastel: "Pastel",
                "3d_render": "Renderizado 3D",
                surrealism: "Surrealismo",
                impressionism: "Impresionismo",
                minimalist: "Minimalista",
                retro: "Retro",
                abstract: "Abstracto",
                flat_design: "Diseño plano",
                cartoon_retro: "Caricatura retro",
                kawaii: "Kawaii",
                collage: "Collage",
                mixed_media: "Medios mixtos",
                naif: "Naïve",
                stained_glass: "Vitrales",
                grunge: "Grunge",
                vintage_childrens_book: "Libro infantil vintage",
                silhouette: "Silueta",
                fantasy_art: "Arte fantástico",
                graffiti: "Graffiti"
            }
        },
        de: {
            title: "KI-Geschichtengenerierung",
            loginButton: "Einloggen",
            logoutButton: "Ausloggen",
            generateButton: "Generieren",
            statusText: "Warten auf den Start der Geschichtengenerierung",
            resultLink: "Klicken Sie hier, um das Ergebnis anzuzeigen",
            storyGenerationModelLabel: "Textgenerierungsmodell:",
            imageGenerationModelLabel: "Bildmodell:",
            sizeLabel: "Bildgröße:",
            styleLabel: "Bildstil:",
            qualityLabel: "Bildqualität:",
            enableImageGenerationLabel: "Bildgenerierung aktivieren",
            loginModalTitle: "Einloggen",
            usernameLabel: "Benutzername:",
            passwordLabel: "Passwort:",
            submitLoginButton: "Einloggen",
            textInputPlaceholder: "Geben Sie hier Ihren Text ein",
            storyLanguage: "de",
            statusWaiting: "Verarbeitung...",
            statusFailed: "Verarbeitung fehlgeschlagen.",
            statusReady: "Bereit!",
            statusNoConnection: "Keine Internetverbindung. Bitte überprüfen Sie Ihr Netzwerk.",
            statusRetrying: "Netzwerkproblem. Wiederholen...",
            styles: {
                cartoon: "Zeichentrick",
                watercolor: "Aquarell",
                pop_art: "Pop Art",
                vintage: "Vintage-Stil",
                fairy_tale: "Märchenillustration",
                black_white: "Schwarz und weiß",
                comic_book: "Comic",
                pencil_sketch: "Bleistiftskizze",
                digital_art: "Digitale Kunst",
                chibi: "Chibi",
                steampunk: "Steampunk",
                pixel_art: "Pixelkunst",
                anime: "Anime",
                pastel: "Pastell",
                "3d_render": "3D-Render",
                surrealism: "Surrealismus",
                impressionism: "Impressionismus",
                minimalist: "Minimalistisch",
                retro: "Retro",
                abstract: "Abstrakt",
                flat_design: "Flat Design",
                cartoon_retro: "Retro-Zeichentrick",
                kawaii: "Kawaii",
                collage: "Collage",
                mixed_media: "Mixed Media",
                naif: "Naive Kunst",
                stained_glass: "Buntglas",
                grunge: "Grunge",
                vintage_childrens_book: "Vintage-Kinderbuch",
                silhouette: "Silhouette",
                fantasy_art: "Fantasy-Kunst",
                graffiti: "Graffiti"
            }
        },
        it: {
            title: "Generazione di storie con l'IA",
            loginButton: "Accedi",
            logoutButton: "Disconnetti",
            generateButton: "Genera",
            statusText: "In attesa dell'inizio della generazione della storia",
            resultLink: "Clicca qui per vedere il risultato",
            storyGenerationModelLabel: "Modello di generazione del testo:",
            imageGenerationModelLabel: "Modello di immagine:",
            sizeLabel: "Dimensione dell'immagine:",
            styleLabel: "Stile dell'immagine:",
            qualityLabel: "Qualità dell'immagine:",
            enableImageGenerationLabel: "Abilita generazione di immagini",
            loginModalTitle: "Accedi",
            usernameLabel: "Nome utente:",
            passwordLabel: "Password:",
            submitLoginButton: "Accedi",
            textInputPlaceholder: "Inserisci qui il tuo testo",
            storyLanguage: "it",
            statusWaiting: "Elaborazione...",
            statusFailed: "Elaborazione fallita.",
            statusReady: "Pronto!",
            statusNoConnection: "Nessuna connessione a Internet. Controlla la tua rete.",
            statusRetrying: "Problema di rete. Riprovo...",
            styles: {
                cartoon: "Cartone animato",
                watercolor: "Acquerello",
                pop_art: "Pop Art",
                vintage: "Stile Vintage",
                fairy_tale: "Illustrazione di fiabe",
                black_white: "Bianco e nero",
                comic_book: "Fumetto",
                pencil_sketch: "Schizzo a matita",
                digital_art: "Arte digitale",
                chibi: "Chibi",
                steampunk: "Steampunk",
                pixel_art: "Pixel Art",
                anime: "Anime",
                pastel: "Pastello",
                "3d_render": "Rendering 3D",
                surrealism: "Surrealismo",
                impressionism: "Impressionismo",
                minimalist: "Minimalista",
                retro: "Retrò",
                abstract: "Astratto",
                flat_design: "Flat Design",
                cartoon_retro: "Cartone Retrò",
                kawaii: "Kawaii",
                collage: "Collage",
                mixed_media: "Tecniche miste",
                naif: "Arte naif",
                stained_glass: "Vetro colorato",
                grunge: "Grunge",
                vintage_childrens_book: "Libro per bambini vintage",
                silhouette: "Silhouette",
                fantasy_art: "Arte fantastica",
                graffiti: "Graffiti"
            }
        },
        pt: {
            title: "Geração de histórias com IA",
            loginButton: "Entrar",
            logoutButton: "Sair",
            generateButton: "Gerar",
            statusText: "Aguardando o início da geração da história",
            resultLink: "Clique aqui para ver o resultado",
            storyGenerationModelLabel: "Modelo de geração de texto:",
            imageGenerationModelLabel: "Modelo de imagem:",
            sizeLabel: "Tamanho da imagem:",
            styleLabel: "Estilo da imagem:",
            qualityLabel: "Qualidade da imagem:",
            enableImageGenerationLabel: "Ativar geração de imagens",
            loginModalTitle: "Entrar",
            usernameLabel: "Nome de usuário:",
            passwordLabel: "Senha:",
            submitLoginButton: "Entrar",
            textInputPlaceholder: "Digite seu texto aqui",
            storyLanguage: "pt",
            statusWaiting: "Processando...",
            statusFailed: "Falha no processamento.",
            statusReady: "Pronto!",
            statusNoConnection: "Sem conexão com a Internet. Verifique sua rede.",
            statusRetrying: "Problema de rede. Tentando novamente...",
            styles: {
                cartoon: "Desenho animado",
                watercolor: "Aquarela",
                pop_art: "Pop Art",
                vintage: "Estilo Vintage",
                fairy_tale: "Ilustração de conto de fadas",
                black_white: "Preto e branco",
                comic_book: "História em quadrinhos",
                pencil_sketch: "Esboço a lápis",
                digital_art: "Arte digital",
                chibi: "Chibi",
                steampunk: "Steampunk",
                pixel_art: "Pixel Art",
                anime: "Anime",
                pastel: "Pastel",
                "3d_render": "Renderização 3D",
                surrealism: "Surrealismo",
                impressionism: "Impressionismo",
                minimalist: "Minimalista",
                retro: "Retrô",
                abstract: "Abstrato",
                flat_design: "Flat Design",
                cartoon_retro: "Desenho Retrô",
                kawaii: "Kawaii",
                collage: "Colagem",
                mixed_media: "Mídia mista",
                naif: "Arte naïf",
                stained_glass: "Vitral",
                grunge: "Grunge",
                vintage_childrens_book: "Livro infantil vintage",
                silhouette: "Silhueta",
                fantasy_art: "Arte fantástica",
                graffiti: "Graffiti"
            }
        }
    };

    const poolData = {
        UserPoolId: '${user_pool_id}',
        ClientId: '${client_id}'
    };
    const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

    const loginButton = document.getElementById('loginButton');
    const logoutButton = document.getElementById('logoutButton');
    const generateForm = document.getElementById('generateForm');
    const authButtons = document.getElementById('authButtons');
    const loginModal = document.getElementById('loginModal');
    const closeModal = document.getElementById('closeModal');
    const submitLogin = document.getElementById('submitLogin');
    const imageGenerationModelSelect = document.getElementById('imageGenerationModelSelect');
    const sizeSelect = document.getElementById('sizeSelect');
    const resultLink = document.getElementById('resultLink');
    const statusText = document.getElementById('statusText');
    const qualitySelect = document.getElementById('qualitySelect');
    const qualityLabel = document.getElementById('qualityLabel');
    const languageSelect = document.getElementById('languageSelect');
    const storyLanguageInput = document.getElementById('storyLanguage');

    let idToken = '';

    checkUserAuthentication();
    updateSizeOptions();
    updateQualityVisibility();
    updateStoryLanguage(languageSelect.value);

    loginButton.addEventListener('click', function() {
        loginModal.style.display = 'flex';
    });

    closeModal.addEventListener('click', function() {
        loginModal.style.display = 'none';
    });

    submitLogin.addEventListener('click', function() {
        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;

        if (username && password) {
            signIn(username, password);
        }
    });

    logoutButton.addEventListener('click', function() {
        const cognitoUser = userPool.getCurrentUser();
        if (cognitoUser) {
            cognitoUser.signOut();
            alert('Déconnecté');
            checkUserAuthentication();
        }
    });

    imageGenerationModelSelect.addEventListener('change', function() {
        updateSizeOptions();
        updateQualityVisibility();
    });

    document.getElementById('generateForm').addEventListener('submit', function(e) {
        e.preventDefault();
        resultLink.style.display = 'none'; // Masquer le lien de résultat
        statusText.textContent = translations[languageSelect.value].statusWaiting; // Utiliser la traduction

        const textInput = document.getElementById('textInput').value;
        const storyGenerationModel = document.getElementById('storyGenerationModelSelect').value;
        const imageGenerationModel = imageGenerationModelSelect.value;
        const size = sizeSelect.value;
        const style = document.getElementById('styleSelect').value;
        const quality = document.getElementById('qualitySelect').value;
        const seed = parseInt(document.getElementById('seedInput').value);
        const enableGeneration = document.getElementById('enableImageGeneration').checked;
        const storyLanguage = storyLanguageInput.value;

        makeRequest(textInput, storyGenerationModel, imageGenerationModel, size, style, quality, seed, enableGeneration, storyLanguage);
    });

    languageSelect.addEventListener('change', function() {
        const selectedLanguage = languageSelect.value;
        changeLanguage(selectedLanguage);
        updateStoryLanguage(selectedLanguage);
    });

    function signIn(username, password) {
        const authenticationData = {
            Username: username,
            Password: password
        };
        const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);

        const userData = {
            Username: username,
            Pool: userPool
        };
        const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function(result) {
                idToken = result.getIdToken().getJwtToken();
                console.log('ID token: ' + idToken);
                alert('Connecté avec succès');
                loginModal.style.display = 'none';
                checkUserAuthentication();
            },
            onFailure: function(err) {
                alert(err.message || JSON.stringify(err));
            },
            newPasswordRequired: function(userAttributes, requiredAttributes) {
                delete userAttributes.email_verified;

                const newPassword = prompt('Vous devez entrer un nouveau mot de passe :');
                cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
            }
        });
    }

    function checkUserAuthentication() {
        const cognitoUser = userPool.getCurrentUser();
        if (cognitoUser != null) {
            cognitoUser.getSession(function(err, session) {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log('Session valid: ' + session.isValid());
                if (session.isValid()) {
                    idToken = session.getIdToken().getJwtToken();
                    authButtons.style.display = 'none';
                    generateForm.style.display = 'flex';
                    scheduleTokenRefresh(session); // Planifier le rafraîchissement du token
                } else {
                    authButtons.style.display = 'flex';
                    generateForm.style.display = 'none';
                }
            });
        } else {
            authButtons.style.display = 'flex';
            generateForm.style.display = 'none';
        }
    }

    function scheduleTokenRefresh(session) {
        const refreshTimeout = (session.getAccessToken().getExpiration() - new Date().getTime() / 1000 - 60) * 1000;
        setTimeout(refreshSession, refreshTimeout);
    }

    function refreshSession() {
        const cognitoUser = userPool.getCurrentUser();
        if (cognitoUser != null) {
            cognitoUser.getSession(function(err, session) {
                if (err) {
                    console.error('Error refreshing session:', err);
                    alert('Votre session a expiré. Veuillez vous reconnecter.');
                    authButtons.style.display = 'flex';
                    generateForm.style.display = 'none';
                } else {
                    idToken = session.getIdToken().getJwtToken();
                    console.log('Token refreshed:', idToken);
                    scheduleTokenRefresh(session);
                }
            });
        }
    }

    function updateSizeOptions() {
        const model = imageGenerationModelSelect.value;
        console.log('Modèle sélectionné:', model); // Debugging
        sizeSelect.innerHTML = ''; // Effacer les options existantes
        if (model === 'openai') {
            sizeSelect.innerHTML = '<option value="1024x1024">1024x1024</option>';
        } else if (model === 'stable_diffusion' || model === 'titan') {
            sizeSelect.innerHTML = `
                <option value="1024x1024">1024x1024</option>
                <option value="512x512">512x512</option>
            `;
        } else {
            sizeSelect.innerHTML = '<option value="1024x1024">1024x1024</option>';
        }
    }

    function updateQualityVisibility() {
        const model = imageGenerationModelSelect.value;
        if (model === 'openai') {
            qualitySelect.style.display = 'block';
            qualityLabel.style.display = 'block'; // Assurez-vous d'afficher le label
        } else {
            qualitySelect.style.display = 'none';
            qualityLabel.style.display = 'none'; // Assurez-vous de masquer le label
        }
    }

    function makeRequest(textInput, storyGenerationModel, imageGenerationModel, size, style, quality, seed, enableGeneration, storyLanguage, retries = 3) {
        let requestBody = { 
            text: textInput, 
            size: size, 
            style: style, 
            quality: quality, 
            seed: seed, 
            story_generation_model: storyGenerationModel,
            generate_images: enableGeneration,
            language: storyLanguage
        };
        if (enableGeneration) {
            requestBody.image_generation_model = imageGenerationModel;
        }

        fetch('${generate_image_url}', {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': idToken  // Utilisation du idToken pour l'autorisation
            },
            body: JSON.stringify(requestBody)
        })
        .then(response => response.json())
        .then(data => {
            const requestId = data.requestId;
            statusText.textContent = translations[languageSelect.value].statusWaiting; // Utiliser la traduction
            checkStatus(requestId);
        })
        .catch(error => {
            console.error('Error:', error);
            if (retries > 0 && navigator.onLine) {
                setTimeout(() => makeRequest(textInput, storyGenerationModel, imageGenerationModel, size, style, quality, seed, enableGeneration, storyLanguage, retries - 1), 5000);
            } else {
                statusText.textContent = translations[languageSelect.value].statusNoConnection; // Utiliser la traduction
            }
        });
    }

    function checkStatus(requestId) {
        if (!navigator.onLine) {
            statusText.textContent = translations[languageSelect.value].statusNoConnection; // Utiliser la traduction
            return;
        }

        setTimeout(() => {
            fetch('${check_status_url}?requestId=' + requestId, {
                method: 'GET',
                headers: { 'Authorization': idToken } // Utilisation du idToken pour l'autorisation
            })
            .then(response => response.json())
            .then(data => {
                if (data.status === 'link') {
                    resultLink.href = data.resultUrl;
                    resultLink.style.display = 'block';
                    statusText.textContent = translations[languageSelect.value].statusReady; // Utiliser la traduction
                } else if (data.status === 'Failed') {
                    statusText.textContent = translations[languageSelect.value].statusFailed; // Utiliser la traduction
                } else {
                    statusText.textContent = translations[languageSelect.value].statusWaiting; // Utiliser la traduction
                    checkStatus(requestId);
                }
            })
            .catch(error => {
                console.error('Error:', error);
                statusText.textContent = translations[languageSelect.value].statusRetrying; // Utiliser la traduction
                setTimeout(() => checkStatus(requestId), 5000);
            });
        }, 5000);
    }

    function updateStyleOptions(lang) {
        const styleSelect = document.getElementById('styleSelect');
        const styles = translations[lang].styles;
        
        styleSelect.innerHTML = ''; // Clear current options

        for (const key in styles) {
            const option = document.createElement('option');
            option.value = key;
            option.textContent = styles[key];
            styleSelect.appendChild(option);
        }
    }

    function updateStoryLanguage(lang) {
        storyLanguageInput.value = translations[lang].storyLanguage;
    }

    function changeLanguage(lang) {
        const elementsToTranslate = document.querySelectorAll('[data-lang]');
        elementsToTranslate.forEach(element => {
            const key = element.getAttribute('data-lang');
            if (translations[lang][key]) {
                element.textContent = translations[lang][key];
            }
        });

        const placeholdersToTranslate = document.querySelectorAll('[data-lang-placeholder]');
        placeholdersToTranslate.forEach(element => {
            const key = element.getAttribute('data-lang-placeholder');
            if (translations[lang][key]) {
                element.placeholder = translations[lang][key];
            }
        });

        // Mettre à jour les messages de statut
        const statusText = document.getElementById('statusText');
        statusText.dataset.lang = 'statusText';

        updateStyleOptions(lang);
        updateStoryLanguage(lang);
    }

    // Initial call to set default language
    changeLanguage('fr');
});

