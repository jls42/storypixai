[[_TOC_]]

# StoryPixAI : Cuentos nocturnos para niños generados e ilustrados por IA

StoryPixAI es un proyecto serverless innovador para generar cuentos nocturnos mágicos, creados por IA. Simplemente escriba algunas palabras sobre el tema de su elección y deje que la inteligencia artificial teja un cuento nocturno único y cautivador, acompañado de una adorables ilustración. Todo es generado automáticamente en el idioma de su elección gracias a los modelos de IA de vanguardia de OpenAI y Amazon Bedrock. Ofrezca a sus hijos un momento mágico antes de dormir, mientras disfruta de una experiencia de usuario fluida gracias a la potencia de la infraestructura serverless de AWS.

## Funcionalidades

* **Cuentos nocturnos personalizados creados por IA:** Transforme las ideas de sus hijos en cuentos nocturnos originales, gracias a la potencia de los modelos de IA de OpenAI y AWS Bedrock.
* **Ilustraciones adorables generadas por IA:** Cada historia cobra vida con imágenes lindas y coloridas, perfectas para estimular la imaginación de sus hijos.
* **Guardado automático en la nube de AWS:** StoryPixAI guarda automáticamente sus historias e ilustraciones en un bucket S3, accesible a través de un enlace único para releerlas en cualquier momento.
* **Acceso seguro al generador:** La interfaz de creación de historias está protegida por autenticación, garantizando así un espacio de creación privado y seguro.
* **Multilingüe:** StoryPixAI es compatible con los siguientes idiomas:
    * Francés (principalmente probado)
    * Inglés
    * Español
    * Alemán
    * Italiano
    * Portugués

# Arquitectura

StoryPixAI se basa en una arquitectura serverless:

## Esquema de arquitectura de la infraestructura sin servidor
![alt text](/img/storypixai-architecture.png)

## Frontend

* **Interfaz de generación (`index.html`):** La interfaz de usuario de StoryPixAI es una aplicación de una sola página (SPA) desarrollada en HTML, CSS y JavaScript puro (Vanilla JavaScript). Ofrece una experiencia de usuario intuitiva e interactiva para:  
    ![alt text](/img/generateur.png)

    * **Autenticación:** Los usuarios deben iniciar sesión con sus credenciales de Cognito para acceder a las funcionalidades de generación. Se proporciona una ventana modal de inicio de sesión para facilitar este proceso.

    * **Selección de idioma:** Un menú desplegable permite elegir el idioma del generador y de la historia entre los soportados: francés, inglés, español, alemán, italiano y portugués. El idioma seleccionado se utiliza para la interfaz de usuario y para guiar la generación de la historia.  
        ![alt text](/img/langues.png)
    * **Entrada del prompt:** Un campo de texto permite al usuario ingresar la idea o el tema del cuento deseado.

    * **Personalización de la generación (opcional):** La interfaz ofrece opciones para personalizar la generación:
        * **Elección del modelo de generación de texto:** OpenAI GPT-4-o, Anthropic Claude 3, o Mistral AI.  
            ![alt text](/img/texte_modele.png)
        * **Elección del modelo de generación de imagen:** OpenAI DALL-E 3 o Stable Diffusion XL.  
            ![alt text](/img/image_modele.png)
        * **Configuraciones específicas del modelo de imagen:** Tamaño, estilo (para Stable Diffusion XL) y calidad (para DALL-E 3).  
            ![alt text](/img/styles.png)  
        * **Seed:** Un campo numérico permite especificar un valor para inicializar el generador aleatorio.
        * **Activación/desactivación de la generación de imagen.**

    * **Inicio de la generación:** Un botón "Generar" desencadena el proceso de generación de la historia y de la imagen (si está activada).

    * **Seguimiento del progreso:** Un mensaje de estado informa al usuario sobre el progreso de la generación ("Procesando...", "¡Listo!", etc.) y de posibles errores.

    * **Visualización del resultado:** Un enlace único a la historia y a la imagen generadas se muestra cuando la generación se ha completado. Este enlace puede copiarse y compartirse. * **Gestión de la autenticación y las solicitudes:**
    * El script `storypixai.js` gestiona la autenticación del usuario con Amazon Cognito y utiliza el token de acceso obtenido para autorizar las solicitudes al API Gateway.
    * También gestiona la comunicación con los endpoints del API Gateway (`/generate` y `/check_status/{task_id}`) para desencadenar la generación y seguir el progreso.
    * Se implementan también mecanismos de gestión de errores y nuevos intentos para garantizar la fiabilidad de la aplicación.

* **Hosting y distribución:**
    * La interfaz de generación (`index.html`) está alojada en Amazon S3 y distribuida a través de Amazon CloudFront, una red de distribución de contenido (CDN) que mejora el rendimiento y la disponibilidad de la aplicación para usuarios de todo el mundo.



## Backend

* **Funciones AWS Lambda (Python):**
    * **`StoryPixAI`:** Esta función es el corazón de la aplicación. Toma como entrada un prompt textual proporcionado por el usuario a través de la interfaz de generación y orquesta el proceso de creación de la historia e ilustración. Llama sucesivamente a las APIs de OpenAI y/o Bedrock para generar el texto de la historia y la imagen asociada. Luego, almacena la imagen en el bucket S3 y devuelve al usuario un enlace único hacia la historia y la imagen generadas. El estado de avance de la generación se almacena en DynamoDB.

    * **`status_checker`:** Esta función es llamada periódicamente por la interfaz de generación para verificar el estado de avance de la creación de la historia y la imagen. Consulta DynamoDB para recuperar el estado de la tarea en curso y devuelve este estado al frontend. Esto permite a la interfaz de generación mantener informado al usuario sobre el progreso e indicarle cuándo la historia y la imagen están listas.

* **Amazon DynamoDB:** Esta base de datos NoSQL de clave-valor se utiliza para almacenar la información relativa a las tareas de generación de historias e imágenes. Cada tarea está identificada por un `requestId` único. La tabla DynamoDB contiene los siguientes atributos:
    * **`requestId` (clave de hash):** Identificador único de la tarea.
    * **`status`:** Estado de la tarea (en curso, terminada, error).
    * **`resultUrl`:** URL de la historia generada (cuando la tarea ha terminado).

    La tabla DynamoDB también posee dos índices secundarios globales:

    * **`StatusIndex`:** Permite buscar las tareas en función de su estado.
    * **`ResultUrlIndex`:** Permite buscar las tareas en función de la URL de la historia generada.

    La función Lambda `status_checker` utiliza estos índices para recuperar eficientemente el estado y la URL de la historia de una tarea dada. El script JavaScript del frontend consulta luego el endpoint `/check_status/{task_id}` del API Gateway para obtener esta información y actualizar la interfaz de usuario en consecuencia.

* **Amazon S3:** Este servicio de almacenamiento de objetos aloja la interfaz web estática (`index.html`), las imágenes generadas y las historias generadas. Las historias y las imágenes se almacenan con claves únicas generadas por la función `StoryPixAI`, y estas claves se devuelven al usuario para permitirle acceder a sus creaciones.

* **Amazon CloudWatch:** Este servicio de monitoreo recoge los registros y métricas de las funciones Lambda, permitiendo seguir el rendimiento de la aplicación, detectar errores y resolver problemas.

# Infrastructure as Code (IaC)

* **Terraform:** El conjunto de la infraestructura AWS (Lambda, API Gateway, S3, CloudFront, Cognito, DynamoDB, ...) está definido y desplegado mediante Terraform. Esto permite una gestión automatizada, reproducible y versionada de la infraestructura, facilitando las actualizaciones y modificaciones.

# Seguridad y autenticación

* **Amazon Cognito:** Este servicio gestiona la autenticación del usuario. La interfaz de generación (`index.html`) utiliza la API Cognito para autenticar al usuario y obtener un token de acceso. Este token se incluye posteriormente en las solicitudes a los endpoints `/generate` y `/check_status/{task_id}` para autorizar el acceso a las funciones `StoryPixAI` y `status_checker` respectivamente.

# Distribución de contenido

* **Amazon CloudFront:** Este servicio de red de distribución de contenido (CDN) se utiliza para distribuir la interfaz web estática (`index.html`) de manera rápida y confiable a los usuarios de todo el mundo. Cachea el contenido en servidores distribuidos geográficamente, reduciendo así la latencia y mejorando la experiencia del usuario.

# Modelos de IA

StoryPixAI es compatible con una combinación de modelos de IA de vanguardia para generar historias e ilustraciones de alta calidad:

* **Modelos de OpenAI:**
    * **Generación de historias:**
        * OpenAI GPT-4-o
    * **Generación de imágenes:**
        * OpenAI DALL-E 3

* **Modelos accesibles a través de AWS Bedrock:**
    * **Generación de historias:**
        * Anthropic Claude 3 Sonnet (20240229-v1)
        * Mistral AI Mistral Large (2402-v1)
    * **Generación de imágenes:**
        * StabilityAI Stable Diffusion XL v1

# Requisitos previos

Antes de comenzar a usar StoryPixAI, asegúrese de contar con los siguientes elementos:

* **Cuenta de AWS:** Necesitará una cuenta de AWS activa para desplegar y gestionar la infraestructura sin servidor. Asegúrese de que su usuario IAM tiene los siguientes permisos (o equivalentes). **Tenga en cuenta que esta política es un ejemplo y puede adaptarse a sus necesidades específicas de seguridad:**

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "cloudwatch:*",
            "ec2:*",
            "lambda:*",
            "s3:*",
            "ssm:*",
            "logs:*",
            "dynamodb:*",
            "cognito-idp:*",
            "cognito-identity:*",
            "apigateway:*",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:AttachRolePolicy",
            "iam:DetachRolePolicy",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:CreatePolicy",
            "iam:DeletePolicy",
            "iam:PassRole",
            "iam:Tag*",
            "iam:Get*",
            "iam:List*",
            "iam:TagRole",
            "kms:TagResource",
            "iam:CreateServiceLinkedRole",
            "budgets:*",
            "events:*"
          ],
          "Resource": "*"
        }
      ]
    }
    ```

* **Activación de modelos Bedrock:** Asegúrese de que los siguientes modelos de IA están activados en su cuenta de AWS Bedrock:
    * Anthropic Claude 3 Sonnet (20240229-v1)
    * Mistral AI Mistral Large (2402-v1)
    * StabilityAI Stable Diffusion XL v1

[Acceso a los modelos Bedrock - us-east-1](https://us-east-1.console.aws.amazon.com/bedrock/home?region=us-east-1#/modelaccess)
![alt text](/img/modele_1.png)
![alt text](/img/modele_2.png)

* **Claves API de OpenAI (si desea usar los modelos de OpenAI):** Para generar ilustraciones de alta calidad con DALL-E 3, se recomienda encarecidamente tener una clave API de OpenAI válida. Puede obtener su clave en el sitio de OpenAI. Tenga en cuenta que StoryPixAI también puede generar imágenes con Stable Diffusion XL v1 (a través de AWS Bedrock), pero DALL-E 3 generalmente ofrece resultados superiores.

**Para desplegar a través de GitLab CI/CD:**

* **Cuenta de GitLab:** Necesitará una cuenta válida de GitLab y acceso al proyecto StoryPixAI en GitLab.
* **Variables de entorno en GitLab:** Deberá configurar las variables de entorno necesarias en su proyecto de GitLab, incluidas las claves API de AWS y OpenAI (si usa DALL-E 3).

# Instalación y despliegue

## Despliegue a través de GitLab CI/CD (Recomendado)

StoryPixAI está diseñado para ser fácilmente desplegado y actualizado mediante una canalización integrada de GitLab CI/CD. Este método de despliegue es **altamente recomendado**, ya que está automatizado, es confiable e independiente de tu sistema operativo (Linux, macOS, Windows). El pipeline automatiza las etapas de preparación del entorno, verificación de la configuración, despliegue y eliminación de la infraestructura.

**Ejecución del pipeline:**

1. **Forkear el proyecto:** Crea una copia del proyecto en tu propio espacio GitLab yendo a la siguiente URL: [https://gitlab.com/jls42/storypixai](https://gitlab.com/jls42/storypixai)
2. **Clonar el repositorio:**
   ```bash
   git clone https://gitlab.com/tu_usuario/storypixai.git # Reemplaza "tu_usuario" por tu nombre de usuario de GitLab
   cd storypixai
   ```
3. **Configurar las variables de entorno:**
   - Ve a la configuración de CI/CD de tu proyecto.
   - Define las siguientes variables:
     * `AWS_ACCESS_KEY_ID`: Tu clave de acceso AWS.
     * `AWS_SECRET_ACCESS_KEY`: Tu clave secreta de AWS.
     * `OPENAI_KEY` (recomendado): Tu clave API de OpenAI (si usas DALL-E 3 o GPT4-o).
     * `TF_VAR_cognito_auth`: Una variable que contiene la configuración de Cognito en formato JSON, por ejemplo:
       ```json
       {"user": "tu_nombre_de_usuario", "password": "tu_contraseña_segura"}
       ```

4. **Personalizar el script `export.sh`:**
   * Abre el archivo `export.sh` y modifica las siguientes variables para que coincidan con tu configuración:
      ```bash
      # el bucket se creará automáticamente si está disponible, elige un nombre único
      BUCKET_STATE="tu_nombre_de_bucket_para_el_estado_terraform"
      AWS_DEFAULT_REGION="tu_región_aws"
      ```

5. **Configurar Terraform:**
   * Crea un archivo `terraform.tfvars` en la raíz del directorio `terraform` y define las variables específicas para tu entorno, incluyendo (ver la sección Documentación de Terraform-docs más abajo para más detalles):
      ```
      bucket_name = "tu_nombre_de_bucket_s3"

6. **Disparar el pipeline:** 
   - Ve a la sección CI/CD de tu proyecto GitLab.
   - Elige la etapa que deseas ejecutar (por ejemplo, `Verificación de Terraform` o `Despliegue de Terraform`).
   - Haz clic en el botón "Play" para iniciar la etapa seleccionada.   
    ![alt text](/img/cicd.png)

**Nota:** Si utilizas la CI/CD proporcionada con el proyecto, no es necesario instalar Terraform. El despliegue se realizará automáticamente a través de GitLab CI/CD después de haber configurado las variables de entorno requeridas.

## Instalación en una máquina Linux (Ubuntu 22.04) (Opcional)

Si prefieres desplegar StoryPixAI manualmente, puedes hacerlo en una máquina Linux. **Ten en cuenta que este método ha sido probado únicamente en Ubuntu 22.04.** Sin embargo, se recomienda utilizar el método de despliegue a través de GitLab CI/CD, ya que es más sencillo y no requiere instalación local de Terraform.

1. **Clonar el repositorio:**

   ```bash
   git clone https://gitlab.com/jls42/storypixai.git
   cd storypixai
   ```

2. **Configurar las variables de entorno:**

   * **AWS:**
        * Ejecuta `aws configure` y sigue las instrucciones para configurar tus credenciales de AWS (claves de acceso y clave secreta) y la región predeterminada.
   * **OpenAI (Opcional):**
        * Ejecuta `source export.sh` para cargar las funciones del script.
        * Ejecuta `manage_openai_key put <tu_clave_openai>` para almacenar tu clave de OpenAI en SSM Parameter Store.

## Despliegue vía Terraform

1. **Preparar el entorno AWS:**
    * Sigue los mismos pasos que para la configuración de las variables de entorno AWS mencionados anteriormente.

2. **Personalizar el script `export.sh`:**
   * Abre el archivo `export.sh` y modifica las siguientes variables para que coincidan con tu configuración:
      ```bash
      # el bucket se creará automáticamente si está disponible, elige un nombre único
      BUCKET_STATE="tu_nombre_de_bucket_para_el_estado_terraform"
      AWS_DEFAULT_REGION="tu_región_aws"
      ```

3. **Configurar Terraform:**
   * Cree un archivo `terraform.tfvars` en la raíz del directorio `terraform` y defina las variables específicas para su entorno, particularmente (vea la sección Documentación de Terraform-docs más abajo para más detalles):
      ```
      bucket_name = "su_nombre_del_bucket_s3"
      cognito_auth = {
        user     = "su_nombre_de_usuario" 
        password = "su_contraseña_segura"
      }
      ```

4. **Desplegar la infraestructura:**
   ```bash
   source export.sh
   terraform_plan
   terraform_apply
   ```
# Uso

1. **Autenticación:**
   - Abra su navegador web y acceda a la URL de su aplicación StoryPixAI.
   - Inicie sesión utilizando el nombre de usuario y la contraseña que definió en la variable `cognito_auth` durante el despliegue.

2. **Creación de una historia:**
   - Una vez conectado, accederá a la interfaz de generación de historias.

   - **Elija el idioma:** Seleccione el idioma deseado en el menú desplegable en la parte superior derecha de la pantalla. Las opciones disponibles son: francés, inglés, español, alemán, italiano y portugués.  

    
   - **Introduzca su idea de historia:** En el campo de texto previsto para ello, escriba algunas palabras o una frase que inspirarán la historia. Puede incluir nombres, lugares, personajes o cualquier otro elemento que considere relevante.
   - **Personalice la generación (opcional):**
      - **Modelo de historia:** Elija entre los modelos disponibles (OpenAI GPT-4-o, Anthropic Claude 3, Mistral AI).  
      - **Modelo de imagen:** Elija entre OpenAI DALL-E 3 o Stable Diffusion XL.  
      - **Tamaño de la imagen:** Seleccione el tamaño deseado para la ilustración (las opciones disponibles dependen del modelo de imagen elegido).
      - **Estilo de imagen:** Elija un estilo de la lista propuesta para personalizar la estética de la ilustración (solo para Stable Diffusion XL).  
      - **Calidad de la imagen:** Seleccione la calidad de la imagen generada por DALL-E 3 (estándar o HD).
      - **Seed:** Ingrese un número entero para inicializar el generador aleatorio y obtener resultados diferentes en cada generación.
      - **Activar/Desactivar la generación de imagen:** Marque o desmarque la casilla para activar o desactivar la generación de una ilustración para la historia.
   - **Inicie la generación:** Haga clic en el botón "Generar".

3. **Seguimiento del progreso:**
   - La interfaz mostrará un mensaje "Procesando...".

4. **Acceso a la historia generada:**
   - Una vez que la generación haya terminado, el mensaje "¡Listo!" aparecerá, seguido de un enlace único a la historia y la ilustración generadas.
   - Haga clic en el enlace para acceder a su creación.

5. **Consulta y compartición:**
   - Puede consultar la historia y la ilustración directamente en su navegador.
   - Puede copiar el enlace único y compartirlo con otras personas para que también puedan leer su creación.

**Cerrar sesión:**

* Haga clic en el botón "Cerrar sesión" para desconectarse de la aplicación.

# Documentación de Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requisitos

| Nombre | Versión |
|--------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Proveedores

| Nombre | Versión |
|--------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.53.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.2 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## Módulos

No hay módulos.

## Recursos | Nombre | Tipo |
|--------|------|
| [aws_api_gateway_account.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account) | recurso |
| [aws_api_gateway_authorizer.cognito_authorizer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | recurso |
| [aws_api_gateway_deployment.api_cors_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | recurso |
| [aws_api_gateway_deployment.api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | recurso |
| [aws_api_gateway_deployment.status_api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | recurso |
| [aws_api_gateway_integration.api_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration.lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration.status_lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration.status_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration_response.get_status_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_integration_response.integration_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_integration_response.integration_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_integration_response.status_options_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_method.api_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method.api_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method.status_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method.status_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method_response.get_status_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_method_response.method_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_method_response.method_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_method_response.status_options_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_resource.api_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | recurso |
| [aws_api_gateway_resource.status_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | recurso |
| [aws_api_gateway_rest_api.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | recurso | n.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | recurso |
| [aws_cloudfront_response_headers_policy.cors_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_response_headers_policy) | recurso |
| [aws_cloudwatch_log_group.api_gw_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | recurso |
| [aws_cloudwatch_log_group.lambda_ia](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | recurso |
| [aws_cloudwatch_log_group.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | recurso |
| [aws_cognito_identity_pool.identity_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool) | recurso |
| [aws_cognito_identity_pool_roles_attachment.identity_pool_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool_roles_attachment) | recurso |
| [aws_cognito_user.example_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user) | recurso |
| [aws_cognito_user_pool.user_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool) | recurso |
| [aws_cognito_user_pool_client.user_pool_client](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool_client) | recurso |
| [aws_dynamodb_table.task_status](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | recurso |
| [aws_iam_policy.lambda_dynamodb_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | recurso |
| [aws_iam_policy.lambda_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | recurso |
| [aws_iam_role.api_cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.auth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.lambda_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.status_checker_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.unauth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role_policy.auth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | recurso |
| [aws_iam_role_policy.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | recurso |
| [aws_iam_role_policy.unauth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | recurso |
| [aws_iam_role_policy_attachment.lambda_dynamodb_access_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | recurso |
| [aws_iam_role_policy_attachment.lambda_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | recurso |
| [aws_lambda_function.StoryPixAI](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | recurso |
| [aws_lambda_function.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | recurso |
| [aws_lambda_layer_version.openai_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version) | recurso |
| [aws_lambda_permission.api_gateway_permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | recurso |
| [aws_lambda_permission.status_api_gateway_permission]( https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | recurso |
| [aws_s3_bucket_acl.storypixai_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | recurso |
| [aws_s3_bucket_cors_configuration.storypixai_cors](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | recurso |
| [aws_s3_bucket_ownership_controls.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_ownership_controls) | recurso |
| [aws_s3_bucket_policy.storypixai_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | recurso |
| [aws_s3_bucket_public_access_block.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | recurso |
| [aws_s3_bucket_website_configuration.storypixai_website_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | recurso |
| [aws_s3_object.index_html](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | recurso |
| [aws_s3_object.script_js](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | recurso |
| [null_resource.prepare_lambda_deployment](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | recurso |
| [archive_file.StoryPixAI_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | fuente de datos |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | fuente de datos |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | fuente de datos |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | fuente de datos |
| [aws_iam_policy_document.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | fuente de datos |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | fuente de datos |
| [template_file.script_js](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | fuente de datos |

## Entradas

| Nombre | Descripción | Tipo | Predeterminado | Requerido |
|--------|-------------|------|---------------|:---------:|
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Nombre del bucket S3 para almacenar las imágenes | `string` | n/a | sí |
| <a name="input_cognito_auth"></a> [cognito\_auth](#input\_cognito\_auth) | Usuario y contraseña para autenticarse en el generador | `map(string)` | <pre>{<br>  "password": "_Ch4ng3M3_42",<br>  "user": "storypixia"<br>}</pre> | no |
| <a name="input_ia_environment_variables"></a> [ia\_environment\_variables](#input\_ia\_environment\_variables) | Variables de entorno para la función Lambda | `map(string)` | <pre>{<br>  "ANTHROPIC_STORY_MODEL": "anthropic.claude-3-sonnet-20240229-v1:0",<br>  "META_STORY_MODEL": "meta.llama3-70b-instruct-v1:0",<br>  "MISTRAL_STORY_MODEL": "mistral.mistral-large-2402-v1:0",<br>  "OPENAI_IMAGE_MODEL": "dall-e-3",<br>  "OPENAI_STORY_MODEL": "gpt-4o",<br>  "STABLE_DIFFUSION_IMAGE_MODEL": "stability.stable-diffusion-xl-v1",<br>  "TITAN_IMAGE_MODEL": "amazon.titan-image-generator-v1"<br>}</pre> | no |
| <a name="input_projet"></a> [projet](#input\_projet) | Nombre del proyecto | `string` | `"StoryPixAI"` | no |
| <a name="input_python_runtime"></a> [python\_runtime](#input\_python\_runtime) | Runtime para la función Lambda | `string` | `"python3.10"` | no |
| <a name="input_region"></a> [region](#input\_region) | Región AWS | `string` | `"us-east-1"` | no |

## Salidas | Nombre | Descripción |
|------|-------------|
| <a name="output_cloudfront"></a> [cloudfront](#output\_cloudfront) | n/a |
| <a name="output_s3_bucket_website_url"></a> [s3\_bucket\_website\_url](#output\_s3\_bucket\_website\_url) | Definición de la salida para la URL del sitio web alojado en S3 |
<!-- END_TF_DOCS -->

# Limitaciones

StoryPixAI es un proyecto que puede presentar ciertas limitaciones:

* **Calidad variable:** La calidad de las historias y las ilustraciones generadas puede variar en función de los modelos de IA utilizados, de los prompts proporcionados y otros factores.
* **Costo:** El uso de las API de OpenAI y Bedrock puede generar costos, dependiendo de su uso.

# Contribuir

StoryPixAI es un proyecto de código abierto. Si desea contribuir, aquí hay algunas ideas:

* **Informar un error:** Si encuentra un problema o un error, por favor ábralo en la sección "Issues" del repositorio de GitLab.
* **Proponer una mejora:** Si tiene una idea de mejora o nueva funcionalidad, no dude en enviar una "Merge Request" con su código.
* **Mejorar la documentación:** Si encuentra errores o imprecisiones en el README, no dude en corregirlos.

¡Gracias por su interés y apoyo a StoryPixAI!

# Autor

Julien LE SAUX  
Sitio web: https://jls42.org  
Correo electrónico: contact@jls42.org  

# Descargo de responsabilidad

El uso de este software es bajo su propio riesgo. El autor no puede ser responsable de los costos, daños o pérdidas resultantes de su uso. Por favor, asegúrese de comprender las tarifas asociadas con el uso de los servicios de AWS antes de desplegar este proyecto.

# Licencia

Este proyecto tiene licencia MIT con Common Clause. Esto significa que eres libre de usar, modificar y distribuir el software, pero no puedes venderlo ni usarlo para ofrecer servicios comerciales. Para más detalles, por favor consulte el archivo [LICENSE](LICENSE).

# Ejemplos de historias generadas

## Francés
### Zoé et Tom
![alt text](/img/zoe_tom_fr.png)

## Inglés
### Léa
![alt text](/img/lea_en.png)

## Español
### Roger
![alt text](/img/roger_es.png)

**Este documento ha sido traducido de la versión fr al idioma es utilizando el modelo gpt-4o. Para más información sobre el proceso de traducción, consulte https://gitlab.com/jls42/ai-powered-markdown-translator**

