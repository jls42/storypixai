# Obtenir les informations sur l'identité de l'appelant actuel
data "aws_caller_identity" "current" {}

# Obtenir la région AWS actuelle
data "aws_region" "current" {}
