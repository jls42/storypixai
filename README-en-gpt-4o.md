[[_TOC_]]

# StoryPixAI: AI-generated and illustrated bedtime stories for children

StoryPixAI is an innovative serverless project for generating magical bedtime stories created by AI. Simply write a few words on the theme of your choice and let the artificial intelligence weave a unique and captivating bedtime story, accompanied by an adorable illustration. Everything is automatically generated in the language of your choice thanks to cutting-edge AI models from OpenAI and Amazon Bedrock. Give your children a magical moment before falling asleep while enjoying a smooth user experience thanks to the power of the AWS serverless infrastructure.

## Features

* **AI-created personalized bedtime stories:** Transform your children's ideas into original bedtime stories using the power of OpenAI and AWS Bedrock AI models.
* **Adorable AI-generated illustrations:** Each story comes to life with cute and colorful images, perfect for stimulating your children's imagination.
* **Automatic cloud storage on AWS:** StoryPixAI automatically saves your stories and illustrations in an S3 bucket, accessible via a unique link to re-read them anytime.
* **Secure access to the generator:** The story creation interface is protected by authentication, ensuring a private and secure creation space.
* **Multilingual:** StoryPixAI supports the following languages:
    * French (mainly tested)
    * English
    * Spanish
    * German
    * Italian
    * Portuguese

# Architecture

StoryPixAI is based on a serverless architecture:

## Serverless Infrastructure Architecture Diagram
![alt text](/img/storypixai-architecture.png)

## Frontend

* **Generation Interface (`index.html`):** The StoryPixAI user interface is a Single Page Application (SPA) developed in pure HTML, CSS, and JavaScript (Vanilla JavaScript). It offers an intuitive and interactive user experience for:  
    ![alt text](/img/generateur.png)

    * **Authentication:** Users must log in with their Cognito credentials to access generation features. A login modal window is provided to facilitate this process.

    * **Language selection:** A dropdown menu allows the selection of the generator's and story's language from those supported: French, English, Spanish, German, Italian, and Portuguese. The selected language is used for the user interface and to guide story generation.  
        ![alt text](/img/langues.png)
    * **Prompt entry:** A text field allows the user to enter the idea or theme of the desired story.

    * **Generation customization (optional):** The interface offers options to customize the generation:
        * **Choice of text generation model:** OpenAI GPT-4-o, Anthropic Claude 3, or Mistral AI.  
            ![alt text](/img/texte_modele.png)
        * **Choice of image generation model:** OpenAI DALL-E 3 or Stable Diffusion XL.  
            ![alt text](/img/image_modele.png)
        * **Specific image model parameters:** Size, style (for Stable Diffusion XL), and quality (for DALL-E 3).  
            ![alt text](/img/styles.png)  
        * **Seed:** A numeric field allows specifying a value to initialize the random generator.
        * **Enable/disable image generation.**

    * **Launch generation:** A "Generate" button triggers the story and image generation process (if enabled).

    * **Progress tracking:** A status message informs the user of the generation progress ("Processing...", "Ready!", etc.) and potential errors.

    * **Result display:** A unique link to the generated story and image is displayed when the generation is complete. This link can be copied and shared. * **Authentication and Request Management:**
    * The `storypixai.js` script manages user authentication with Amazon Cognito and uses the obtained access token to authorize requests to API Gateway.
    * It also manages communication with the API Gateway endpoints (`/generate` and `/check_status/{task_id}`) to trigger generation and track progress.
    * Error handling and retry mechanisms are also implemented to ensure the application's reliability.

* **Hosting and Distribution:**
    * The generation interface (`index.html`) is hosted on Amazon S3 and distributed via Amazon CloudFront, a content delivery network (CDN) that improves the application's performance and availability for users worldwide.



## Backend

* **AWS Lambda Functions (Python):**
    * **`StoryPixAI`:** This function is the heart of the application. It takes a text prompt provided by the user through the generation interface as input and orchestrates the process of creating the story and illustration. It successively calls OpenAI and/or Bedrock APIs to generate the story text and the associated image. Then, it stores the image in the S3 bucket and returns a unique link to the user for the generated story and image. The generation progress is stored in DynamoDB.

    * **`status_checker`:** This function is periodically called by the generation interface to check the progress of story and image creation. It queries DynamoDB to retrieve the status of the current task and returns this status to the frontend. This allows the generation interface to keep the user informed of the progress and indicate when the story and image are ready.

* **Amazon DynamoDB:** This NoSQL key-value database is used to store information related to story and image generation tasks. Each task is identified by a unique `requestId`. The DynamoDB table contains the following attributes:
    * **`requestId` (hash key):** Unique identifier for the task.
    * **`status`:** Task status (in progress, completed, error).
    * **`resultUrl`:** URL of the generated story (when the task is completed).

    The DynamoDB table also has two global secondary indexes:

    * **`StatusIndex`:** Allows searching for tasks based on their status.
    * **`ResultUrlIndex`:** Allows searching for tasks based on the URL of the generated story.

    The `status_checker` Lambda function uses these indexes to efficiently retrieve the status and URL of a given task's story. The frontend JavaScript script then queries the API Gateway endpoint `/check_status/{task_id}` to obtain this information and update the user interface accordingly.

* **Amazon S3:** This object storage service hosts the static web interface (`index.html`), generated images, and generated stories. The stories and images are stored with unique keys generated by the `StoryPixAI` function, and these keys are returned to the user to access their creations.

* **Amazon CloudWatch:** This monitoring service collects logs and metrics from the Lambda functions, allowing the tracking of the application's performance, detecting errors, and troubleshooting.

# Infrastructure as Code (IaC)

* **Terraform:** The entire AWS infrastructure (Lambda, API Gateway, S3, CloudFront, Cognito, DynamoD, ...) is defined and deployed using Terraform. This allows for automated, reproducible, and versioned infrastructure management, facilitating updates and changes.

# Security and Authentication

* **Amazon Cognito:** This service manages user authentication. The generation interface (`index.html`) uses the Cognito API to authenticate the user and obtain an access token. This token is then included in requests to the endpoints `/generate` and `/check_status/{task_id}` to authorize access to the `StoryPixAI` and `status_checker` functions respectively.

# Content Distribution

* **Amazon CloudFront:** This content delivery network (CDN) service is used to distribute the static web interface (`index.html`) quickly and reliably to users around the world. It caches content on geographically distributed servers, thus reducing latency and improving user experience.

# AI Models

StoryPixAI is compatible with a combination of cutting-edge AI models to generate high-quality stories and illustrations:

* **OpenAI Models:**
    * **Story Generation:**
        * OpenAI GPT-4-o
    * **Image Generation:**
        * OpenAI DALL-E 3

* **Models Accessible via AWS Bedrock:**
    * **Story Generation:**
        * Anthropic Claude 3 Sonnet (20240229-v1)
        * Mistral AI Mistral Large (2402-v1)
    * **Image Generation:**
        * StabilityAI Stable Diffusion XL v1

# Prerequisites

Before starting to use StoryPixAI, make sure you have the following items:

* **AWS Account:** You will need an active AWS account to deploy and manage the serverless infrastructure. Ensure that your IAM user has the following permissions (or equivalent). **Note that this policy is an example and may be tailored to your specific security needs:**

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "cloudwatch:*",
            "ec2:*",
            "lambda:*",
            "s3:*",
            "ssm:*",
            "logs:*",
            "dynamodb:*",
            "cognito-idp:*",
            "cognito-identity:*",
            "apigateway:*",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:AttachRolePolicy",
            "iam:DetachRolePolicy",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:CreatePolicy",
            "iam:DeletePolicy",
            "iam:PassRole",
            "iam:Tag*",
            "iam:Get*",
            "iam:List*",
            "iam:TagRole",
            "kms:TagResource",
            "iam:CreateServiceLinkedRole",
            "budgets:*",
            "events:*"
          ],
          "Resource": "*"
        }
      ]
    }
    ```

* **Activation of Bedrock Models:** Ensure that the following AI models are activated in your AWS Bedrock account:
    * Anthropic Claude 3 Sonnet (20240229-v1)
    * Mistral AI Mistral Large (2402-v1)
    * StabilityAI Stable Diffusion XL v1

[Accès aux modèles Bedrock - us-east-1](https://us-east-1.console.aws.amazon.com/bedrock/home?region=us-east-1#/modelaccess)
![alt text](/img/modele_1.png)
![alt text](/img/modele_2.png)

* **OpenAI API Keys (if you wish to use OpenAI models)** To generate high-quality illustrations with DALL-E 3, it is highly recommended to have a valid OpenAI API key. You can obtain your key on the OpenAI website. Note that StoryPixAI can also generate images with Stable Diffusion XL v1 (via AWS Bedrock), but DALL-E 3 generally offers superior results.

**To deploy via GitLab CI/CD:**

* **GitLab Account:** You will need a valid GitLab account and access to the StoryPixAI project on GitLab.
* **GitLab Environment Variables:** You will need to configure the necessary environment variables in your GitLab project, including AWS and OpenAI API keys (if you use DALL-E 3).

# Installation and Deployment

## Deployment via GitLab CI/CD (Recommended)

StoryPixAI is designed to be easily deployed and updated thanks to an integrated GitLab CI/CD pipeline. This deployment method is **highly recommended** as it is automated, reliable, and independent of your operating system (Linux, macOS, Windows). The pipeline automates the steps of environment preparation, configuration verification, deployment, and infrastructure deletion.

**Pipeline Execution:**

1. **Fork the project:** Create a copy of the project in your own GitLab space by going to the following URL: [https://gitlab.com/jls42/storypixai](https://gitlab.com/jls42/storypixai)
2. **Clone the repository:**
   ```bash
   git clone https://gitlab.com/your_username/storypixai.git # Replace "your_username" with your GitLab username
   cd storypixai
   ```
3. **Set up environment variables:**
   - Go to the CI/CD settings of your project.
   - Define the following variables:
     * `AWS_ACCESS_KEY_ID`: Your AWS access key.
     * `AWS_SECRET_ACCESS_KEY`: Your AWS secret key.
     * `OPENAI_KEY` (recommended): Your OpenAI API key (if using DALL-E 3 or GPT4-o).
     * `TF_VAR_cognito_auth`: A variable containing the Cognito configuration in JSON format, for example:
       ```json
       {"user": "your_username", "password": "your_secure_password"}
       ```

4. **Customize the `export.sh` script:**
   * Open the `export.sh` file and modify the following variables to match your configuration:
      ```bash
      # the bucket will be created automatically if available, choose a unique name
      BUCKET_STATE="your_terraform_state_bucket_name"
      AWS_DEFAULT_REGION="your_aws_region"
      ```

5. **Set up Terraform:**
   * Create a `terraform.tfvars` file at the root of the `terraform` directory and define the environment-specific variables, including (see the Terraform-docs section below for more details):
      ```
      bucket_name = "your_s3_bucket_name"

6. **Trigger the pipeline:**
   - Go to the CI/CD section of your GitLab project.
   - Choose the step you wish to execute (e.g., `Terraform Verify` or `Terraform Deploy`).
   - Click the "Play" button to start the selected step.   
    ![alt text](/img/cicd.png)

**Note:** If you use the CI/CD provided with the project, installing Terraform is not necessary. Deployment will be done automatically via GitLab CI/CD after configuring the required environment variables.

## Installation on a Linux Machine (Ubuntu 22.04) (Optional)

If you prefer to deploy StoryPixAI manually, you can do so on a Linux machine. **Note that this method has been tested only on Ubuntu 22.04.** However, it is recommended to use the GitLab CI/CD deployment method, as it is simpler and does not require a local installation of Terraform.

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/jls42/storypixai.git
   cd storypixai
   ```

2. **Set up environment variables:**

   * **AWS:**
        * Run `aws configure` and follow the instructions to set up your AWS credentials (access keys and secret key) and default region.
   * **OpenAI (Optional):**
        * Run `source export.sh` to load the script's functions.
        * Run `manage_openai_key put <your_openai_key>` to store your OpenAI key in SSM Parameter Store.

## Deployment via Terraform

1. **Prepare the AWS environment:**
    * Follow the same steps as for configuring the AWS environment variables above.

2. **Customize the `export.sh` script:**
   * Open the `export.sh` file and modify the following variables to match your configuration:
      ```bash
      # the bucket will be created automatically if available, choose a unique name
      BUCKET_STATE="your_terraform_state_bucket_name"
      AWS_DEFAULT_REGION="your_aws_region"
      ``` **Configure Terraform:**
   * Create a `terraform.tfvars` file at the root of the `terraform` directory and define the variables specific to your environment, including (see the Terraform-docs section below for more details):
      ```
      bucket_name = "your_s3_bucket_name"
      cognito_auth = {
        user     = "your_username" 
        password = "your_secure_password"
      }
      ```

4. **Deploy the Infrastructure:**
   ```bash
   source export.sh
   terraform_plan
   terraform_apply
   ```
# Usage

1. **Authentication:**
   - Open your web browser and go to the URL of your StoryPixAI application.
   - Log in using the username and password you defined in the `cognito_auth` variable during deployment.

2. **Creating a Story:**
   - Once logged in, you will access the story generation interface.

   - **Choose the Language:** Select the desired language from the dropdown menu at the top right of the screen. The available options are: French, English, Spanish, German, Italian, and Portuguese.  

    
   - **Enter Your Story Idea:** In the designated text field, enter a few words or a sentence to inspire the story. You can include names, places, characters, or any other element that seems relevant to you.
   - **Customize the Generation (optional):**
      - **Story Model:** Choose from the available models (OpenAI GPT-4-o, Anthropic Claude 3, Mistral AI).  
      - **Image Model:** Choose between OpenAI DALL-E 3 or Stable Diffusion XL.  
      - **Image Size:** Select the desired size for the illustration (available options depend on the chosen image model).
      - **Image Style:** Choose a style from the provided list to customize the illustration's aesthetics (only for Stable Diffusion XL).  
      - **Image Quality:** Select the image quality generated by DALL-E 3 (standard or HD).
      - **Seed:** Enter an integer to initialize the random generator and get different results with each generation.
      - **Enable/Disable Image Generation:** Check or uncheck the box to enable or disable the generation of an illustration for the story.
   - **Launch the Generation:** Click the "Generate" button.

3. **Tracking Progress:**
   - The interface will display a "Processing..." message.

4. **Accessing the Generated Story:**
   - Once the generation is complete, the "Ready!" message will appear, followed by a unique link to the generated story and illustration.
   - Click the link to access your creation.

5. **Viewing and Sharing:**
   - You can view the story and illustration directly in your browser.
   - You can copy the unique link and share it with others so they can also read your creation.

**Logout:**

* Click the "Logout" button to log out of the application.

# Terraform-docs Documentation  
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.53.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.2 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## Modules

No modules.

## Resources | Name | Type |
|------|------|
| [aws_api_gateway_account.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account) | resource |
| [aws_api_gateway_authorizer.cognito_authorizer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | resource |
| [aws_api_gateway_deployment.api_cors_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | resource |
| [aws_api_gateway_deployment.api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | resource |
| [aws_api_gateway_deployment.status_api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | resource |
| [aws_api_gateway_integration.api_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration.lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration.status_lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration.status_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration_response.get_status_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_integration_response.integration_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_integration_response.integration_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_integration_response.status_options_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_method.api_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method.api_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method.status_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method.status_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method_response.get_status_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_method_response.method_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_method_response.method_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_method_response.status_options_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_resource.api_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | resource |
| [aws_api_gateway_resource.status_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | resource |
| [aws_api_gateway_rest_api.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | resource | https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource | https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_acl.storypixai_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_cors_configuration.storypixai_cors](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | resource |
| [aws_s3_bucket_ownership_controls.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_ownership_controls) | resource |
| [aws_s3_bucket_policy.storypixai_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_website_configuration.storypixai_website_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | resource |
| [aws_s3_object.index_html](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | resource |
| [aws_s3_object.script_js](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | resource |
| [null_resource.prepare_lambda_deployment](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [archive_file.StoryPixAI_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [template_file.script_js](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Name of the S3 bucket to store images | `string` | n/a | yes |
| <a name="input_cognito_auth"></a> [cognito\_auth](#input\_cognito\_auth) | Login and password to authenticate on the generator | `map(string)` | <pre>{<br>  "password": "_Ch4ng3M3_42",<br>  "user": "storypixia"<br>}</pre> | no |
| <a name="input_ia_environment_variables"></a> [ia\_environment\_variables](#input\_ia\_environment\_variables) | Environment variables for the Lambda function | `map(string)` | <pre>{<br>  "ANTHROPIC_STORY_MODEL": "anthropic.claude-3-sonnet-20240229-v1:0",<br>  "META_STORY_MODEL": "meta.llama3-70b-instruct-v1:0",<br>  "MISTRAL_STORY_MODEL": "mistral.mistral-large-2402-v1:0",<br>  "OPENAI_IMAGE_MODEL": "dall-e-3",<br>  "OPENAI_STORY_MODEL": "gpt-4o",<br>  "STABLE_DIFFUSION_IMAGE_MODEL": "stability.stable-diffusion-xl-v1",<br>  "TITAN_IMAGE_MODEL": "amazon.titan-image-generator-v1"<br>}</pre> | no |
| <a name="input_projet"></a> [projet](#input\_projet) | Project name | `string` | `"StoryPixAI"` | no |
| <a name="input_python_runtime"></a> [python\_runtime](#input\_python\_runtime) | Runtime for the Lambda function | `string` | `"python3.10"` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS Region | `string` | `"us-east-1"` | no |

## Outputs | Name | Description |
|------|-------------|
| <a name="output_cloudfront"></a> [cloudfront](#output\_cloudfront) | n/a |
| <a name="output_s3_bucket_website_url"></a> [s3\_bucket\_website\_url](#output\_s3\_bucket\_website\_url) | Definition of the output for the URL of the website hosted on S3 |
<!-- END_TF_DOCS -->

# Limitations

StoryPixAI is a project that may have some limitations:

* **Variable quality:** The quality of generated stories and illustrations can vary depending on the AI models used, the prompts provided, and other factors.
* **Cost:** Using the OpenAI and Bedrock APIs may incur costs, depending on your usage.

# Contributing

StoryPixAI is an open source project. If you wish to contribute, here are some ways:

* **Report a bug:** If you encounter an issue or a bug, please open it in the "Issues" section of the GitLab repository.
* **Suggest an improvement:** If you have an idea for an enhancement or a new feature, feel free to submit a "Merge Request" with your code.
* **Improve the documentation:** If you find errors or inaccuracies in the README, feel free to correct them.

Thank you for your interest and support for StoryPixAI!

# Author

Julien LE SAUX  
Website: https://jls42.org  
Email: contact@jls42.org  

# Disclaimer

Use of this software is at your own risk. The author cannot be held responsible for costs, damages, or losses resulting from its use. Please ensure you understand the fees associated with using AWS services before deploying this project.

# License

This project is licensed under the MIT License with a Common Clause. This means you are free to use, modify, and distribute the software, but you cannot sell it or use it to offer commercial services. For more details, please see the [LICENSE](LICENSE) file.

# Examples of Generated Stories

## French
### Zoé and Tom
![alt text](/img/zoe_tom_fr.png)

## English
### Léa
![alt text](/img/lea_en.png)

## Spanish
### Roger
![alt text](/img/roger_es.png)

**This document has been translated from the fr version to the en language using the gpt-4o model. For more information on the translation process, visit https://gitlab.com/jls42/ai-powered-markdown-translator**

