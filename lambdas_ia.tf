# Archive du répertoire de la fonction Lambda en un fichier zip
data "archive_file" "StoryPixAI_zip" {
  type        = "zip"
  source_dir  = "${path.module}/lambdas/StoryPixAI"
  output_path = "${path.module}/lambdas/StoryPixAI.zip"
}

# Définition de la fonction Lambda StoryPixAI
resource "aws_lambda_function" "StoryPixAI" {
  function_name    = "StoryPixAI"
  handler          = "StoryPixAI.lambda_handler"
  runtime          = var.python_runtime
  role             = aws_iam_role.lambda_exec_role.arn
  timeout          = 300
  filename         = data.archive_file.StoryPixAI_zip.output_path
  source_code_hash = data.archive_file.StoryPixAI_zip.output_base64sha256

  layers = [aws_lambda_layer_version.openai_layer.arn]

  environment {
    variables = {
      CLOUDFRONT_DOMAIN            = aws_cloudfront_distribution.storypixai.domain_name
      BUCKET_NAME                  = var.bucket_name
      TITAN_IMAGE_MODEL            = var.ia_environment_variables["TITAN_IMAGE_MODEL"]
      STABLE_DIFFUSION_IMAGE_MODEL = var.ia_environment_variables["STABLE_DIFFUSION_IMAGE_MODEL"]
      OPENAI_IMAGE_MODEL           = var.ia_environment_variables["OPENAI_IMAGE_MODEL"]
      ANTHROPIC_STORY_MODEL        = var.ia_environment_variables["ANTHROPIC_STORY_MODEL"]
      OPENAI_STORY_MODEL           = var.ia_environment_variables["OPENAI_STORY_MODEL"]
      MISTRAL_STORY_MODEL          = var.ia_environment_variables["MISTRAL_STORY_MODEL"]
      META_STORY_MODEL             = var.ia_environment_variables["META_STORY_MODEL"]
    }
  }
}

# Création du rôle IAM pour la fonction Lambda
resource "aws_iam_role" "lambda_exec_role" {
  name = "lambda_execution_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# Définition de la politique IAM pour la fonction Lambda
resource "aws_iam_policy" "lambda_policy" {
  name = "lambda_policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::${var.bucket_name}/*"
      },
      {
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:logs:*:*:*"
      },
      {
        Action = "bedrock:InvokeModel",
        Effect = "Allow",
        Resource = [
          "arn:aws:bedrock:${data.aws_region.current.name}::foundation-model/${var.ia_environment_variables["TITAN_IMAGE_MODEL"]}",
          "arn:aws:bedrock:${data.aws_region.current.name}::foundation-model/${var.ia_environment_variables["STABLE_DIFFUSION_IMAGE_MODEL"]}",
          "arn:aws:bedrock:${data.aws_region.current.name}::foundation-model/${var.ia_environment_variables["MISTRAL_STORY_MODEL"]}",
          "arn:aws:bedrock:${data.aws_region.current.name}::foundation-model/${var.ia_environment_variables["ANTHROPIC_STORY_MODEL"]}",
          "arn:aws:bedrock:${data.aws_region.current.name}::foundation-model/${var.ia_environment_variables["META_STORY_MODEL"]}"
        ]
      },
      {
        Effect = "Allow",
        Action = "ssm:GetParameter",
        Resource = [
          "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/openaikey"
        ]
      },
      {
        Action = [
          "dynamodb:PutItem",
          "dynamodb:UpdateItem"
        ]
        Effect   = "Allow"
        Resource = aws_dynamodb_table.task_status.arn
      },
    ]
  })
}

# Attachement de la politique IAM au rôle IAM
resource "aws_iam_role_policy_attachment" "lambda_policy_attachment" {
  role       = aws_iam_role.lambda_exec_role.name
  policy_arn = aws_iam_policy.lambda_policy.arn
}

# Préparation de l'environnement de déploiement de la couche Lambda
resource "null_resource" "prepare_lambda_deployment" {
  provisioner "local-exec" {
    command = <<EOF
    mkdir -p ${path.module}/.tmp_lambda_layer/python
    pip install -r ${path.module}/requirements.txt -t ${path.module}/.tmp_lambda_layer/python
    cd ${path.module}/.tmp_lambda_layer && zip -r ../lambdas/openai_layer.zip .
    rm -rf ${path.module}/.tmp_lambda_layer
    EOF
  }
}

# Définition de la version de la couche Lambda pour OpenAI
resource "aws_lambda_layer_version" "openai_layer" {
  depends_on          = [null_resource.prepare_lambda_deployment]
  layer_name          = "openai_layer"
  compatible_runtimes = [var.python_runtime]
  filename            = "${path.module}/lambdas/openai_layer.zip"
}
