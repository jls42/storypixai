[[_TOC_]]

# StoryPixAI: Histórias para dormir para crianças geradas e ilustradas pela IA

StoryPixAI é um projeto serverless inovador de geração de histórias mágicas para dormir, criadas pela IA. Basta escrever algumas palavras sobre o tema de sua escolha e deixar a inteligência artificial tecer uma história única e cativante para dormir, acompanhada de uma ilustração adorável. Tudo é gerado automaticamente no idioma de sua escolha graças aos modelos de ponta da IA da OpenAI e Amazon Bedrock. Proporcione aos seus filhos um momento mágico antes de dormir, enquanto aproveita uma experiência de usuário fluida graças ao poder da infraestrutura AWS serverless.

## Funcionalidades

* **Histórias personalizadas criadas pela IA para dormir:** Transforme as ideias dos seus filhos em histórias originais para dormir, graças ao poder dos modelos de IA da OpenAI e AWS Bedrock.
* **Ilustrações adoráveis geradas pela IA:** Cada história ganha vida com imagens fofas e coloridas, perfeitas para estimular a imaginação dos seus filhos.
* **Salvamento automático no AWS Cloud:** O StoryPixAI salva automaticamente suas histórias e ilustrações em um bucket S3, acessível via um link único para lê-los a qualquer momento.
* **Acesso seguro ao gerador:** A interface de criação de histórias é protegida por autenticação, garantindo assim um espaço de criação privado e seguro.
* **Multilíngue:** StoryPixAI suporta os seguintes idiomas:
    * Francês (principalmente testado)
    * Inglês
    * Espanhol
    * Alemão
    * Italiano
    * Português

# Arquitetura

StoryPixAI baseia-se em uma arquitetura serverless:

## Diagrama de arquitetura da infraestrutura serverless
![alt text](/img/storypixai-architecture.png)

## Frontend

* **Interface de geração (`index.html`):** A interface de usuário do StoryPixAI é uma Single Page Application (SPA) desenvolvida em HTML, CSS e JavaScript puro (Vanilla JavaScript). Ela oferece uma experiência de usuário intuitiva e interativa para:  
    ![alt text](/img/generateur.png)

    * **Autenticação:** Os usuários devem se conectar com suas credenciais Cognito para acessar as funcionalidades de geração. Uma janela modal de login é fornecida para facilitar esse processo.

    * **Seleção de idioma:** Um menu suspenso permite escolher o idioma do gerador e da história entre os suportados: francês, inglês, espanhol, alemão, italiano e português. O idioma selecionado é usado para a interface de usuário e para guiar a geração da história.  
        ![alt text](/img/langues.png)
    * **Entrada do prompt:** Um campo de texto permite que o usuário insira a ideia ou o tema desejado para a história.

    * **Personalização da geração (opcional):** A interface oferece opções para personalizar a geração:
        * **Escolha do modelo de geração de texto:** OpenAI GPT-4-o, Anthropic Claude 3, ou Mistral AI.  
            ![alt text](/img/texte_modele.png)
        * **Escolha do modelo de geração de imagem:** OpenAI DALL-E 3 ou Stable Diffusion XL.  
            ![alt text](/img/image_modele.png)
        * **Parâmetros específicos para o modelo de imagem:** Tamanho, estilo (para Stable Diffusion XL) e qualidade (para DALL-E 3).  
            ![alt text](/img/styles.png)  
        * **Seed:** Um campo numérico permite especificar um valor para inicializar o gerador aleatório.
        * **Ativação/desativação da geração de imagem.**

    * **Iniciar a geração:** Um botão "Gerar" inicia o processo de geração da história e da imagem (se ativada).

    * **Acompanhar o progresso:** Uma mensagem de status informa o usuário sobre o progresso da geração ("Processando...", "Pronto!", etc.) e possíveis erros.

    * **Exibição do resultado:** Um link único para a história e imagem geradas é exibido quando a geração é concluída. Este link pode ser copiado e compartilhado. * **Gerenciamento de autenticação e solicitações:**
    * O script `storypixai.js` gerencia a autenticação do usuário com Amazon Cognito e usa o token de acesso obtido para autorizar as solicitações ao API Gateway.
    * Ele também gerencia a comunicação com os endpoints do API Gateway (`/generate` e `/check_status/{task_id}`) para desencadear a geração e acompanhar o progresso.
    * Mecanismos de gerenciamento de erros e novas tentativas também são implementados para garantir a confiabilidade do aplicativo.

* **Hospedagem e distribuição:**
    * A interface de geração (`index.html`) é hospedada no Amazon S3 e distribuída via Amazon CloudFront, uma rede de entrega de conteúdo (CDN) que melhora o desempenho e a disponibilidade do aplicativo para usuários em todo o mundo.


## Backend

* **Funções AWS Lambda (Python):**
    * **`StoryPixAI`:** Esta função é o coração do aplicativo. Ela recebe de entrada um prompt textual fornecido pelo usuário através da interface de geração e orquestra o processo de criação da história e da ilustração. Ela chama sucessivamente as APIs da OpenAI e/ou Bedrock para gerar o texto da história e a imagem associada. Em seguida, armazena a imagem no bucket S3 e retorna ao usuário um link único para a história e a imagem geradas. O progresso da geração é armazenado no DynamoDB.

    * **`status_checker`:** Esta função é chamada periodicamente pela interface de geração para verificar o andamento da criação da história e da imagem. Ela consulta o DynamoDB para recuperar o status da tarefa em andamento e retorna esse status para o frontend. Isso permite que a interface de geração mantenha o usuário informado sobre o progresso e indique quando a história e a imagem estão prontas.

* **Amazon DynamoDB:** Este banco de dados NoSQL chave-valor é usado para armazenar informações relacionadas às tarefas de geração de histórias e imagens. Cada tarefa é identificada por um `requestId` único. A tabela DynamoDB contém os seguintes atributos:
    * **`requestId` (chave de hash):** Identificador único da tarefa.
    * **`status`:** Status da tarefa (em andamento, concluída, erro).
    * **`resultUrl`:** URL da história gerada (quando a tarefa está concluída).

    A tabela DynamoDB também possui dois índices secundários globais:

    * **`StatusIndex`:** Permite pesquisar as tarefas com base no status.
    * **`ResultUrlIndex`:** Permite pesquisar as tarefas com base na URL da história gerada.

    A função Lambda `status_checker` usa esses índices para recuperar eficientemente o status e a URL da história de uma dada tarefa. O script JavaScript do frontend consulta em seguida o endpoint `/check_status/{task_id}` do API Gateway para obter essas informações e atualizar a interface do usuário em consequência.

* **Amazon S3:** Este serviço de armazenamento de objetos hospeda a interface web estática (`index.html`), as imagens geradas e as histórias geradas. As histórias e as imagens são armazenadas com chaves únicas geradas pela função `StoryPixAI`, e essas chaves são retornadas ao usuário para permitir que ele acesse suas criações.

* **Amazon CloudWatch:** Este serviço de monitoramento coleta os logs e métricas das funções Lambda, permitindo acompanhar o desempenho do aplicativo, detectar erros e resolver problemas.

# Infrastructure as Code (IaC)

* **Terraform:** Toda a infraestrutura AWS (Lambda, API Gateway, S3, CloudFront, Cognito, DynamoD, ...) é definida e implantada usando Terraform. Isso permite um gerenciamento automatizado, reproduzível e versionado da infraestrutura, facilitando atualizações e modificações.

# Segurança e autenticação

* **Amazon Cognito:** Este serviço gerencia a autenticação do usuário. A interface de geração (`index.html`) usa a API do Cognito para autenticar o usuário e obter um token de acesso. Este token é então incluído nas requisições aos endpoints `/generate` e `/check_status/{task_id}` para autorizar o acesso às funções `StoryPixAI` e `status_checker` respectivamente.

# Distribuição de conteúdo

* **Amazon CloudFront :** Este serviço de rede de distribuição de conteúdo (CDN) é utilizado para distribuir a interface web estática (`index.html`) de maneira rápida e confiável aos usuários do mundo inteiro. Ele armazena em cache o conteúdo em servidores distribuídos geograficamente, reduzindo assim a latência e melhorando a experiência do usuário.

# Modelos de IA

StoryPixAI é compatível com uma combinação de modelos de IA de ponta para gerar histórias e ilustrações de alta qualidade:

* **Modelos OpenAI :**
    * **Geração de histórias :**
        * OpenAI GPT-4-o
    * **Geração de imagens :**
        * OpenAI DALL-E 3

* **Modelos acessíveis via AWS Bedrock:**
    * **Geração de histórias :**
        * Anthropic Claude 3 Sonnet (20240229-v1)
        * Mistral AI Mistral Large (2402-v1)
    * **Geração de imagens :**
        * StabilityAI Stable Diffusion XL v1


# Pré-requisitos

Antes de começar a utilizar o StoryPixAI, certifique-se de ter os seguintes elementos:

* **Conta AWS:** Você precisará de uma conta AWS ativa para implantar e gerenciar a infraestrutura serverless. Certifique-se de que o seu usuário IAM tenha as seguintes permissões (ou equivalentes). **Note que esta policy é um exemplo e pode ser adaptada às suas necessidades específicas de segurança:**

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "cloudwatch:*",
            "ec2:*",
            "lambda:*",
            "s3:*",
            "ssm:*",
            "logs:*",
            "dynamodb:*",
            "cognito-idp:*",
            "cognito-identity:*",
            "apigateway:*",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:AttachRolePolicy",
            "iam:DetachRolePolicy",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:CreatePolicy",
            "iam:DeletePolicy",
            "iam:PassRole",
            "iam:Tag*",
            "iam:Get*",
            "iam:List*",
            "iam:TagRole",
            "kms:TagResource",
            "iam:CreateServiceLinkedRole",
            "budgets:*",
            "events:*"
          ],
          "Resource": "*"
        }
      ]
    }
    ```

* **Ativação dos modelos Bedrock:** Certifique-se de que os seguintes modelos de IA estão ativados na sua conta AWS Bedrock:
    * Anthropic Claude 3 Sonnet (20240229-v1)
    * Mistral AI Mistral Large (2402-v1)
    * StabilityAI Stable Diffusion XL v1

[Acesso aos modelos Bedrock - us-east-1](https://us-east-1.console.aws.amazon.com/bedrock/home?region=us-east-1#/modelaccess)
![alt text](/img/modele_1.png)
![alt text](/img/modele_2.png)


* **Chaves de API da OpenAI (se você desejar utilizar os modelos da OpenAI)** Para gerar ilustrações de alta qualidade com o DALL-E 3, é altamente recomendado ter uma chave de API OpenAI válida. Você pode obter sua chave no site da OpenAI. Note que o StoryPixAI também pode gerar imagens com o Stable Diffusion XL v1 (via AWS Bedrock), mas o DALL-E 3 geralmente oferece resultados superiores.

**Para implantar via GitLab CI/CD:**

* **Conta GitLab:** Você precisará de uma conta GitLab válida e acesso ao projeto StoryPixAI no GitLab.
* **Variáveis de Ambiente do GitLab:** Você deverá configurar as variáveis de ambiente necessárias no seu projeto GitLab, incluindo as chaves de API da AWS e da OpenAI (se você usar o DALL-E 3).

# Instalação e implantação

## Implantação via GitLab CI/CD (Recomendado)

StoryPixAI é projetado para ser facilmente implantado e atualizado através de um pipeline GitLab CI/CD integrado. Esse método de implantação é **fortemente recomendado**, pois é automatizado, confiável e independente do seu sistema operacional (Linux, macOS, Windows). O pipeline automatiza as etapas de preparação do ambiente, verificação de configuração, implantação e exclusão da infraestrutura.

**Execução do pipeline:**

1. **Fork do projeto:** Crie uma cópia do projeto no seu próprio espaço GitLab acessando a seguinte URL: [https://gitlab.com/jls42/storypixai](https://gitlab.com/jls42/storypixai)
2. **Clonar o repositório:**
   ```bash
   git clone https://gitlab.com/seu_usuario/storypixai.git # Substitua "seu_usuario" pelo seu nome de usuário do GitLab
   cd storypixai
   ```
3. **Configurar as variáveis de ambiente:**
   - Vá para as configurações de CI/CD do seu projeto.
   - Defina as seguintes variáveis:
     * `AWS_ACCESS_KEY_ID`: Sua chave de acesso AWS.
     * `AWS_SECRET_ACCESS_KEY`: Sua chave secreta AWS.
     * `OPENAI_KEY` (recomendado): Sua chave API OpenAI (se você estiver usando DALL-E 3 ou GPT4-o).
     * `TF_VAR_cognito_auth`: Uma variável contendo a configuração do Cognito no formato JSON, por exemplo:
       ```json
       {"user": "seu_nome_de_usuario", "password": "sua_senha_segura"}
       ```

4. **Personalizar o script `export.sh`:**
   * Abra o arquivo `export.sh` e modifique as seguintes variáveis para que correspondam à sua configuração:
      ```bash
      # o bucket será criado automaticamente se estiver disponível, escolha um nome único
      BUCKET_STATE="seu_nome_de_bucket_para_o_estado_terraform"
      AWS_DEFAULT_REGION="sua_região_aws"
      ```

5. **Configurar o Terraform:**
   * Crie um arquivo `terraform.tfvars` na raiz do diretório `terraform` e defina as variáveis específicas para o seu ambiente, incluindo (veja a seção Documentação Terraform-docs abaixo para mais detalhes):
      ```
      bucket_name = "seu_nome_de_bucket_s3"

6. **Acionar o pipeline:**
   - Vá para a seção CI/CD do seu projeto GitLab.
   - Escolha a etapa que você deseja executar (por exemplo, `Verificação Terraform` ou `Implantação Terraform`).
   - Clique no botão "Play" para iniciar a etapa selecionada.
    ![alt text](/img/cicd.png)

**Nota:** Se você usar o CI/CD fornecido com o projeto, não será necessário instalar o Terraform. A implantação será feita automaticamente via GitLab CI/CD após configurar as variáveis de ambiente necessárias.

## Instalação em uma máquina Linux (Ubuntu 22.04) (Opcional)

Se você preferir implantar o StoryPixAI manualmente, poderá fazê-lo em uma máquina Linux. **Observe que este método foi testado apenas no Ubuntu 22.04.** No entanto, é recomendado usar o método de implantação via GitLab CI/CD, pois ele é mais simples e não requer a instalação local do Terraform.

1. **Clonar o repositório:**

   ```bash
   git clone https://gitlab.com/jls42/storypixai.git
   cd storypixai
   ```

2. **Configurar as variáveis de ambiente:**

   * **AWS:**
        * Execute `aws configure` e siga as instruções para configurar suas credenciais AWS (chaves de acesso e chave secreta) e a região padrão.
   * **OpenAI (Opcional):**
        * Execute `source export.sh` para carregar as funções do script.
        * Execute `manage_openai_key put <sua_chave_openai>` para armazenar sua chave OpenAI no SSM Parameter Store.

## Implantação via Terraform

1. **Preparar o ambiente AWS:**
    * Siga as mesmas etapas para configurar as variáveis de ambiente AWS mencionadas acima.

2. **Personalizar o script `export.sh`:**
   * Abra o arquivo `export.sh` e modifique as seguintes variáveis para que correspondam à sua configuração:
      ```bash
      # o bucket será criado automaticamente se estiver disponível, escolha um nome único
      BUCKET_STATE="seu_nome_de_bucket_para_o_estado_terraform"
      AWS_DEFAULT_REGION="sua_região_aws"
      ``` **Configurar Terraform:**
   * Crie um arquivo `terraform.tfvars` na raiz do diretório `terraform` e defina as variáveis específicas ao seu ambiente, notadamente (veja a seção Documentação Terraform-docs mais abaixo para mais detalhes):
      ```
      bucket_name = "seu_nome_do_bucket_s3"
      cognito_auth = {
        user     = "seu_nome_de_usuario" 
        password = "sua_senha_segura"
      }
      ```

4. **Implantar a infraestrutura:**
   ```bash
   source export.sh
   terraform_plan
   terraform_apply
   ```
# Utilização

1. **Autenticação:**
   - Abra seu navegador web e acesse a URL do seu aplicativo StoryPixAI.
   - Faça login usando o nome de usuário e a senha que você definiu na variável `cognito_auth` durante a implantação.

2. **Criação de uma história:**
   - Uma vez conectado, você acessará a interface de geração de histórias.

   - **Escolha o idioma:** Selecione o idioma desejado no menu suspenso no canto superior direito da tela. As opções disponíveis são: francês, inglês, espanhol, alemão, italiano e português.  

    
   - **Digite sua ideia de história:** No campo de texto fornecido, insira algumas palavras ou uma frase que irão inspirar a história. Você pode incluir nomes, lugares, personagens ou qualquer outro elemento que achar relevante.
   - **Personalize a geração (opcional):**
      - **Modelo de história:** Escolha entre os modelos disponíveis (OpenAI GPT-4-o, Anthropic Claude 3, Mistral AI).  
      - **Modelo de imagem:** Escolha entre OpenAI DALL-E 3 ou Stable Diffusion XL.  
      - **Tamanho da imagem:** Selecione o tamanho desejado para a ilustração (as opções disponíveis dependem do modelo de imagem escolhido).
      - **Estilo da imagem:** Escolha um estilo entre a lista proposta para personalizar a estética da ilustração (apenas para Stable Diffusion XL).  
      - **Qualidade da imagem:** Selecione a qualidade da imagem gerada por DALL-E 3 (padrão ou HD).
      - **Seed:** Insira um número inteiro para inicializar o gerador aleatório e obter resultados diferentes a cada geração.
      - **Ativar/Desativar a geração de imagem:** Marque ou desmarque a caixa para ativar ou desativar a geração de uma ilustração para a história.
   - **Inicie a geração:** Clique no botão "Gerar".

3. **Acompanhamento do progresso:**
   - A interface exibirá a mensagem "Processando...".

4. **Acesso à história gerada:**
   - Uma vez que a geração estiver concluída, a mensagem "Pronto!" será exibida, seguida por um link único para a história e a ilustração geradas.
   - Clique no link para acessar sua criação.

5. **Consulta e compartilhamento:**
   - Você pode consultar a história e a ilustração diretamente no navegador.
   - Você pode copiar o link único e compartilhá-lo com outras pessoas para que elas também possam ler sua criação.

**Desconectar:**

* Clique no botão "Desconectar" para sair do aplicativo.

# Documentação Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requisitos

| Nome | Versão |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Provedores

| Nome | Versão |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.53.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.2 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## Módulos

Nenhum módulo.

## Recursos | Nome | Tipo |
|------|------|
| [aws_api_gateway_account.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account) | recurso |
| [aws_api_gateway_authorizer.cognito_authorizer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | recurso |
| [aws_api_gateway_deployment.api_cors_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | recurso |
| [aws_api_gateway_deployment.api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | recurso |
| [aws_api_gateway_deployment.status_api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | recurso |
| [aws_api_gateway_integration.api_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration.lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration.status_lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration.status_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | recurso |
| [aws_api_gateway_integration_response.get_status_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_integration_response.integration_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_integration_response.integration_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_integration_response.status_options_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | recurso |
| [aws_api_gateway_method.api_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method.api_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method.status_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method.status_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | recurso |
| [aws_api_gateway_method_response.get_status_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_method_response.method_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_method_response.method_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_method_response.status_options_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | recurso |
| [aws_api_gateway_resource.api_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | recurso |
| [aws_api_gateway_resource.status_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | recurso |
| [aws_api_gateway_rest_api.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | recurso |
| [aws_cloudfront_distributio n.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | recurso |
| [aws_cloudfront_response_headers_policy.cors_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_response_headers_policy) | recurso |
| [aws_cloudwatch_log_group.api_gw_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | recurso |
| [aws_cloudwatch_log_group.lambda_ia](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | recurso |
| [aws_cloudwatch_log_group.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | recurso |
| [aws_cognito_identity_pool.identity_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool) | recurso |
| [aws_cognito_identity_pool_roles_attachment.identity_pool_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool_roles_attachment) | recurso |
| [aws_cognito_user.example_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user) | recurso |
| [aws_cognito_user_pool.user_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool) | recurso |
| [aws_cognito_user_pool_client.user_pool_client](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool_client) | recurso |
| [aws_dynamodb_table.task_status](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | recurso |
| [aws_iam_policy.lambda_dynamodb_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | recurso |
| [aws_iam_policy.lambda_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | recurso |
| [aws_iam_role.api_cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.auth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.lambda_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.status_checker_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role.unauth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role_policy.auth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | recurso |
| [aws_iam_role_policy.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | recurso |
| [aws_iam_role_policy.unauth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | recurso |
| [aws_iam_role_policy_attachment.lambda_dynamodb_access_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | recurso |
| [aws_iam_role_policy_attachment.lambda_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | recurso |
| [aws_lambda_function.StoryPixAI](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | recurso |
| [aws_lambda_function.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | recurso |
| [aws_lambda_layer_version.openai_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version) | recurso |
| [aws_lambda_permission.api_gateway_permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | recurso |
| [aws_lambda_permission.status_api_gateway_permission]( https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | recurso |
| [aws_s3_bucket.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | recurso |
| [aws_s3_bucket_acl.storypixai_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | recurso |
| [aws_s3_bucket_cors_configuration.storypixai_cors](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | recurso |
| [aws_s3_bucket_ownership_controls.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_ownership_controls) | recurso |
| [aws_s3_bucket_policy.storypixai_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | recurso |
| [aws_s3_bucket_public_access_block.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | recurso |
| [aws_s3_bucket_website_configuration.storypixai_website_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | recurso |
| [aws_s3_object.index_html](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | recurso |
| [aws_s3_object.script_js](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | recurso |
| [null_resource.prepare_lambda_deployment](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | recurso |
| [archive_file.StoryPixAI_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | fonte de dados |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | fonte de dados |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | fonte de dados |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | fonte de dados |
| [aws_iam_policy_document.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | fonte de dados |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | fonte de dados |
| [template_file.script_js](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | fonte de dados |

## Inputs

| Nome | Descrição | Tipo | Padrão | Requerido |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Nome do bucket S3 para armazenar as imagens | `string` | n/a | yes |
| <a name="input_cognito_auth"></a> [cognito\_auth](#input\_cognito\_auth) | Login e senha para autenticar no gerador | `map(string)` | <pre>{<br>  "password": "_Ch4ng3M3_42",<br>  "user": "storypixia"<br>}</pre> | no |
| <a name="input_ia_environment_variables"></a> [ia\_environment\_variables](#input\_ia_environment_variables) | Variáveis de ambiente para a função Lambda | `map(string)` | <pre>{<br>  "ANTHROPIC_STORY_MODEL": "anthropic.claude-3-sonnet-20240229-v1:0",<br>  "META_STORY_MODEL": "meta.llama3-70b-instruct-v1:0",<br>  "MISTRAL_STORY_MODEL": "mistral.mistral-large-2402-v1:0",<br>  "OPENAI_IMAGE_MODEL": "dall-e-3",<br>  "OPENAI_STORY_MODEL": "gpt-4o",<br>  "STABLE_DIFFUSION_IMAGE_MODEL": "stability.stable-diffusion-xl-v1",<br>  "TITAN_IMAGE_MODEL": "amazon.titan-image-generator-v1"<br>}</pre> | no |
| <a name="input_projet"></a> [projet](#input\_projet) | Nome do projeto | `string` | `"StoryPixAI"` | no |
| <a name="input_python_runtime"></a> [python\_runtime](#input\_python_runtime) | Runtime para a função Lambda | `string` | `"python3.10"` | no |
| <a name="input_region"></a> [region](#input\_region) | Região AWS | `string` | `"us-east-1"` | no |

## Outputs | Nome | Descrição |
|------|-------------|
| <a name="output_cloudfront"></a> [cloudfront](#output\_cloudfront) | n/a |
| <a name="output_s3_bucket_website_url"></a> [s3\_bucket\_website\_url](#output\_s3\_bucket\_website\_url) | Definição da saída para o URL do site web hospedado no S3 |
<!-- END_TF_DOCS -->

# Limitações

StoryPixAI é um projeto que pode apresentar algumas limitações:

* **Qualidade variável:** A qualidade das histórias e das ilustrações geradas pode variar em função dos modelos de IA utilizados, dos prompts fornecidos e de outros fatores.
* **Custo:** O uso das APIs da OpenAI e Bedrock pode acarretar custos, dependendo do seu uso.

# Contribuir

StoryPixAI é um projeto de código aberto. Se você deseja contribuir, aqui estão algumas sugestões:

* **Relatar um bug:** Se você encontrar um problema ou um bug, por favor, registre-o na seção "Issues" do repositório GitLab.
* **Propor uma melhoria:** Se você tiver uma ideia de melhoria ou de nova funcionalidade, sinta-se à vontade para enviar uma "Merge Request" com seu código.
* **Melhorar a documentação:** Se você encontrar erros ou imprecisões no README, não hesite em corrigi-los.

Obrigado pelo seu interesse e apoio ao StoryPixAI!

# Autor

Julien LE SAUX  
Site web: https://jls42.org  
Email: contact@jls42.org  

# Termo de isenção de responsabilidade

O uso deste software é por sua conta e risco. O autor não pode ser responsabilizado por custos, danos ou perdas decorrentes de seu uso. Certifique-se de compreender as taxas associadas ao uso dos serviços AWS antes de implantar este projeto.

# Licença

Este projeto está licenciado sob a MIT com Common Clause. Isso significa que você é livre para usar, modificar e distribuir o software, mas não pode vendê-lo ou usá-lo para oferecer serviços comerciais. Para mais detalhes, consulte o arquivo [LICENSE](LICENSE).

# Exemplos de histórias geradas

## Francês
### Zoé e Tom
![alt text](/img/zoe_tom_fr.png)

## Inglês
### Léa
![alt text](/img/lea_en.png)

## Espanhol
### Roger
![alt text](/img/roger_es.png)

**Este documento foi traduzido da versão fr para a língua pt utilizando o modelo gpt-4o. Para mais informações sobre o processo de tradução, consulte https://gitlab.com/jls42/ai-powered-markdown-translator**

