[[_TOC_]]

# StoryPixAI : 由人工智能生成和插图的儿童睡前故事

StoryPixAI 是一个创新的无服务器项目，用于生成神奇的睡前故事，这些故事由人工智能创建。只需写下他们选择主题的几个词语，让人工智能编织一个独特而引人入胜的睡前故事，并附上一幅可爱的插图。所有内容都可以通过 OpenAI 和 Amazon Bedrock 的尖端 AI 模型自动生成，并以您选择的语言呈现。为您的孩子在入睡前提供一个神奇的时刻，同时通过 AWS 无服务器基础设施体验流畅的用户体验。

## 功能

* **由 AI 创建的个性化睡前故事：** 通过 OpenAI 和 AWS Bedrock 的强大 AI 模型，将您孩子的想法转化为原创睡前故事。
* **由 AI 生成的可爱插图：** 每个故事都通过可爱多彩的图片栩栩如生，非常适合激发孩子的想象力。
* **AWS 云端自动保存：** StoryPixAI 自动将您的故事和插图保存到 S3 存储桶，可以通过唯一链接随时重新阅读。
* **安全的生成器访问：** 故事创建界面通过身份验证保护，确保一个私密且安全的创作空间。
* **多语言支持：** StoryPixAI 支持以下语言：
    * 法语（主要测试）
    * 英语
    * 西班牙语
    * 德语
    * 意大利语
    * 葡萄牙语

# 架构

StoryPixAI 基于无服务器架构：

## 无服务器架构示意图
![alt text](/img/storypixai-architecture.png)

## 前端

* **生成界面 (`index.html`)：** StoryPixAI 的用户界面是一个使用纯 HTML、CSS 和 JavaScript（Vanilla JavaScript）开发的单页应用程序（SPA）。它提供了直观且互动的用户体验：  
    ![alt text](/img/generateur.png)

    * **身份验证：** 用户必须使用他们的 Cognito 凭据登录，才能访问生成功能。提供了一个模态登录窗口以简化此过程。

    * **语言选择：** 一个下拉菜单允许选择生成器和故事的语言，包括法语、英语、西班牙语、德语、意大利语和葡萄牙语。所选语言用于用户界面，并指导故事的生成。  
        ![alt text](/img/langues.png)
    * **提示输入：** 一个文本框允许用户输入所需故事的想法或主题。

    * **生成个性化（可选）：** 界面提供了一些选项以个性化生成：
        * **文本生成模型选择：** OpenAI GPT-4-o, Anthropic Claude 3, 或 Mistral AI。  
            ![alt text](/img/texte_modele.png)
        * **图像生成模型选择：** OpenAI DALL-E 3 或 Stable Diffusion XL。  
            ![alt text](/img/image_modele.png)
        * **图像模型特定参数：** 尺寸、风格（针对 Stable Diffusion XL）和质量（针对 DALL-E 3）。  
            ![alt text](/img/styles.png)  
        * **Seed：** 一个数字字段允许指定一个值来初始化随机生成器。
        * **图像生成的启用/禁用。**

    * **生成启动：** 一个“生成”按钮触发故事和图像（如果启用）的生成过程。

    * **进度跟踪：** 状态消息告知用户生成进度（“处理中...”，“准备好了！”等）以及可能的错误。

    * **结果显示：** 生成完成后，会显示一个指向生成的故事和图像的唯一链接。该链接可以复制和分享。 * **身份验证和请求管理：**
    * 脚本 `storypixai.js` 处理用户与 Amazon Cognito 的身份验证，并使用获得的访问令牌授权对 API Gateway 的请求。
    * 它还管理与 API Gateway 端点 (`/generate` 和 `/check_status/{task_id}`) 的通信，以触发生成并跟踪进度。
    * 还实现了错误管理和重试机制，以确保应用程序的可靠性。

* **托管和分发：**
    * 生成界面 (`index.html`) 托管在 Amazon S3 上，并通过 Amazon CloudFront（内容分发网络 CDN）分发，提升了全球用户的应用性能和可用性。



## 后端

* **AWS Lambda 函数（Python）：**
    * **`StoryPixAI`：** 该函数是应用程序的核心。它接受用户通过生成界面提供的文本提示，并协调故事和插图的创建过程。它依次调用 OpenAI 和/或 Bedrock 的 API 来生成故事文本和相关图片。然后，它将图片存储在 S3 存储桶中，并返回给用户指向生成故事和图片的唯一链接。生成的进度状态存储在 DynamoDB 中。

    * **`status_checker`：** 这个函数定期被生成界面调用，以检查故事和图片创建的进度。它查询 DynamoDB 以获取当前任务的状态，并将状态返回给前端。这使生成界面能够让用户了解进度，并告诉用户故事和图片何时准备好。

* **Amazon DynamoDB：** 这种键值 NoSQL 数据库用于存储与生成故事和图片任务相关的信息。每个任务都由一个唯一的 `requestId` 标识。DynamoDB 表包含以下属性：
    * **`requestId`（哈希键）：** 任务的唯一标识符。
    * **`status`：** 任务状态（进行中，完成，错误）。
    * **`resultUrl`：** 生成故事的 URL（当任务完成时）。

    DynamoDB 表还具有两个全局二级索引：

    * **`StatusIndex`：** 用于根据任务状态搜索任务。
    * **`ResultUrlIndex`：** 用于根据生成故事的 URL 搜索任务。

    Lambda 函数 `status_checker` 使用这些索引来有效地检索给定任务的状态和故事 URL。前端的 JavaScript 脚本随后查询 API Gateway 的端点 `/check_status/{task_id}` 以获取这些信息，并相应地更新用户界面。

* **Amazon S3：** 该对象存储服务托管静态网页界面（`index.html`）、生成的图片和生成的故事。故事和图片使用由函数 `StoryPixAI` 生成的唯一键存储，并将这些键返回给用户以允许其访问其创建内容。

* **Amazon CloudWatch：** 该监控服务收集 Lambda 函数的日志和指标，允许跟踪应用程序性能、检测错误和解决问题。

# 基础设施即代码 (IaC)

* **Terraform：** 整个 AWS 基础设施（Lambda、API Gateway、S3、CloudFront、Cognito、DynamoDB 等）通过 Terraform 定义和部署。这实现了自动化、可重复和版本化的基础设施管理，便于更新和修改。

# 安全和身份验证

* **Amazon Cognito：** 此服务管理用户身份验证。生成界面 (`index.html`) 使用 Cognito API 验证用户并获取访问令牌。 然后将此令牌包含在`/generate`和`/check_status/{task_id}`端点的请求中，分别授权访问`StoryPixAI`和`status_checker`功能。

# 内容分发

* **Amazon CloudFront:** 此内容分发网络（CDN）服务用于快速可靠地向全球用户分发静态网页接口（`index.html`）。它将内容缓存到地理分布的服务器上，从而减少延迟并改善用户体验。

# AI模型

StoryPixAI兼容多种先进的AI模型，用于生成高质量的故事和插图：

* **OpenAI模型：**
    * **故事生成：**
        * OpenAI GPT-4-o
    * **图像生成：**
        * OpenAI DALL-E 3

* **通过AWS Bedrock访问的模型：**
    * **故事生成：**
        * Anthropic Claude 3 Sonnet (20240229-v1)
        * Mistral AI Mistral Large (2402-v1)
    * **图像生成：**
        * StabilityAI Stable Diffusion XL v1

# 前提条件

在开始使用StoryPixAI之前，请确保您具备以下条件：

* **AWS账户：** 您需要一个活跃的AWS账户来部署和管理无服务器基础设施。确保您的IAM用户拥有以下权限（或等效权限）。**请注意，此策略仅为示例，可以根据您的具体安全需求进行调整：**

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "cloudwatch:*",
            "ec2:*",
            "lambda:*",
            "s3:*",
            "ssm:*",
            "logs:*",
            "dynamodb:*",
            "cognito-idp:*",
            "cognito-identity:*",
            "apigateway:*",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:AttachRolePolicy",
            "iam:DetachRolePolicy",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:CreatePolicy",
            "iam:DeletePolicy",
            "iam:PassRole",
            "iam:Tag*",
            "iam:Get*",
            "iam:List*",
            "iam:TagRole",
            "kms:TagResource",
            "iam:CreateServiceLinkedRole",
            "budgets:*",
            "events:*"
          ],
          "Resource": "*"
        }
      ]
    }
    ```

* **激活Bedrock模型：** 确保以下AI模型已在您的AWS Bedrock账户中激活：
    * Anthropic Claude 3 Sonnet (20240229-v1)
    * Mistral AI Mistral Large (2402-v1)
    * StabilityAI Stable Diffusion XL v1

[访问Bedrock模型 - us-east-1 ](https://us-east-1.console.aws.amazon.com/bedrock/home?region=us-east-1#/modelaccess)
![alt text](/img/modele_1.png)
![alt text](/img/modele_2.png)

* **OpenAI API密钥（如果您希望使用OpenAI模型）** 要使用DALL-E 3生成高质量插图，强烈建议您拥有有效的OpenAI API密钥。您可以在OpenAI网站上获取您的密钥。请注意，StoryPixAI也可以使用Stable Diffusion XL v1（通过AWS Bedrock）生成图像，但DALL-E 3通常提供更好的结果。

**通过GitLab CI/CD进行部署：**

* **GitLab账户：** 您需要一个有效的GitLab账户并能访问GitLab上的StoryPixAI项目。
* **GitLab环境变量：** 您需要在您的GitLab项目中配置必要的环境变量，包括AWS和OpenAI API密钥（如果您使用DALL-E 3）。

# 安装和部署

## 通过GitLab CI/CD部署（推荐）

StoryPixAI设计为可以通过集成的GitLab CI/CD流水线轻松部署和更新。 此部署方法**强烈推荐**，因为它是自动化的、可靠的，并且与您的操作系统（Linux、macOS、Windows）无关。该流水线自动执行环境准备、配置检查、部署和基础设施删除的步骤。

**执行流水线：**

1. **分叉项目：** 在您的 GitLab 空间中创建项目的副本，访问以下 URL：[https://gitlab.com/jls42/storypixai](https://gitlab.com/jls42/storypixai)
2. **克隆存储库：**
   ```bash
   git clone https://gitlab.com/votre_utilisateur/storypixai.git # 将 "votre_utilisateur" 替换为您的 GitLab 用户名
   cd storypixai
   ```
3. **配置环境变量：**
   - 转到您的项目的 CI/CD 设置。
   - 定义以下变量：
     * `AWS_ACCESS_KEY_ID`：您的 AWS 访问密钥。
     * `AWS_SECRET_ACCESS_KEY`：您的 AWS 密钥。
     * `OPENAI_KEY`（推荐）：您的 OpenAI API 密钥（如果您使用 DALL-E 3 或 GPT4-o）。
     * `TF_VAR_cognito_auth`：包含 Cognito 配置的 JSON 变量，例如：
       ```json
       {"user": "votre_nom_d_utilisateur", "password": "votre_mot_de_passe_securise"}
       ```

4. **自定义 `export.sh` 脚本：**
   * 打开 `export.sh` 文件并修改以下变量以匹配您的配置：
      ```bash
      # 如果可用，将自动创建存储桶，请选择一个唯一的名称
      BUCKET_STATE="votre_nom_de_bucket_pour_l_état_terraform"
      AWS_DEFAULT_REGION="votre_région_aws"
      ```

5. **配置 Terraform：**
   * 在 `terraform` 目录的根目录中创建一个 `terraform.tfvars` 文件，并定义特定于您的环境的变量，特别是（参见下文的 Terraform-docs 文档部分以了解更多细节）：
      ```
      bucket_name = "votre_nom_de_bucket_s3"

6. **触发流水线：**
   - 转到您的 GitLab 项目的 CI/CD 部分。
   - 选择您希望执行的步骤（例如，`Vérification Terraform` 或 `Déploiement Terraform`）。
   - 点击 "Play" 按钮启动选择的步骤。   
    ![alt text](/img/cicd.png)

**注意：** 如果您使用项目随附的 CI/CD，则无需安装 Terraform。完成所需的环境变量配置后，部署将通过 GitLab CI/CD 自动进行。

## 在 Linux 主机上安装（Ubuntu 22.04）（可选）

如果您更喜欢手动部署 StoryPixAI，您可以在 Linux 主机上进行。**请注意，仅在 Ubuntu 22.04 上测试了此方法。** 但是，建议使用 GitLab CI/CD 部署方法，因为它更简单且不需要本地安装 Terraform。

1. **克隆存储库：**

   ```bash
   git clone https://gitlab.com/jls42/storypixai.git
   cd storypixai
   ```

2. **配置环境变量：**

   * **AWS：**
        * 运行 `aws configure` 并按照说明配置您的 AWS 证书（访问密钥和密钥）和默认区域。
   * **OpenAI（可选）：**
        * 运行 `source export.sh` 以加载脚本函数。
        * 运行 `manage_openai_key put <votre_clé_openai>` 将您的 OpenAI 密钥存储在 SSM Parameter Store 中。

## 通过 Terraform 部署

1. **准备 AWS 环境：**
    * 按上述相同步骤配置 AWS 环境变量。

2. **自定义 `export.sh` 脚本：**
   * 打开 `export.sh` 文件并修改以下变量以匹配您的配置：
      ```bash
      # 如果可用，将自动创建存储桶，请选择一个唯一的名称
      BUCKET_STATE="votre_nom_de_bucket_pour_l_état_terraform"
      AWS_DEFAULT_REGION="votre_région_aws"
      ```

3. **配置 Terraform：**
   * 在 `terraform` 目录的根目录下创建一个 `terraform.tfvars` 文件，并定义特定于您的环境的变量，特别是（有关详细信息，请参阅下面的 Terraform-docs 文档部分）：
      ```
      bucket_name = "votre_nom_de_bucket_s3"
      cognito_auth = {
        user     = "votre_nom_d_utilisateur" 
        password = "votre_mot_de_passe_securise"
      }
      ```

4. **部署基础设施：**
   ```bash
   source export.sh
   terraform_plan
   terraform_apply
   ```
# 使用说明

1. **认证：**
   - 打开您的网页浏览器，访问 StoryPixAI 应用程序的 URL。
   - 使用部署过程中在 `cognito_auth` 变量中定义的用户名和密码登录。

2. **创建故事：**
   - 登录后，您将进入故事生成界面。

   - **选择语言：** 从屏幕右上角的下拉菜单中选择所需的语言。可用选项有：法语、英语、西班牙语、德语、意大利语和葡萄牙语。  

    
   - **输入您的故事点子：** 在指定的文本框中输入一些单词或一句话来激发故事。您可以包括名称、地点、角色或任何其他您认为相关的元素。
   - **自定义生成（可选）：**
      - **故事模型：** 从可用模型中选择（OpenAI GPT-4-o、Anthropic Claude 3、Mistral AI）。  
      - **图像模型：** 在 OpenAI DALL-E 3 和 Stable Diffusion XL 之间进行选择。  
      - **图像大小：** 选择所需的图像大小（可用选项取决于所选择的图像模型）。
      - **图像风格：** 从提供的列表中选择一种风格来定制图像的美学（仅适用于 Stable Diffusion XL）。  
      - **图像质量：** 选择 DALL-E 3 生成的图像质量（标准或高清）。
      - **种子：** 输入一个整数以初始化随机生成器，并在每次生成时获得不同的结果。
      - **启用/禁用图像生成：** 选中或取消选中复选框以启用或禁用故事的图像生成。
   - **开始生成：** 点击“生成”按钮。

3. **进度跟踪：**
   - 界面将显示“处理进行中...”的消息。

4. **访问生成的故事：**
   - 一旦生成完成，“准备好了！”消息将显示，随后是指向生成故事和插图的唯一链接。
   - 点击链接即可访问您的创作。

5. **查看和分享：**
   - 您可以在浏览器中直接查看故事和插图。
   - 您可以复制唯一链接并与其他人分享，让他们也可以阅读您的创作。

**注销：**

* 点击“注销”按钮以退出应用程序。

# Terraform-docs 文档  
<!-- BEGIN_TF_DOCS -->
## 要求

| 名称 | 版本 |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## 提供者

| 名称 | 版本 |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.53.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.2 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## 模块

无模块。

## 资源 | 名称 | 类型 |
|------|------|
| [aws_api_gateway_account.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account) | 资源 |
| [aws_api_gateway_authorizer.cognito_authorizer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | 资源 |
| [aws_api_gateway_deployment.api_cors_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | 资源 |
| [aws_api_gateway_deployment.api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | 资源 |
| [aws_api_gateway_deployment.status_api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | 资源 |
| [aws_api_gateway_integration.api_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | 资源 |
| [aws_api_gateway_integration.lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | 资源 |
| [aws_api_gateway_integration.status_lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | 资源 |
| [aws_api_gateway_integration.status_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | 资源 |
| [aws_api_gateway_integration_response.get_status_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | 资源 |
| [aws_api_gateway_integration_response.integration_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | 资源 |
| [aws_api_gateway_integration_response.integration_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | 资源 |
| [aws_api_gateway_integration_response.status_options_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | 资源 |
| [aws_api_gateway_method.api_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | 资源 |
| [aws_api_gateway_method.api_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | 资源 |
| [aws_api_gateway_method.status_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | 资源 |
| [aws_api_gateway_method.status_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | 资源 |
| [aws_api_gateway_method_response.get_status_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | 资源 |
| [aws_api_gateway_method_response.method_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | 资源 |
| [aws_api_gateway_method_response.method_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | 资源 |
| [aws_api_gateway_method_response.status_options_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | 资源 |
| [aws_api_gateway_resource.api_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | 资源 |
| [aws_api_gateway_resource.status_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | 资源 |
| [aws_api_gateway_rest_api.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | 资源 |
| [aws_cloudfront_distributio n.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | 资源 |
| [aws_cloudfront_response_headers_policy.cors_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_response_headers_policy) | 资源 |
| [aws_cloudwatch_log_group.api_gw_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | 资源 |
| [aws_cloudwatch_log_group.lambda_ia](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | 资源 |
| [aws_cloudwatch_log_group.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | 资源 |
| [aws_cognito_identity_pool.identity_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool) | 资源 |
| [aws_cognito_identity_pool_roles_attachment.identity_pool_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool_roles_attachment) | 资源 |
| [aws_cognito_user.example_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user) | 资源 |
| [aws_cognito_user_pool.user_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool) | 资源 |
| [aws_cognito_user_pool_client.user_pool_client](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool_client) | 资源 |
| [aws_dynamodb_table.task_status](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | 资源 |
| [aws_iam_policy.lambda_dynamodb_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | 资源 |
| [aws_iam_policy.lambda_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | 资源 |
| [aws_iam_role.api_cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | 资源 |
| [aws_iam_role.auth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | 资源 |
| [aws_iam_role.lambda_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | 资源 |
| [aws_iam_role.status_checker_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | 资源 |
| [aws_iam_role.unauth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | 资源 |
| [aws_iam_role_policy.auth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | 资源 |
| [aws_iam_role_policy.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | 资源 |
| [aws_iam_role_policy.unauth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | 资源 |
| [aws_iam_role_policy_attachment.lambda_dynamodb_access_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | 资源 |
| [aws_iam_role_policy_attachment.lambda_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | 资源 |
| [aws_lambda_function.StoryPixAI](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | 资源 |
| [aws_lambda_function.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | 资源 |
| [aws_lambda_layer_version.openai_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version) | 资源 |
| [aws_lambda_permission.api_gateway_permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | 资源 |
| [aws_lambda_permission.status_api_gateway_permission]( https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_acl.storypixai_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_cors_configuration.storypixai_cors](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | resource |
| [aws_s3_bucket_ownership_controls.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_ownership_controls) | resource |
| [aws_s3_bucket_policy.storypixai_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_website_configuration.storypixai_website_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | resource |
| [aws_s3_object.index_html](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | resource |
| [aws_s3_object.script_js](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | resource |
| [null_resource.prepare_lambda_deployment](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [archive_file.StoryPixAI_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [template_file.script_js](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## 输入

| 名称 | 描述 | 类型 | 默认值 | 必填 |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | 存储图像的 S3 bucket 名称 | `string` | 不适用 | 是 |
| <a name="input_cognito_auth"></a> [cognito\_auth](#input\_cognito\_auth) | 登录和密码，用于在生成器上进行身份验证 | `map(string)` | <pre>{<br>  "password": "_Ch4ng3M3_42",<br>  "user": "storypixia"<br>}</pre> | 否 |
| <a name="input_ia_environment_variables"></a> [ia\_environment\_variables](#input\_ia\_environment\_variables) | Lambda 函数的环境变量 | `map(string)` | <pre>{<br>  "ANTHROPIC_STORY_MODEL": "anthropic.claude-3-sonnet-20240229-v1:0",<br>  "META_STORY_MODEL": "meta.llama3-70b-instruct-v1:0",<br>  "MISTRAL_STORY_MODEL": "mistral.mistral-large-2402-v1:0",<br>  "OPENAI_IMAGE_MODEL": "dall-e-3",<br>  "OPENAI_STORY_MODEL": "gpt-4o",<br>  "STABLE_DIFFUSION_IMAGE_MODEL": "stability.stable-diffusion-xl-v1",<br>  "TITAN_IMAGE_MODEL": "amazon.titan-image-generator-v1"<br>}</pre> | 否 |
| <a name="input_projet"></a> [projet](#input\_projet) | 项目名称 | `string` | `"StoryPixAI"` | 否 |
| <a name="input_python_runtime"></a> [python\_runtime](#input\_python\_runtime) | Lambda 函数的运行时 | `string` | `"python3.10"` | 否 |
| <a name="input_region"></a> [region](#input\_region) | AWS 区域 | `string` | `"us-east-1"` | 否 |

## 输出 | 名称 | 描述 |
|------|-------------|
| <a name="output_cloudfront"></a> [cloudfront](#output\_cloudfront) | n/a |
| <a name="output_s3_bucket_website_url"></a> [s3\_bucket\_website\_url](#output\_s3\_bucket\_website\_url) | 定义托管在 S3 上的网站 URL 的输出 |

# 限制

StoryPixAI 是一个可能存在某些限制的项目：

* **质量可变：** 故事和插图的生成质量可能会因所使用的 AI 模型、提供的提示和其他因素而异。
* **成本：** 使用 OpenAI 和 Bedrock 的 API 可能会产生费用，具体取决于您的使用情况。

# 贡献

StoryPixAI 是一个开源项目。如果您希望贡献，请参阅以下几条建议：

* **报告错误：** 如果您遇到问题或错误，请在 GitLab 仓库的 "Issues" 部分提出。
* **提出改进：** 如果您有改进建议或新功能创意，请提交您的代码并创建 “Merge Request”。
* **改进文档：** 如果您发现 README 中有错误或不准确之处，请随时进行更正。

感谢您对 StoryPixAI 的兴趣和支持！

# 作者

Julien LE SAUX  
网站：https://jls42.org  
电子邮件：contact@jls42.org  

# 免责声明

使用此软件的风险由您自己承担。作者不对因使用此软件所导致的费用、损害或损失负责。请确保在部署此项目之前明白使用 AWS 服务的相关费用。

# 许可

此项目采用 MIT 许可和 Common Clause 许可。这意味着您可以自由使用、修改和分发此软件，但不得出售或用于提供商业服务。具体详情请参阅 [LICENSE](LICENSE) 文件。

# 生成的故事示例

## 法语
### Zoé et Tom
![alt text](/img/zoe_tom_fr.png)

## 英语
### Léa
![alt text](/img/lea_en.png)

## 西班牙语
### Roger
![alt text](/img/roger_es.png)

**此文档由 fr 版翻译为 zh 语言，使用 gpt-4o 模型。有关翻译过程的更多信息，请参见 https://gitlab.com/jls42/ai-powered-markdown-translator**

