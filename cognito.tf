# Création d'un User Pool Cognito
resource "aws_cognito_user_pool" "user_pool" {
  name = "my_user_pool"

  password_policy {
    minimum_length                   = 8
    require_lowercase                = true
    require_numbers                  = true
    require_symbols                  = true
    require_uppercase                = true
    temporary_password_validity_days = 7
  }

  auto_verified_attributes = ["email"]

  schema {
    name                = "email"
    attribute_data_type = "String"
    required            = true
    mutable             = false
  }

  admin_create_user_config {
    allow_admin_create_user_only = false
  }
}

# Création d'un App Client pour le User Pool
resource "aws_cognito_user_pool_client" "user_pool_client" {
  name            = "my_user_pool_client"
  user_pool_id    = aws_cognito_user_pool.user_pool.id
  generate_secret = false
  explicit_auth_flows = [
    "ALLOW_ADMIN_USER_PASSWORD_AUTH",
    "ALLOW_CUSTOM_AUTH",
    "ALLOW_USER_PASSWORD_AUTH",
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH"
  ]
  refresh_token_validity        = 30 # Validité des refresh tokens en jours
  access_token_validity         = 1  # Validité des access tokens en heures
  id_token_validity             = 1  # Validité des ID tokens en heures
  prevent_user_existence_errors = "ENABLED"
}

# Création d'un Identity Pool
resource "aws_cognito_identity_pool" "identity_pool" {
  identity_pool_name               = "my_identity_pool"
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id     = aws_cognito_user_pool_client.user_pool_client.id
    provider_name = aws_cognito_user_pool.user_pool.endpoint
  }
}

# Création du rôle IAM pour les utilisateurs authentifiés
resource "aws_iam_role" "auth_role" {
  name = "auth_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect = "Allow",
      Principal = {
        Federated = "cognito-identity.amazonaws.com"
      },
      Action = "sts:AssumeRoleWithWebIdentity",
      Condition = {
        StringEquals = {
          "cognito-identity.amazonaws.com:aud" = aws_cognito_identity_pool.identity_pool.id
        },
        "ForAnyValue:StringLike" : {
          "cognito-identity.amazonaws.com:amr" : "authenticated"
        }
      }
    }]
  })
}

resource "aws_iam_role_policy" "auth_role_policy" {
  role = aws_iam_role.auth_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect = "Allow",
      Action = [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*"
      ],
      Resource = "*"
    }]
  })
}

# Création du rôle IAM pour les utilisateurs non authentifiés
resource "aws_iam_role" "unauth_role" {
  name = "unauth_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect = "Allow",
      Principal = {
        Federated = "cognito-identity.amazonaws.com"
      },
      Action = "sts:AssumeRoleWithWebIdentity",
      Condition = {
        StringEquals = {
          "cognito-identity.amazonaws.com:aud" = aws_cognito_identity_pool.identity_pool.id
        },
        "ForAnyValue:StringLike" : {
          "cognito-identity.amazonaws.com:amr" : "unauthenticated"
        }
      }
    }]
  })
}

resource "aws_iam_role_policy" "unauth_role_policy" {
  role = aws_iam_role.unauth_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect = "Allow",
      Action = [
        "mobileanalytics:PutEvents",
        "cognito-sync:*"
      ],
      Resource = "*"
    }]
  })
}

# Attachement des rôles IAM au Identity Pool
resource "aws_cognito_identity_pool_roles_attachment" "identity_pool_roles" {
  identity_pool_id = aws_cognito_identity_pool.identity_pool.id

  roles = {
    "authenticated"   = aws_iam_role.auth_role.arn
    "unauthenticated" = aws_iam_role.unauth_role.arn
  }
}

# Création d'un utilisateur Cognito pour le User Pool
resource "aws_cognito_user" "example_user" {
  user_pool_id = aws_cognito_user_pool.user_pool.id
  username     = var.cognito_auth["user"]
  password     = var.cognito_auth["password"]
}
