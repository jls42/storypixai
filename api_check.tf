locals {
  # Calculer le hash SHA-256 du fichier api_check.tf pour déclencher des redeploiements
  api_check_hash = filesha256("${path.module}/api_check.tf")
}

# Définition de la ressource API Gateway pour le chemin "check-status"
resource "aws_api_gateway_resource" "status_resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "check-status"
}

# Définition de la méthode GET pour la ressource "check-status"
resource "aws_api_gateway_method" "status_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.status_resource.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.cognito_authorizer.id
}

# Intégration de la méthode GET avec la fonction Lambda
resource "aws_api_gateway_integration" "status_lambda_integration" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.status_resource.id
  http_method = aws_api_gateway_method.status_method.http_method

  integration_http_method = "POST" // Les fonctions Lambda sont toujours invoquées avec POST
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.status_checker.invoke_arn
}

# Ajout des en-têtes CORS à la méthode GET existante
resource "aws_api_gateway_method_response" "get_status_method_response" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.status_resource.id
  http_method = aws_api_gateway_method.status_method.http_method
  status_code = "200" # Assurez-vous que cela correspond à vos codes de statut configurés

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
}

# Mise à jour de l'intégration de la réponse pour inclure CORS
resource "aws_api_gateway_integration_response" "get_status_integration_response" {
  depends_on  = [aws_api_gateway_integration.status_lambda_integration]
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.status_resource.id
  http_method = aws_api_gateway_method.status_method.http_method
  status_code = "200" # Assurez-vous que cela correspond à vos codes de statut configurés

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
}

# Autorisation pour l'API Gateway d'invoquer la fonction Lambda
resource "aws_lambda_permission" "status_api_gateway_permission" {
  statement_id  = "AllowExecutionFromAPIGatewayStatus"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.status_checker.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/check-status"
}

# Déploiement de l'API Gateway pour la ressource "check-status"
resource "aws_api_gateway_deployment" "status_api_deployment" {
  depends_on = [
    aws_api_gateway_integration.status_lambda_integration,
    aws_api_gateway_method_response.get_status_method_response,
    aws_api_gateway_integration_response.get_status_integration_response,
    aws_api_gateway_method.status_method,
    aws_lambda_permission.status_api_gateway_permission,
    aws_api_gateway_integration.status_options_integration,
    aws_api_gateway_method.status_options_method,
    aws_api_gateway_method_response.status_options_response,
    aws_api_gateway_integration_response.status_options_integration_response,
    aws_api_gateway_integration.lambda_integration,
    aws_lambda_permission.api_gateway_permission
  ]
  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = "prod"
  triggers = {
    redeployment = local.api_check_hash
  }
  lifecycle {
    create_before_destroy = true
  }
}

# Méthode HTTP OPTIONS pour gérer CORS pour "check-status"
resource "aws_api_gateway_method" "status_options_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.status_resource.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

# Réponse de la méthode OPTIONS pour CORS
resource "aws_api_gateway_method_response" "status_options_response" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.status_resource.id
  http_method = aws_api_gateway_method.status_options_method.http_method
  status_code = "200"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Headers" = true
  }
}

# Intégration Mock pour la méthode OPTIONS
resource "aws_api_gateway_integration" "status_options_integration" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.status_resource.id
  http_method = aws_api_gateway_method.status_options_method.http_method

  type = "MOCK"
  request_templates = {
    "application/json" = jsonencode({
      statusCode = 200
    })
  }
}

# Réponse d'intégration pour la méthode OPTIONS
resource "aws_api_gateway_integration_response" "status_options_integration_response" {
  depends_on = [
    aws_api_gateway_integration.status_options_integration,
    aws_api_gateway_method_response.status_options_response
  ]
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.status_resource.id
  http_method = aws_api_gateway_method.status_options_method.http_method
  status_code = "200"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = "'*'",
    "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS'",
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
  }
}
