locals {
  # Calculer le hash SHA-256 du fichier api_ia.tf pour déclencher des redeploiements
  api_ia_hash = filesha256("${path.module}/api_ia.tf")
}

# Documentation pour le code javascript
# https://docs.aws.amazon.com/fr_fr/cognito/latest/developerguide/authentication.html

# Définition de l'authorizer Cognito pour l'API Gateway
resource "aws_api_gateway_authorizer" "cognito_authorizer" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  name        = "CognitoAuthorizer"
  type        = "COGNITO_USER_POOLS"
  provider_arns = [
    aws_cognito_user_pool.user_pool.arn
  ]
}

# Définition du compte API Gateway pour l'intégration avec CloudWatch
resource "aws_api_gateway_account" "api" {
  cloudwatch_role_arn = aws_iam_role.api_cloudwatch.arn
}

# Création d'une API Gateway REST
resource "aws_api_gateway_rest_api" "api" {
  name        = "S3ImageGeneratorAPI"
  description = "API pour générer des images à partir de S3"
}

# Définition de la ressource sur l'API Gateway
resource "aws_api_gateway_resource" "api_resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "generate-image"
}

# Définition de la méthode HTTP POST pour l'API
resource "aws_api_gateway_method" "api_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.api_resource.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.cognito_authorizer.id
}

# Intégration de Lambda pour la méthode POST en mode asynchrone
resource "aws_api_gateway_integration" "lambda_integration" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.api_resource.id
  http_method = aws_api_gateway_method.api_method.http_method

  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.StoryPixAI.invoke_arn

  request_templates = {
    "application/json" = <<EOF
{
  "body" : $input.json('$'),
  "requestId": "$context.requestId"
}
EOF
  }

  request_parameters = {
    "integration.request.header.X-Amz-Invocation-Type" = "'Event'"
  }
}

# Méthode de réponse pour la gestion asynchrone
resource "aws_api_gateway_method_response" "method_response_202" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.api_resource.id
  http_method = aws_api_gateway_method.api_method.http_method
  status_code = "202"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = true
    "method.response.header.Access-Control-Allow-Headers" = true
    "method.response.header.Access-Control-Allow-Methods" = true
  }
}

# Intégration de la réponse pour la gestion asynchrone
resource "aws_api_gateway_integration_response" "integration_response_202" {
  depends_on = [
    aws_api_gateway_integration.lambda_integration
  ]

  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.api_resource.id
  http_method = aws_api_gateway_method.api_method.http_method
  status_code = "202"

  response_templates = {
    "application/json" = "{\"requestId\": \"$context.requestId\"}"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = "'*'",
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
    "method.response.header.Access-Control-Allow-Methods" = "'POST,OPTIONS'"
  }
}

# Permission pour API Gateway d'invoquer Lambda
resource "aws_lambda_permission" "api_gateway_permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.StoryPixAI.function_name
  principal     = "apigateway.amazonaws.com"

  # Dépend de l'API Gateway pour assurer que la permission n'est ajoutée qu'après sa création
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/generate-image"
}

# Déploiement de l'API Gateway
resource "aws_api_gateway_deployment" "api_deployment" {
  depends_on = [
    aws_api_gateway_integration.lambda_integration,
    aws_api_gateway_integration_response.integration_response_202
  ]

  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = "prod"
  triggers = {
    redeployment = local.api_ia_hash
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Méthode HTTP OPTIONS pour l'API
resource "aws_api_gateway_method" "api_options_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.api_resource.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

# Méthode de réponse pour la gestion des OPTIONS
resource "aws_api_gateway_method_response" "method_response_options_200" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.api_resource.id
  http_method = aws_api_gateway_method.api_options_method.http_method
  status_code = "200"

  response_models = {
    "application/json" = "Empty"
  }

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = false,
    "method.response.header.Access-Control-Allow-Methods" = false,
    "method.response.header.Access-Control-Allow-Origin"  = false
  }
}

# Intégration Mock pour la méthode OPTIONS
resource "aws_api_gateway_integration" "api_options_integration" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.api_resource.id
  http_method = aws_api_gateway_method.api_options_method.http_method

  type = "MOCK"
  request_templates = {
    "application/json" = jsonencode({
      statusCode = 200
    })
  }
}

# Intégration de la réponse pour la méthode OPTIONS
resource "aws_api_gateway_integration_response" "integration_response_options_200" {
  depends_on = [
    aws_api_gateway_integration.api_options_integration
  ]

  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.api_resource.id
  http_method = aws_api_gateway_method.api_options_method.http_method
  status_code = "200"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
    "method.response.header.Access-Control-Allow-Methods" = "'OPTIONS'",
    "method.response.header.Access-Control-Allow-Origin"  = "'*'"
  }
}

# Redéploiement de l'API Gateway après les modifications CORS
resource "aws_api_gateway_deployment" "api_cors_deployment" {
  depends_on = [
    aws_api_gateway_integration_response.integration_response_options_200
  ]

  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_deployment.api_deployment.stage_name
}
