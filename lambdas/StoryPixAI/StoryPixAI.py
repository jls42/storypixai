import json
import boto3
import base64
import os
import re
from datetime import datetime
from openai import OpenAI
import logging

# Configuration du logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)

generate_images = False
region_name = os.getenv("AWS_REGION", "us-east-1")

# Création d'un client SSM
ssm = boto3.client("ssm", region_name=region_name)

# Obtention de la clé API OpenAI depuis le SSM
parameter = ssm.get_parameter(Name="/openaikey", WithDecryption=True)


def correct_resume_tags(text):
    """
    Corrige les balises 'résumé', 'resume', 'titre' et leurs variantes en 'resume' et 'titre' respectivement dans le texte généré.

    Args:
        text (str): Texte à corriger.

    Returns:
        str: Texte avec les balises corrigées.
    """
    # Remplacement des variantes de 'résumé' en 'resume'
    corrected_text = re.sub(r"\[résumé\]", "[resume]", text, flags=re.IGNORECASE)
    corrected_text = re.sub(
        r"\[end_résumé\]", "[end_resume]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[résume\]", "[resume]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[end_résume\]", "[end_resume]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[resume\]", "[resume]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[end_resume\]", "[end_resume]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[Resume\]", "[resume]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[End_Resume\]", "[end_resume]", corrected_text, flags=re.IGNORECASE
    )

    # Remplacement des variantes de 'titre' en 'titre'
    corrected_text = re.sub(
        r"\[titre\]", "[titre]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[end_titre\]", "[end_titre]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[Titre\]", "[titre]", corrected_text, flags=re.IGNORECASE
    )
    corrected_text = re.sub(
        r"\[End_Titre\]", "[end_titre]", corrected_text, flags=re.IGNORECASE
    )

    return corrected_text


def get_language_description(language):
    """
    Retourne la description de la langue en français pour le prompt.

    Args:
        language (str): Langue de l'histoire.

    Returns:
        str: Description de la langue en français.
    """
    language_mapping = {
        "en": "Anglaise",
        "fr": "Française",
        "es": "Espagnole",
        "de": "Allemande",
        "it": "Italienne",
        "pt": "Portugaise",
    }
    return language_mapping.get(language, "Française")


def extract_dimensions(size):
    """
    Extrait les dimensions (largeur et hauteur) d'une chaîne de taille de la forme 'widthxheight'.

    Args:
        size (str): Taille sous forme de chaîne, par exemple '1024x1024'.

    Returns:
        tuple: Tuple contenant la largeur et la hauteur en entiers.
    """
    width, height = map(int, size.split("x"))
    return width, height


def update_dynamodb(request_id, status, result_url=None):
    """
    Met à jour une entrée dans la table DynamoDB avec l'ID de la requête, le statut et l'URL du résultat.

    Args:
        request_id (str): ID de la requête.
        status (str): Statut de la requête.
        result_url (str, optional): URL du résultat. Defaults to None.

    Returns:
        dict: Réponse de DynamoDB.
    """
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("TaskStatus")

    item = {"requestId": request_id, "status": status}
    if result_url is not None:
        item["resultUrl"] = result_url

    response = table.put_item(Item=item)
    return response


def extract_summaries(text):
    """
    Extrait les résumés du texte en utilisant des balises spécifiques.

    Args:
        text (str): Texte contenant les résumés.

    Returns:
        list: Liste des résumés extraits.
    """
    pattern = r"\[resume\](.*?)\[end_resume\]"
    summaries = re.findall(pattern, text, re.DOTALL)
    return summaries


def generate_image_instructions(prompt, style, language):
    """
    Génère les instructions pour la création d'images.

    Args:
        prompt (str): Description de l'image à générer.
        style (str): Style de l'image.

    Returns:
        str: Instructions formatées pour la génération d'images.
    """
    language_description = get_language_description(language)
    logger.info(
        f"""
    Génère un dessin pour enfant dans le style "{style}" basé sur cette description en langue "{language_description}" : {prompt}.
    La scène doit être purement visuelle, sans aucun texte, et conçue pour éveiller l'émerveillement chez les jeunes spectateurs. 
    """
    )
    return f"""
    Génère un dessin pour enfant dans le style "{style}" basé sur cette description en langue "{language_description}" : {prompt}.
    La scène doit être purement visuelle, sans aucun texte, et conçue pour éveiller l'émerveillement chez les jeunes spectateurs. 
    """


def generate_image_titan_instructions(prompt, style, language):
    """
    Génère un message d'instruction pour l'image Titan.

    Args:
        prompt (str): Description de l'image à générer.
        style (str): Style de l'image.

    Returns:
        str: Message d'instruction limité à 255 caractères.
    """
    language_description = get_language_description(language)
    logger.info(
        f"""Génère un dessin pour enfant dans le style "{style}" basé sur cette description en langue "{language_description}" : {prompt}"""[
            :512
        ]
    )
    return f"""Génère un dessin pour enfant dans le style "{style}" basé sur cette description en langue "{language_description}" : {prompt}"""[
        :512
    ]


def generate_story_instructions(prompt, language):
    """
    Génère les instructions pour créer une histoire captivante pour enfants.

    Args:
        prompt (str): Texte source pour inspirer l'histoire.
        language (str): Langue de l'histoire.

    Returns:
        str: Instructions formatées pour la génération de l'histoire.
    """
    language_description = get_language_description(language)
    return f"""
    Crée une histoire unique de 1000 à 1500 mots, captivante et riche en descriptions visuelles pour enfants uniquement dans la langue "{language_description}", inspirée par : "{prompt}". Cette histoire doit mêler aventure, magie, et enseigner des valeurs importantes telles que l'amitié, le courage, la persévérance, l'empathie, et la gentillesse.

    L'histoire peut aborder des thèmes comme : l'amitié entre un enfant humain et un animal merveilleux, la découverte d'un monde magique caché, un long voyage vers une contrée enchantée, un enfant qui découvre qu'il/elle possède des pouvoirs magiques spéciaux et doit apprendre à les maîtriser, une quête pour sauver une créature légendaire en danger, un voyage à travers des royaumes féeriques pour briser un sortilège ancien, une aventure sous-marine dans un monde marin peuplé de sirènes et de créatures fantastiques, une mission pour réunir des objets magiques dispersés afin d'empêcher un grand cataclysme, une compétition amicale entre enfants dotés de capacités extraordinaires dans une école de sorcellerie, etc.
    L'histoire peut également explorer : l'acceptation de soi à travers un personnage unique comme un enfant métamorphe, la découverte d'une ancienne civilisation perdue et de ses secrets, une épopée pour retrouver des parents disparus dans un monde parallèle, une lutte contre les forces des ténèbres menaçant d'engloutir un royaume enchanté, etc.
    N'hésites pas à combiner plusieurs de ces idées pour créer une trame narrative riche et captivante. Tu peux aussi t'inspirer de contes ou légendes traditionnels et leur donner un nouvel éclairage fantastique adapté aux enfants.
    Raconte l'histoire au présent pour une immersion maximale.

    Instructions spécifiques :
    - Utilise des phrases courtes et simples, adaptées pour des enfants de moins de 10 ans.
    - Intègre des dialogues dynamiques et réalistes pour rendre l'histoire vivante.
    - Choisis des mots simples pour une meilleure compréhension par de jeunes lecteurs.
    - Crée des personnages diversifiés en termes d'âge, de genre, d'origine ethnique et de capacités. Assure-toi que l'apparence des personnages (cheveux, yeux, taille, etc.) est précisée au niveau du résumé si jamais ils doivent y apparaître pour être cohérent avec le texte de l'histoire.
    - Attribue des traits de personnalité uniques, des intérêts, des peurs et des rêves à chaque personnage pour une caractérisation approfondie.
    - Développe les personnages et leurs relations tout au long de l'histoire en montrant leurs interactions, leurs moments de partage et leur évolution.
    - Crée des conflits émotionnels et intellectuels, au-delà des défis physiques.
    - Décris en détail les défis physiques et les actions des personnages pour les surmonter. Par exemple, lorsqu'ils traversent la forêt, mentionne les branches qui les gênent, les racines sur lesquelles ils trébuchent, la végétation dense qu'ils doivent écarter. Montre leur fatigue, leurs efforts pour avancer, les émotions qu'ils ressentent face à ces difficultés.
    - Fais échouer les personnages principaux à un moment donné. Montre comment ils gèrent cet échec et essaient à nouveau. Décris en détail leurs sentiments de doute, de frustration ou de découragement, et comment ils puisent dans leur détermination et leur amitié pour surmonter cet obstacle. Assure-toi que l'échec est significatif et impacte réellement la progression de l'histoire.
    - Crée des conflits entre les personnages principaux, ou entre les personnages principaux et les personnages secondaires.
    - Ajoute des rebondissements et des défis supplémentaires pour maintenir l'intérêt des jeunes lecteurs. Décris en détail la réaction des personnages face à ces rebondissements, leurs émotions, leurs doutes et leurs efforts pour s'adapter à la nouvelle situation.
    - Résous les conflits de manière créative et non violente, en mettant l'accent sur le pouvoir de la communication et de la coopération.
    - Développe les antagonistes en leur donnant des motivations claires, des traits de personnalité distincts et des capacités redoutables qui les rendent réellement menaçants pour les héros. Décris en détail leurs actions pour contrecarrer ou mettre en échec les héros à plusieurs reprises au cours de l'histoire. Montre comment leur présence et leurs actions sèment le doute, la peur ou le découragement chez les héros avant qu'ils ne parviennent à les surmonter.
    - Assure-toi que le récit comporte une structure narrative claire avec une introduction captivante, de l'action, des conflits, et une résolution.
    - Ajoute un objectif clair pour les personnages à atteindre et un accomplissement significatif à la fin de l'histoire.
    - Inclue des moments de réflexion ou d'émotion pour permettre aux lecteurs de se connecter aux personnages et à leurs aventures.
    - Varie les interactions entre les personnages pour éviter les répétitions et maintenir l'intérêt.
    - Maintiens un bon rythme dans l'histoire en alternant des scènes d'action, de réflexion et d'émotion. Ajoute des éléments de suspense pour maintenir l'intérêt des jeunes lecteurs.
    - Utilise abondamment des descriptions visuelles riches en couleurs, en textures et en formes pour stimuler l'imagination des enfants et créer un monde immersif.
    - Inclue des descriptions sensorielles pour enrichir l'expérience narrative (sons, odeurs, textures).
    - Chaque personnage doit avoir une motivation claire et des traits de caractère distincts.
    - Assure-toi que chaque chapitre se termine par un cliffhanger ou une question ouverte pour maintenir l'intérêt des lecteurs.
    - Ajoute des éléments éducatifs subtils (faits scientifiques, connaissances culturelles) pour enrichir l'histoire sans alourdir le récit.
    - Enrichis les descriptions sensorielles pour permettre aux lecteurs de vraiment "voir", "entendre" et "ressentir" l'environnement des personnages.
    - Personnalise l'histoire avec des noms ou des éléments familiers pour une connexion émotionnelle plus forte.
    - Intègre des questions de réflexion et d'interaction pour engager les enfants.
    - Ajoute des éléments d'humour et des jeux de mots pour rendre l'histoire amusante.
    - Utilise des illustrations mentales vives et détaillées pour stimuler l'imagination.
    - Intègre une leçon morale ou un message éducatif de manière naturelle dans le récit.
    - Intègre des messages positifs et encourageants dans tes histoires, comme l'importance de croire en soi, de poursuivre ses rêves et de surmonter les obstacles.
    - Ajoute des éléments d'humour et de légèreté dans tes histoires pour les rendre plus amusantes et agréables à lire pour les enfants.
    - Intègre des éléments éducatifs dans tes histoires de manière subtile et ludique, comme des métaphores pour enseigner des concepts scientifiques ou des voyages dans différents pays pour enseigner la géographie et les cultures.
    - Ajoute des éléments interactifs dans tes histoires, comme des questions aux enfants, des choix qui influencent l'histoire, ou des petits défis ou jeux à réaliser.

    Ajoute des difficultés et des obstacles significatifs pour rendre l'histoire plus engageante et permettre aux héros de montrer leur courage et leur ingéniosité :
    - Développe les antagonistes en leur donnant des motivations claires, des traits de personnalité distincts et des capacités redoutables qui les rendent réellement menaçants pour les héros. Décris en détail leurs actions pour contrecarrer ou mettre en échec les héros à plusieurs reprises au cours de l'histoire. Montre comment leur présence et leurs actions sèment le doute, la peur ou le découragement chez les héros avant qu'ils ne parviennent à les surmonter.
    - Décris chaque affrontement au niveau quasi "temps réel", avec les actions, réactions, émotions, blessures, etc. détaillées pas à pas, presque comme si on y assistait. Intègre des éléments de surprise, de retournements inattendus au cours de ces affrontements pour augmenter le suspense. Montre comment les capacités et l'ingéniosité des antagonistes poussent les héros dans leurs derniers retranchements.
    - Lorsque les héros échouent, prends le temps de décrire en détail leurs émotions négatives (déception, frustration, colère, tristesse, etc.) et leurs doutes intérieurs. Montre qu'ils remettent en question leur capacité à poursuivre leur quête à la suite de ces échecs cuisants. Fais en sorte qu'ils aient besoin d'un véritable déclic intérieur, motivé par l'amitié ou leurs valeurs, pour se relever et persévérer. Montre comment cela impacte leurs relations entre eux (reproches, disputes, tensions, ou au contraire un élan de solidarité).
    - Décris les affrontements physiques ou psychologiques étape par étape, en montrant les actions, réactions et émotions ressenties de part et d'autre. N'hésite pas à inclure des blessures, de la souffrance ou de la peur pour les héros lors de ces affrontements acharnés. Fais en sorte que la victoire des héros ne soit jamais acquise d'avance et nécessite des sacrifices ou des prises de risque de leur part.
    - Crée des situations où les héros doivent collaborer et utiliser leurs compétences spécifiques pour réussir.
    - Intègre des moments de doute ou de découragement pour montrer la persévérance des héros. Décris leurs luttes internes et comment ils trouvent la force de continuer. Fais en sorte que les héros aient besoin d'un véritable déclic intérieur, motivé par l'amitié ou leurs valeurs, pour se relever et persévérer.
    - Ajoute des moments où l'amitié ou la confiance entre les héros est mise à rude épreuve par les difficultés rencontrées. Montre comment ils doivent surmonter leurs doutes, leur colère ou leur rancune les uns envers les autres pour rester soudés. Décris leurs prises de conscience, leurs excuses et leur cheminement pour renouer des liens forts malgré l'adversité.
    - Place les héros dans des situations où ils doivent faire un choix difficile qui aura des conséquences douloureuses (abandonner un compagnon, renoncer à un rêve, etc.). Montre leur dilemme intérieur, leur déchirement avant de faire ce choix douloureux pour un plus grand bien. N'aie pas peur d'inclure des pertes, des renoncements ou des traumatismes marquants issus de ces choix cornéliens.
    - Fais en sorte que les personnages apprennent et grandissent à travers les difficultés qu'ils rencontrent.
    - Ajoute des rebondissements inattendus qui changent la direction de l'histoire et maintiennent l'intérêt des lecteurs. Décris en détail la réaction des personnages face à ces rebondissements, leurs émotions, leurs doutes et leurs efforts pour s'adapter à la nouvelle situation.
    - Fais en sorte que les antagonistes infligent de véritables blessures physiques et/ou psychologiques aux héros au cours des affrontements. Décris ces blessures, la douleur ressentie, l'impact sur leur moral et leurs capacités à avancer. Montre leur résolution, leur courage pour continuer malgré ces handicaps.
    - Assure-toi que chaque défi est pertinent pour l'histoire et contribue au développement des personnages.
    - Décris en détail chaque énigme ou défi rencontré par les personnages. Par exemple, si les enfants doivent résoudre des énigmes chantées par les vents, précise le contenu de ces énigmes et la manière dont les enfants trouvent les réponses grâce à leur persévérance ou à l'aide de personnages secondaires.
    - Lorsque les personnages surmontent un obstacle, montre le processus complet de leurs tentatives, incluant les échecs et les efforts qu'ils font avant de réussir. Par exemple, détaille comment ils essaient plusieurs méthodes pour résoudre une énigme ou surmonter un défi avant de finalement trouver la solution.
    - Intègre des dialogues et des interactions entre les personnages et les gardiens ou les antagonistes qui posent des défis. Par exemple, si un enfant des vents protège un objet précieux, décris la conversation où il teste la patience des héros et les réactions des enfants face à ce test.
    - Ajoute des descriptions des émotions et des pensées des personnages lorsqu'ils font face à des épreuves difficiles, montrant leur détermination, leurs doutes, et comment ils surmontent ces sentiments pour réussir.
    - Assure-toi que chaque défi est clairement expliqué avec des indices et des solutions logiques que les enfants peuvent comprendre et suivre. Par exemple, spécifie les indices que les héros utilisent pour résoudre les énigmes et comment ces indices les mènent à la solution.


    IMPORTANT : Ne traduisez ni modifiez pas les balises suivantes :
    [titre]Ton titre ici[end_titre] (balises de titre)
    [resume] et [end_resume] (balises de résumé)
    N'ajoutez aucune autre balise que celles spécifiées ci-dessus.

    Voici comment structurer les descriptions visuelles inspirées par : "{prompt}" :
    - Commence chaque description avec la balise [resume] et finis avec la balise [end_resume]. Ne traduisez ni modifiez pas ces balises.
    - Les descriptions doivent se concentrer exclusivement sur les éléments visuels sans inclure d'actions ou de dialogues des personnages.
    - Chaque élément clé mentionné dans le prompt initial doit être décrit de manière unique et détaillée.
    - Ne mentionne chaque élément (personnage, animal, lieu, objet clé) qu'une seule fois dans les descriptions visuelles. Une fois qu'un élément a été décrit, ne le mentionne plus dans les descriptions suivantes, même indirectement.
    - Utilise des descriptions riches en couleurs, en textures et en formes pour stimuler l'imagination visuelle.
    - Inclue des éléments fantastiques, magiques ou surréalistes pour rendre les scènes plus intéressantes et mémorables.
    - Veille à ce que chaque description soit suffisamment détaillée pour permettre la création d'une illustration complète.

    Exemple de structure de descriptions visuelles (ces exemples sont seulement pour référence, ne les utilisez pas tels quels dans l'histoire) :
    [resume]Un koala super sympa avec une fourrure douce et grise, des yeux pétillants et un sourire amical. Il est assis sur une branche d'eucalyptus, grignotant des feuilles et observant son environnement avec curiosité.[end_resume]
    [resume]Un escargot très méchant avec une coquille noire et luisante, et des yeux perçants qui semblent voir à travers tout. Il se déplace lentement mais de manière menaçante, laissant derrière lui une traînée de bave visqueuse.[end_resume]
    [resume]Un arbre magique avec des feuilles d'un bleu profond qui brillent comme des étoiles. Des oiseaux de toutes les couleurs chantent autour des branches, ajoutant une mélodie enchantée à l'atmosphère mystique.[end_resume]

    Assure-toi que chaque description visuelle est riche, détaillée et entièrement nouvelle, sans aucune répétition d'éléments précédents. Évite d'utiliser les exemples fournis ci-dessus et crée des descriptions fraîches pour chaque scène.

    La conclusion de l'histoire doit renforcer les thèmes de l'aventure et de l'amitié avec une touche plus percutante, et être accompagnée d'une dernière description visuelle marquante.
    [resume]Visualise le chemin de retour à travers un paysage unique et magique, différent pour chaque histoire. Par exemple, un pont arc-en-ciel, un sentier lumineux sous une pluie d'étoiles filantes, des pas dans le sable avec un soleil couchant, etc. Assure-toi que la description finale est riche en détails visuels et évoque une atmosphère enchantée et inoubliable.[end_resume]

    Pour varier les débuts d'histoire et éviter la répétition, choisis parmi les exemples suivants, ou laisse libre cours à ton imagination :
    - Une classe à l'école, un voyage en famille, une fête d'anniversaire, une visite chez les grands-parents, un jour de pluie où les enfants jouent à l'intérieur, une sortie en nature, etc.
    - La découverte d'un livre magique, une rencontre inattendue avec un personnage mystérieux, un rêve étrange qui devient réalité, un message secret trouvé dans une bouteille, un animal parlant qui apparaît soudainement, etc.
    - Des personnages principaux différents : une fratrie, des amis, un enfant et son grand-parent, un groupe de camarades de classe, etc.
    - Des lieux de départ variés : une maison en ville, une cabane dans les bois, un appartement au bord de la mer, une ferme, une école, etc.
    - Déclencheur de l'aventure variés aussi : un portail vers un monde magique, un objet mystérieux trouvé dans le grenier, un événement étrange comme une éclipse ou une étoile filante, un animal parlant qui a besoin d'aide, un visiteur de l'espace, etc.

    Cette structure aide à créer un récit harmonieux et visuellement riche, propice à l'illustration et captivant pour les enfants.
    Attention, je te rappelle la langue cible de l'histoire : "{language_description}"
    """


def generate_story(
    prompt, model_type, model_id, language, api_key=None, region_name="us-east-1"
):
    """
    Génère une histoire en utilisant le modèle spécifié.

    Args:
        prompt (str): Texte source pour l'histoire.
        model_type (str): Type de modèle à utiliser ('openai', 'mistral', 'anthropic', 'meta').
        model_id (str): ID du modèle à utiliser.
        api_key (str, optional): Clé API pour OpenAI. Defaults to None.

    Returns:
        str: Histoire générée.
    """
    instruction = generate_story_instructions(prompt, language)

    if model_type == "openai":
        client = OpenAI(api_key=api_key)
        try:
            response = client.chat.completions.create(
                model=model_id,
                messages=[
                    {
                        "role": "system",
                        "content": "Vous êtes un assistant AI expert des histoires pour enfant.",
                    },
                    {"role": "user", "content": instruction},
                ],
            )
            first_choice_message = response.choices[0].message
            return first_choice_message.content
        except Exception as e:
            return f"Une erreur est survenue : {e}"

    elif model_type in ["mistral", "anthropic", "meta"]:
        client = boto3.client("bedrock-runtime", region_name=region_name)

        if model_type == "meta":
            # Embed the prompt in Llama 3's instruction format.
            formatted_prompt = f"""
            <|begin_of_text|>
            <|start_header_id|>user<|end_header_id|>
            {instruction}
            <|eot_id|>
            <|start_header_id|>assistant<|end_header_id|>
            """
            payload = {
                "prompt": formatted_prompt,
                "max_gen_len": 2048,
                "temperature": 0.2,
            }
        elif model_type == "mistral":
            # Format the request payload for Mistral
            formatted_prompt = f"<s>[INST] {instruction} [/INST]"
            payload = {
                "prompt": formatted_prompt,
                "max_tokens": 2800,
                "temperature": 0.2,
            }
        elif model_type == "anthropic":
            # Format the request payload for Anthropic
            payload = {
                "anthropic_version": "bedrock-2023-05-31",
                "max_tokens": 2048,
                "temperature": 0.2,
                "messages": [
                    {
                        "role": "user",
                        "content": [{"type": "text", "text": instruction}],
                    }
                ],
            }

        try:
            response = client.invoke_model(
                modelId=model_id,
                body=json.dumps(payload),
            )
            response_body = json.loads(response["body"].read())
            if model_type == "meta":
                return response_body["generation"]
            elif model_type == "mistral":
                return " ".join([output["text"] for output in response_body["outputs"]])
            elif model_type == "anthropic":
                return " ".join(
                    [content["text"] for content in response_body["content"]]
                )
        except Exception as e:
            print(f"Error invoking {model_type} model: {e}")
            raise


def generate_image_for_each_summary(
    summaries, model, bucket_name, seed, style, size, quality, language
):
    """
    Génère des images pour chaque résumé extrait.

    Args:
        summaries (list): Liste des résumés extraits.
        model (str): Modèle à utiliser pour la génération ('openai', 'titan', 'stable_diffusion').
        bucket_name (str): Nom du bucket S3 où uploader les images.
        seed (int): Valeur de la seed pour la génération de l'image.
        style (str): Style de l'image.
        size (str): Taille de l'image au format 'widthxheight'.
        quality (str): Qualité de l'image ('standard', 'hd', etc.).

    Returns:
        list: Liste des URLs des images générées.
    """
    images_urls = []
    for summary in summaries:
        image_data = generate_image(
            summary, model, seed, style, size, quality, language
        )
        if image_data is not None:
            image_url = upload_to_s3(image_data, bucket_name)
            images_urls.append(image_url)
        else:
            images_urls.append(
                ""
            )  # Ajouter une URL d'image transparente ou d'espace réservé à la place
    return images_urls


def create_html_with_images(text_data, images_urls, generate_images=True):
    """
    Crée un contenu HTML avec le texte et les images.

    Args:
        text_data (str): Texte généré.
        images_urls (list): Liste des URLs des images générées.
        generate_images (bool, optional): Indique si les images doivent être générées. Defaults to True.

    Returns:
        str: Contenu HTML.
    """
    title_match = re.search(r"\[titre\](.*?)\[end_titre\]", text_data)
    if (title_match) is not None:
        title = title_match.group(1)  # Extrait le titre
        text_data = text_data.replace(title_match.group(0), "")
    else:
        title = "Histoire Générée par l'IA"  # Un titre par défaut si aucun n'est trouvé

    html_content = """
    <html>
    <head>
    <title>Histoire générée par l'IA</title>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans:wght@400;700&display=swap" rel="stylesheet">
    <style>
    body { font-family: 'Noto Sans', sans-serif; padding: 20px; }
    p {
        margin-bottom: 1.5em;
        line-height: 1.6;
        text-indent: 20px;
        max-width: 1024px;
        margin-left: auto;
        margin-right: auto;
    }
    img {
        max-width: 100%;
        height: auto;
        margin-bottom: 20px;
        margin-left: auto;
        margin-right: auto;
    }
    .center {
        text-align: center;
    }
    .title {
        font-weight: bold;
        font-size: 24px;
        text-align: center;
        margin: 20px 0;
    }
    .image-caption {
        font-style: italic;
        font-size: 0.8em;
        text-align: center;
        margin-bottom: 10px;
    }
    </style>
    </head>
    <body>
    """

    # Ajouter le titre au contenu HTML
    html_content += f'<div class="title">{title}</div>\n'

    summaries = extract_summaries(text_data)
    segments = re.split(r"\[resume\].*?\[end_resume\]", text_data, flags=re.DOTALL)

    if len(segments) == len(summaries) + 1:
        summaries.append("")

    for i, segment in enumerate(segments):
        formatted_segment = segment.strip().replace(". ", ".<br>")
        html_content += f"<p>{formatted_segment}</p>\n"
        if generate_images and i < len(images_urls) and images_urls[i]:
            image_url = images_urls[i]
            html_content += f'<div class="center"><img src="{image_url}" alt="Image générée"></div>\n'
            # html_content += f'<p class="image-caption">{summaries[i].strip()}</p>\n'
        elif not generate_images and summaries[i]:
            html_content += f'<p class="image-caption">{summaries[i].strip()}</p>\n'

    html_content += "</body></html>"
    return html_content


def generate_image(prompt, model, seed, style, size, quality, language):
    """
    Génère une image en utilisant le modèle spécifié.

    Args:
        prompt (str): Texte décrivant l'image à générer.
        model (str): Modèle à utiliser pour la génération ('openai', 'titan', 'stable_diffusion').
        seed (int): Valeur de la seed pour la génération de l'image.
        style (str): Style de l'image à générer.
        size (str): Taille de l'image au format 'widthxheight'.
        quality (str): Qualité de l'image ('standard', 'hd', etc.) applicable pour OpenAI.

    Returns:
        str: Image encodée en base64.
    """
    width, height = extract_dimensions(size)

    if model == "openai":
        client = OpenAI(api_key=parameter["Parameter"]["Value"])
        adjusted_prompt = generate_image_instructions(prompt, style, language)

        try:
            response = client.images.generate(
                prompt=adjusted_prompt,
                model=os.environ.get("OPENAI_IMAGE_MODEL"),
                n=1,
                size=size,
                response_format="b64_json",
                quality=quality,
                user="user_id",
            )
            image_data = response.data[0].b64_json
            return image_data
        except Exception as e:
            logger.error(f"Error generating image with OpenAI: {str(e)}", exc_info=True)
            return None

    elif model == "titan":
        client = boto3.client("bedrock-runtime", region_name=region_name)
        adjusted_prompt = generate_image_titan_instructions(prompt, style, language)
        payload = {
            "taskType": "TEXT_IMAGE",
            "textToImageParams": {"text": adjusted_prompt},
            "imageGenerationConfig": {
                "numberOfImages": 1,
                "cfgScale": 9,
                "height": height,
                "width": width,
                "seed": seed,
            },
        }
        try:
            response = client.invoke_model(
                modelId=os.environ.get("TITAN_IMAGE_MODEL"), body=json.dumps(payload)
            )
            response_data = json.loads(response["body"].read())
            return response_data["images"][0]
        except Exception as e:
            logger.error(f"Error generating image with Titan: {str(e)}", exc_info=True)
            return None

    elif model == "stable_diffusion":
        client = boto3.client("bedrock-runtime", region_name=region_name)
        adjusted_prompt = generate_image_instructions(prompt, style, language)
        payload = json.dumps(
            {
                "text_prompts": [{"text": adjusted_prompt, "weight": 1.0}],
                "cfg_scale": 25,
                "seed": seed,
                "steps": 50,
                "width": width,
                "height": height,
            }
        )

        try:
            response = client.invoke_model(
                modelId=os.environ.get("STABLE_DIFFUSION_IMAGE_MODEL"), body=payload
            )
            response_body = json.loads(response.get("body").read())
            return response_body["artifacts"][0].get("base64")
        except Exception as e:
            logger.error(
                f"Error generating image with Stable Diffusion: {str(e)}", exc_info=True
            )
            return None

    else:
        logger.error(f"Unknown model type: {model}")
        return None


def create_html_content(text_data):
    """
    Crée le contenu HTML pour le texte généré.

    Args:
        text_data (str): Texte généré.

    Returns:
        str: Contenu HTML.
    """
    html_content = "<html><head><title>Generated Text</title></head><body>"
    html_content += f"<p>{text_data}</p>"
    html_content += "</body></html>"
    return html_content


def upload_to_s3(content, bucket_name, content_type="image/jpeg"):
    """
    Uploade le contenu généré sur S3.

    Args:
        content (str): Contenu à uploader.
        bucket_name (str): Nom du bucket S3.
        content_type (str, optional): Type de contenu. Defaults to "image/jpeg".

    Returns:
        str: URL du contenu uploadé.
    """
    s3_client = boto3.client("s3")
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    if content_type == "image/jpeg":
        file_name = f"generated_images/{timestamp}.jpg"
        content_to_upload = base64.b64decode(content)
    else:
        file_name = f"generated_content/{timestamp}.html"
        content_to_upload = content.encode("utf-8")
        content_type = "text/html; charset=utf-8"

    try:
        s3_client.put_object(
            Bucket=bucket_name,
            Key=file_name,
            Body=content_to_upload,
            ContentType=content_type,
        )
        if content_type == "text/html; charset=utf-8":
            return f"https://{os.environ['CLOUDFRONT_DOMAIN']}/{file_name}"
        return f"https://{bucket_name}.s3.amazonaws.com/{file_name}"
    except Exception as e:
        print(f"Error uploading content to S3: {e}")
        raise


def lambda_handler(event, context):
    """
    Fonction principale du lambda handler pour traiter les événements reçus.

    Args:
        event (dict): Événement déclencheur.
        context (dict): Contexte d'exécution.

    Returns:
        dict: Réponse HTTP.
    """
    logger.info("Début de la fonction lambda_handler.")
    logger.info(f"Corps de la requête reçu: {event.get('body')}")
    request_id = event.get("requestId")
    logger.info(f"ID de la requête: {request_id}")

    if isinstance(event.get("body"), dict):
        body = event.get("body")  # Le corps est déjà un dictionnaire
    else:
        body = json.loads(event.get("body", "{}"))  # Sinon, on le parse

    prompt = body.get(
        "text",
        "Tom et Zoé frères à la recherche de la maison en banane. Sur leur route ils croisent le grand dinosaure qui les guide",
    )
    generate_images = body.get("generate_images", True)
    size = body.get("size", "1024x1024")
    style = body.get("style", "cartoon")
    quality = body.get("quality", "standard")
    seed = body.get("seed", 42)
    language = body.get("language", "fr")
    image_generation_model = body.get("image_generation_model", "openai").lower()
    bucket_name = os.environ.get("BUCKET_NAME")
    cloudfront_domain = os.environ.get("CLOUDFRONT_DOMAIN")
    titan_image_model = os.environ.get("TITAN_IMAGE_MODEL")
    stable_diffusion_image_model = os.environ.get("STABLE_DIFFUSION_IMAGE_MODEL")
    openai_image_model = os.environ.get("OPENAI_IMAGE_MODEL")
    anthropic_story_model = os.environ.get("ANTHROPIC_STORY_MODEL")
    mistral_story_model = os.environ.get("MISTRAL_STORY_MODEL")
    meta_story_model = os.environ.get("META_STORY_MODEL")
    openai_story_model = os.environ.get("OPENAI_STORY_MODEL")
    story_generation_model = body.get("story_generation_model", "openai")

    logger.info(f"Génération d'image: {generate_images}")

    if not prompt:
        return {
            "statusCode": 400,
            "body": json.dumps({"error": "Text prompt is required"}),
        }
    logger.info(f"Prompt reçu: {prompt}")
    logger.info(f"Méthode de génération choisie: {story_generation_model}")
    try:
        update_dynamodb(request_id, "Processing")

        if story_generation_model in ["anthropic", "mistral", "openai", "meta"]:
            model_id = {
                "anthropic": anthropic_story_model,
                "mistral": mistral_story_model,
                "openai": openai_story_model,
                "meta": meta_story_model,
            }.get(story_generation_model, openai_story_model)

            api_key = (
                parameter["Parameter"]["Value"]
                if story_generation_model == "openai"
                else None
            )
            text_data = generate_story(
                prompt, story_generation_model, model_id, language, api_key
            )
            logger.info(f"Texte généré par l'ia : {text_data}")

        logger.info("Texte généré avec succès")
        text_data = correct_resume_tags(text_data)
        # Extraction des résumés pour la génération d'image
        summaries = extract_summaries(text_data)
        logger.info(f"Nombre de résumés extraits: {len(summaries)}")
        style_with_spaces = style.replace("_", " ")
        if generate_images and summaries:
            # Génération d'images basées sur les résumés extraits
            images_urls = generate_image_for_each_summary(
                summaries,
                image_generation_model,
                bucket_name,
                seed,
                style_with_spaces,
                size,
                quality,
                language,
            )
            logger.info("Images générées et URL récupérées")
        else:
            images_urls = []

        # Construction du contenu HTML avec les images
        html_content = create_html_with_images(text_data, images_urls, generate_images)
        if html_content:
            update_dynamodb(request_id, "Content Generated")
        # Upload du contenu HTML sur S3
        response_message = upload_to_s3(
            html_content, bucket_name, content_type="text/html"
        )
        logger.info("Contenu HTML uploadé avec succès")
        response_body = {"message": response_message}
        logger.info(f"Réponse : {response_message}")
        update_dynamodb(request_id, "link", response_message)
        return {
            "isBase64Encoded": False,
            "statusCode": 200 if summaries else 400,
            "headers": {"Content-Type": "application/json"},
            "body": json.dumps(response_body),
        }

    except Exception as e:
        logger.error(
            f"Erreur lors de l'exécution de la fonction lambda: {str(e)}", exc_info=True
        )
        update_dynamodb(request_id, "Failed")
        return {
            "statusCode": 500,
            "isBase64Encoded": False,
            "headers": {"Content-Type": "application/json"},
            "body": json.dumps({"error": str(e)}),
        }
