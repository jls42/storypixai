# Archive du répertoire de la fonction Lambda en un fichier zip
data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "${path.module}/lambdas/status_checker"
  output_path = "${path.module}/lambdas/status_checker.zip"
}

# Définition de la fonction Lambda
resource "aws_lambda_function" "status_checker" {
  function_name    = "StatusCheckerFunction"
  handler          = "status_checker.lambda_handler"
  runtime          = var.python_runtime
  role             = aws_iam_role.status_checker_exec_role.arn
  filename         = data.archive_file.lambda_zip.output_path
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256

  environment {
    variables = {
      DYNAMODB_TABLE = "TaskStatus"
    }
  }
}

# Création du rôle IAM pour la fonction Lambda
resource "aws_iam_role" "status_checker_exec_role" {
  name = "status_checker_execution_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# Définition de la politique IAM pour l'accès à DynamoDB
resource "aws_iam_policy" "lambda_dynamodb_access" {
  name = "lambda_dynamodb_access"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:GetItem",
          "dynamodb:Scan",
          "dynamodb:Query"
        ]
        Resource = "*"
        Effect   = "Allow"
      },
    ]
  })
}

# Attachement de la politique IAM au rôle IAM
resource "aws_iam_role_policy_attachment" "lambda_dynamodb_access_attachment" {
  role       = aws_iam_role.status_checker_exec_role.name
  policy_arn = aws_iam_policy.lambda_dynamodb_access.arn
}
