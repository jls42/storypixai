terraform {
  # Configuration du backend pour stocker l'état Terraform
  backend "s3" {
    # Les détails du backend S3 (comme le bucket, la clé et la région) doivent être spécifiés ici ou dans un fichier de configuration distinct.
  }

  # Déclaration des fournisseurs nécessaires pour votre projet
  required_providers {
    # Configuration du fournisseur AWS
    aws = {
      source  = "hashicorp/aws" # Indique que le fournisseur AWS provient de HashiCorp
      version = "~> 5.36"       # Spécifie la version (ou une version compatible) du fournisseur AWS à utiliser
    }
    # Configuration du fournisseur d'archive
    archive = {
      source  = "hashicorp/archive" # Indique que le fournisseur d'archive provient de HashiCorp
      version = "~> 2.0"            # Spécifie la version (ou une version compatible) du fournisseur d'archive à utiliser
    }
  }

  # Spécifie la version minimale de Terraform requise pour exécuter cette configuration
  required_version = ">= 0.12" # Assure que Terraform version 0.12 ou plus récente est utilisée
}
