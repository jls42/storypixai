[[_TOC_]]

# StoryPixAI : Histoires du soir pour enfants générées et illustrées par l'IA

StoryPixAI est un projet serverless innovant de génération d'histoires du soir magiques, créées par l'IA. Rédigez simplement quelques mots sur le thème de leur choix et laissez l'intelligence artificielle tisser une histoire du soir unique et captivante, accompagnée d'une illustration adorable. Le tout est généré automatiquement dans la langue de votre choix grâce aux modèles d'IA de pointe d'OpenAI et Amazon Bedrock. Offrez à vos enfants un moment magique avant de s'endormir, tout en profitant d'une expérience utilisateur fluide grâce à la puissance de l'infrastructure AWS serverless.

## Fonctionnalités

* **Histoires du soir personnalisées créées par l'IA :** Transformez les idées de vos enfants en histoires du soir originales, grâce à la puissance des modèles d'IA d'OpenAI et AWS Bedrock.
* **Illustrations adorables générées par l'IA :** Chaque histoire prend vie avec des images mignonnes et colorées, parfaites pour stimuler l'imagination de vos enfants.
* **Sauvegarde automatique sur le cloud AWS :** StoryPixAI enregistre automatiquement vos histoires et illustrations sur un bucket S3, accessible via un lien unique pour les relire à tout moment.
* **Accès sécurisé au générateur :** L'interface de création d'histoires est protégée par une authentification, garantissant ainsi un espace de création privé et sécurisé.
* **Multilingue :** StoryPixAI prend en charge les langues suivantes :
    * Français (principalement testé)
    * Anglais
    * Espagnol
    * Allemand
    * Italien
    * Portugais

# Architecture

StoryPixAI repose sur une architecture serverless :

## Schéma d'architecture de l'infrastucture sans serveur
![alt text](/img/storypixai-architecture.png)

## Frontend

* **Interface de génération (`index.html`) :** L'interface utilisateur de StoryPixAI est une Single Page Application (SPA) développée en HTML, CSS et JavaScript pur (Vanilla JavaScript). Elle offre une expérience utilisateur intuitive et interactive pour :  
    ![alt text](/img/generateur.png)

    * **Authentification :** Les utilisateurs doivent se connecter avec leurs identifiants Cognito pour accéder aux fonctionnalités de génération. Une fenêtre modale de connexion est fournie pour faciliter ce processus.

    * **Sélection de la langue :** Un menu déroulant permet de choisir la langue du générateur et de l'histoire parmi celles prises en charge : français, anglais, espagnol, allemand, italien et portugais. La langue sélectionnée est utilisée pour l'interface utilisateur et pour guider la génération de l'histoire.  
        ![alt text](/img/langues.png)
    * **Saisie du prompt :** Un champ de texte permet à l'utilisateur de saisir l'idée ou le thème de l'histoire souhaitée.

    * **Personnalisation de la génération (optionnel) :** L'interface propose des options pour personnaliser la génération :
        * **Choix du modèle de génération de texte :** OpenAI GPT-4-o, Anthropic Claude 3, ou Mistral AI.  
            ![alt text](/img/texte_modele.png)
        * **Choix du modèle de génération d'image :** OpenAI DALL-E 3 ou Stable Diffusion XL.  
            ![alt text](/img/image_modele.png)
        * **Paramètres spécifiques au modèle d'image :** Taille, style (pour Stable Diffusion XL) et qualité (pour DALL-E 3).  
            ![alt text](/img/styles.png)  
        * **Seed :** Un champ numérique permet de spécifier une valeur pour initialiser le générateur aléatoire.
        * **Activation/désactivation de la génération d'image.**

    * **Lancement de la génération :** Un bouton "Générer" déclenche le processus de génération de l'histoire et de l'image (si activée).

    * **Suivi de la progression :** Un message d'état informe l'utilisateur de la progression de la génération ("Traitement en cours...", "Prêt !", etc.) et de potentielles erreurs.

    * **Affichage du résultat :** Un lien unique vers l'histoire et l'image générées est affiché lorsque la génération est terminée. Ce lien peut être copié et partagé.

* **Gestion de l'authentification et des requêtes :**
    * Le script `storypixai.js` gère l'authentification de l'utilisateur avec Amazon Cognito et utilise le jeton d'accès obtenu pour autoriser les requêtes à l'API Gateway.
    * Il gère également la communication avec les endpoints de l'API Gateway (`/generate` et `/check_status/{task_id}`) pour déclencher la génération et suivre la progression.
    * Des mécanismes de gestion des erreurs et de nouvelles tentatives sont également implémentés pour garantir la fiabilité de l'application.

* **Hébergement et distribution :**
    * L'interface de génération (`index.html`) est hébergée sur Amazon S3 et distribuée via Amazon CloudFront, un réseau de diffusion de contenu (CDN) qui améliore les performances et la disponibilité de l'application pour les utilisateurs du monde entier.



## Backend

* **Fonctions AWS Lambda (Python) :**
    * **`StoryPixAI` :** Cette fonction est le cœur de l'application. Elle prend en entrée un prompt textuel fourni par l'utilisateur via l'interface de génération et orchestre le processus de création de l'histoire et de l'illustration. Elle appelle successivement les API d'OpenAI et/ou Bedrock pour générer le texte de l'histoire et l'image associée. Ensuite, elle stocke l'image dans le bucket S3 et renvoie à l'utilisateur un lien unique vers l'histoire et l'image générées. L'état d'avancement de la génération est stocké dans DynamoDB.

    * **`status_checker` :** Cette fonction est appelée périodiquement par l'interface de génération pour vérifier l'état d'avancement de la création de l'histoire et de l'image. Elle interroge DynamoDB pour récupérer le statut de la tâche en cours et renvoie ce statut au frontend. Cela permet à l'interface de génération de tenir l'utilisateur informé de la progression et de lui indiquer quand l'histoire et l'image sont prêtes.

* **Amazon DynamoDB :** Cette base de données NoSQL clé-valeur est utilisée pour stocker les informations relatives aux tâches de génération d'histoires et d'images. Chaque tâche est identifiée par un `requestId` unique. La table DynamoDB contient les attributs suivants :
    * **`requestId` (clé de hachage) :** Identifiant unique de la tâche.
    * **`status` :** Statut de la tâche (en cours, terminée, erreur).
    * **`resultUrl` :** URL de l'histoire générée (lorsque la tâche est terminée).

    La table DynamoDB possède également deux index secondaires globaux :

    * **`StatusIndex` :** Permet de rechercher les tâches en fonction de leur statut.
    * **`ResultUrlIndex` :** Permet de rechercher les tâches en fonction de l'URL de l'histoire générée.

    La fonction Lambda `status_checker` utilise ces index pour récupérer efficacement le statut et l'URL de l'histoire d'une tâche donnée. Le script JavaScript du frontend interroge ensuite l'endpoint `/check_status/{task_id}` de l'API Gateway pour obtenir ces informations et mettre à jour l'interface utilisateur en conséquence.

* **Amazon S3 :** Ce service de stockage objet héberge l'interface web statique (`index.html`), les images générées, et les histoires générées. Les histoires et les images sont stockées avec des clés uniques générées par la fonction `StoryPixAI`, et ces clés sont renvoyées à l'utilisateur pour lui permettre d'accéder à ses créations.

* **Amazon CloudWatch :** Ce service de surveillance collecte les journaux et les métriques des fonctions Lambda, permettant de suivre les performances de l'application, de détecter les erreurs et de résoudre les problèmes.

# Infrastructure as Code (IaC)

* **Terraform :** L'ensemble de l'infrastructure AWS (Lambda, API Gateway, S3, CloudFront, Cognito, DynamoD, ...) est défini et déployé à l'aide de Terraform. Cela permet une gestion automatisée, reproductible et versionnée de l'infrastructure, facilitant les mises à jour et les modifications.

# Sécurité et authentification

* **Amazon Cognito :** Ce service gère l'authentification de l'utilisateur. L'interface de génération (`index.html`) utilise l'API Cognito pour authentifier l'utilisateur et obtenir un jeton d'accès. Ce jeton est ensuite inclus dans les requêtes aux endpoints `/generate` et `/check_status/{task_id}` pour autoriser l'accès aux fonctions `StoryPixAI` et `status_checker` respectivement.

# Distribution du contenu

* **Amazon CloudFront :** Ce service de réseau de diffusion de contenu (CDN) est utilisé pour distribuer l'interface web statique (`index.html`) de manière rapide et fiable aux utilisateurs du monde entier. Il met en cache le contenu sur des serveurs répartis géographiquement, réduisant ainsi la latence et améliorant l'expérience utilisateur.

# Modèles d'IA

StoryPixAI est compatible avec une combinaison de modèles d'IA de pointe pour générer des histoires et des illustrations de haute qualité :

* **Modèles OpenAI :**
    * **Génération d'histoires :**
        * OpenAI GPT-4-o
    * **Génération d'images :**
        * OpenAI DALL-E 3

* **Modèles accessibles via AWS Bedrock :**
    * **Génération d'histoires :**
        * Anthropic Claude 3 Sonnet (20240229-v1)
        * Mistral AI Mistral Large (2402-v1)
    * **Génération d'images :**
        * StabilityAI Stable Diffusion XL v1


# Prérequis

Avant de commencer à utiliser StoryPixAI, assurez-vous de disposer des éléments suivants :

* **Compte AWS :** Vous aurez besoin d'un compte AWS actif pour déployer et gérer l'infrastructure serverless. Assurez-vous que votre utilisateur IAM dispose des autorisations suivantes (ou équivalentes). **Notez que cette policy est un exemple et peut être adaptée à vos besoins spécifiques en matière de sécurité :**

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "cloudwatch:*",
            "ec2:*",
            "lambda:*",
            "s3:*",
            "ssm:*",
            "logs:*",
            "dynamodb:*",
            "cognito-idp:*",
            "cognito-identity:*",
            "apigateway:*",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:AttachRolePolicy",
            "iam:DetachRolePolicy",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:CreatePolicy",
            "iam:DeletePolicy",
            "iam:PassRole",
            "iam:Tag*",
            "iam:Get*",
            "iam:List*",
            "iam:TagRole",
            "kms:TagResource",
            "iam:CreateServiceLinkedRole",
            "budgets:*",
            "events:*"
          ],
          "Resource": "*"
        }
      ]
    }
    ```

* **Activation des modèles Bedrock :** Assurez-vous que les modèles d'IA suivants sont activés dans votre compte AWS Bedrock :
    * Anthropic Claude 3 Sonnet (20240229-v1)
    * Mistral AI Mistral Large (2402-v1)
    * StabilityAI Stable Diffusion XL v1

[Accès aux modèles Bedrock - us-east-1 ](https://us-east-1.console.aws.amazon.com/bedrock/home?region=us-east-1#/modelaccess)
![alt text](/img/modele_1.png)
![alt text](/img/modele_2.png)


* **Clés d'API OpenAI (si vous souhaitez utiliser les modèles OpenAI)** Pour générer des illustrations de haute qualité avec DALL-E 3, il est fortement recommandé de disposer d'une clé API OpenAI valide. Vous pouvez obtenir votre clé sur le site d'OpenAI. Notez que StoryPixAI peut également générer des images avec Stable Diffusion XL v1 (via AWS Bedrock), mais DALL-E 3 offre généralement des résultats supérieurs.

**Pour déployer via GitLab CI/CD :**

* **Compte GitLab :** Vous aurez besoin d'un compte GitLab valide et d'un accès au projet StoryPixAI sur GitLab.
* **Variables d'environnement GitLab :** Vous devrez configurer les variables d'environnement nécessaires dans votre projet GitLab, notamment les clés d'API AWS et OpenAI (si vous utilisez DALL-E 3).

# Installation et déploiement

## Déploiement via GitLab CI/CD (Recommandé)

StoryPixAI est conçu pour être facilement déployé et mis à jour grâce à un pipeline GitLab CI/CD intégré. Cette méthode de déploiement est **fortement recommandée**, car elle est automatisée, fiable et indépendante de votre système d'exploitation (Linux, macOS, Windows). Le pipeline automatise les étapes de préparation de l'environnement, de vérification de la configuration, de déploiement et de suppression de l'infrastructure.

**Exécution du pipeline :**

1. **Forker le projet :** Créez une copie du projet dans votre propre espace GitLab en vous rendant sur l'URL suivante : [https://gitlab.com/jls42/storypixai](https://gitlab.com/jls42/storypixai)
2. **Cloner le dépôt :**
   ```bash
   git clone https://gitlab.com/votre_utilisateur/storypixai.git # Remplacez "votre_utilisateur" par votre nom d'utilisateur GitLab
   cd storypixai
   ```
3. **Configurer les variables d'environnement :**
   - Allez dans les paramètres CI/CD de votre projet.
   - Définissez les variables suivantes :
     * `AWS_ACCESS_KEY_ID` : Votre clé d'accès AWS.
     * `AWS_SECRET_ACCESS_KEY` : Votre clé secrète AWS.
     * `OPENAI_KEY` (recommandé) : Votre clé API OpenAI (si vous utilisez DALL-E 3 ou GPT4-o).
     * `TF_VAR_cognito_auth` : Une variable contenant la configuration de Cognito au format JSON, par exemple :
       ```json
       {"user": "votre_nom_d_utilisateur", "password": "votre_mot_de_passe_securise"}
       ```

4. **Personnaliser le script `export.sh` :**
   * Ouvrez le fichier `export.sh` et modifiez les variables suivantes pour qu'elles correspondent à votre configuration :
      ```bash
      # le bucket sera créé automatiquement s'il est disponible, choisissez un nom unique
      BUCKET_STATE="votre_nom_de_bucket_pour_l_état_terraform"
      AWS_DEFAULT_REGION="votre_région_aws"
      ```

5. **Configurer Terraform :**
   * Créez un fichier `terraform.tfvars` à la racine du répertoire `terraform` et définissez les variables spécifiques à votre environnement, notamment (voir la section Documentation Terraform-docs plus bas pour plus de détails) :
      ```
      bucket_name = "votre_nom_de_bucket_s3"

6. **Déclencher le pipeline :** 
   - Allez dans la section CI/CD de votre projet GitLab.
   - Choisissez l'étape que vous souhaitez exécuter (par exemple, `Vérification Terraform` ou `Déploiement Terraform`).
   - Cliquez sur le bouton "Play" pour lancer l'étape sélectionnée.   
    ![alt text](/img/cicd.png)

**Note :** Si vous utilisez la CI/CD fournie avec le projet, l'installation de Terraform n'est pas nécessaire. Le déploiement se fera automatiquement via GitLab CI/CD après avoir configuré les variables d'environnement requises.

## Installation sur un poste Linux (Ubuntu 22.04) (Optionnel)

Si vous préférez déployer StoryPixAI manuellement, vous pouvez le faire sur un poste Linux. **Notez que cette méthode a été testée uniquement sur Ubuntu 22.04.** Cependant, il est recommandé d'utiliser la méthode de déploiement via GitLab CI/CD, car elle est plus simple et ne nécessite pas d'installation locale de Terraform.

1. **Cloner le dépôt :**

   ```bash
   git clone https://gitlab.com/jls42/storypixai.git
   cd storypixai
   ```

2. **Configurer les variables d'environnement :**

   * **AWS :**
        * Exécutez `aws configure` et suivez les instructions pour configurer vos identifiants AWS (clés d'accès et clé secrète) et la région par défaut.
   * **OpenAI (Optionnel) :**
        * Exécutez `source export.sh` pour charger les fonctions du script.
        * Exécutez `manage_openai_key put <votre_clé_openai>` pour stocker votre clé OpenAI dans SSM Parameter Store.

## Déploiement via Terraform

1. **Préparer l'environnement AWS :**
    * Suivez les mêmes étapes que pour la configuration des variables d'environnement AWS ci-dessus.

2. **Personnaliser le script `export.sh` :**
   * Ouvrez le fichier `export.sh` et modifiez les variables suivantes pour qu'elles correspondent à votre configuration :
      ```bash
      # le bucket sera créé automatiquement s'il est disponible, choisissez un nom unique
      BUCKET_STATE="votre_nom_de_bucket_pour_l_état_terraform"
      AWS_DEFAULT_REGION="votre_région_aws"
      ```

3. **Configurer Terraform :**
   * Créez un fichier `terraform.tfvars` à la racine du répertoire `terraform` et définissez les variables spécifiques à votre environnement, notamment (voir la section Documentation Terraform-docs plus bas pour plus de détails) :
      ```
      bucket_name = "votre_nom_de_bucket_s3"
      cognito_auth = {
        user     = "votre_nom_d_utilisateur" 
        password = "votre_mot_de_passe_securise"
      }
      ```

4. **Déployer l'infrastructure :**
   ```bash
   source export.sh
   terraform_plan
   terraform_apply
   ```
# Utilisation

1. **Authentification :**
   - Ouvrez votre navigateur web et accédez à l'URL de votre application StoryPixAI.
   - Connectez-vous en utilisant le nom d'utilisateur et le mot de passe que vous avez définis dans la variable `cognito_auth` lors du déploiement.

2. **Création d'une histoire :**
   - Une fois connecté, vous accéderez à l'interface de génération d'histoires.

   - **Choisissez la langue :** Sélectionnez la langue souhaitée dans le menu déroulant en haut à droite de l'écran. Les options disponibles sont : français, anglais, espagnol, allemand, italien et portugais.  

    
   - **Saisissez votre idée d'histoire :** Dans le champ de texte prévu à cet effet, entrez quelques mots ou une phrase qui inspireront l'histoire. Vous pouvez inclure des noms, des lieux, des personnages ou tout autre élément qui vous semble pertinent.
   - **Personnalisez la génération (optionnel) :**
      - **Modèle d'histoire :** Choisissez parmi les modèles disponibles (OpenAI GPT-4-o, Anthropic Claude 3, Mistral AI).  
      - **Modèle d'image :** Choisissez entre OpenAI DALL-E 3 ou Stable Diffusion XL.  
      - **Taille de l'image :** Sélectionnez la taille souhaitée pour l'illustration (les options disponibles dépendent du modèle d'image choisi).
      - **Style d'image :** Choisissez un style parmi la liste proposée pour personnaliser l'esthétique de l'illustration (uniquement pour Stable Diffusion XL).  
      - **Qualité de l'image :** Sélectionnez la qualité de l'image générée par DALL-E 3 (standard ou HD).
      - **Seed :** Entrez un nombre entier pour initialiser le générateur aléatoire et obtenir des résultats différents à chaque génération.
      - **Activer/Désactiver la génération d'image :** Cochez ou décochez la case pour activer ou désactiver la génération d'une illustration pour l'histoire.
   - **Lancez la génération :** Cliquez sur le bouton "Générer".

3. **Suivi de la progression :**
   - L'interface affichera un message "Traitement en cours...".

4. **Accès à l'histoire générée :**
   - Une fois la génération terminée, le message "Prêt!" s'affichera, suivi d'un lien unique vers l'histoire et l'illustration générées.
   - Cliquez sur le lien pour accéder à votre création.

5. **Consultation et partage :**
   - Vous pouvez consulter l'histoire et l'illustration directement dans votre navigateur.
   - Vous pouvez copier le lien unique et le partager avec d'autres personnes pour qu'elles puissent également lire votre création.

**Déconnexion :**

* Cliquez sur le bouton "Se déconnecter" pour vous déconnecter de l'application.

# Documentation Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.53.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.2 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_api_gateway_account.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account) | resource |
| [aws_api_gateway_authorizer.cognito_authorizer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | resource |
| [aws_api_gateway_deployment.api_cors_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | resource |
| [aws_api_gateway_deployment.api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | resource |
| [aws_api_gateway_deployment.status_api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | resource |
| [aws_api_gateway_integration.api_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration.lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration.status_lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration.status_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration_response.get_status_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_integration_response.integration_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_integration_response.integration_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_integration_response.status_options_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_method.api_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method.api_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method.status_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method.status_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method_response.get_status_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_method_response.method_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_method_response.method_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_method_response.status_options_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_api_gateway_resource.api_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | resource |
| [aws_api_gateway_resource.status_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | resource |
| [aws_api_gateway_rest_api.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | resource |
| [aws_cloudfront_distribution.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | resource |
| [aws_cloudfront_response_headers_policy.cors_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_response_headers_policy) | resource |
| [aws_cloudwatch_log_group.api_gw_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cloudwatch_log_group.lambda_ia](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cloudwatch_log_group.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cognito_identity_pool.identity_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool) | resource |
| [aws_cognito_identity_pool_roles_attachment.identity_pool_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool_roles_attachment) | resource |
| [aws_cognito_user.example_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user) | resource |
| [aws_cognito_user_pool.user_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool) | resource |
| [aws_cognito_user_pool_client.user_pool_client](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool_client) | resource |
| [aws_dynamodb_table.task_status](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_policy.lambda_dynamodb_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.lambda_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.api_cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.auth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.lambda_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.status_checker_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.unauth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.auth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.unauth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy_attachment.lambda_dynamodb_access_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.lambda_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_lambda_function.StoryPixAI](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | resource |
| [aws_lambda_function.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | resource |
| [aws_lambda_layer_version.openai_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version) | resource |
| [aws_lambda_permission.api_gateway_permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_lambda_permission.status_api_gateway_permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_acl.storypixai_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_cors_configuration.storypixai_cors](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | resource |
| [aws_s3_bucket_ownership_controls.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_ownership_controls) | resource |
| [aws_s3_bucket_policy.storypixai_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_website_configuration.storypixai_website_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | resource |
| [aws_s3_object.index_html](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | resource |
| [aws_s3_object.script_js](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | resource |
| [null_resource.prepare_lambda_deployment](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [archive_file.StoryPixAI_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [template_file.script_js](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Nom du bucket S3 pour stocker les images | `string` | n/a | yes |
| <a name="input_cognito_auth"></a> [cognito\_auth](#input\_cognito\_auth) | Login et mot de passe pour s'authentifier sur le générateur | `map(string)` | <pre>{<br>  "password": "_Ch4ng3M3_42",<br>  "user": "storypixia"<br>}</pre> | no |
| <a name="input_ia_environment_variables"></a> [ia\_environment\_variables](#input\_ia\_environment\_variables) | Variables d'environnement pour la fonction Lambda | `map(string)` | <pre>{<br>  "ANTHROPIC_STORY_MODEL": "anthropic.claude-3-sonnet-20240229-v1:0",<br>  "META_STORY_MODEL": "meta.llama3-70b-instruct-v1:0",<br>  "MISTRAL_STORY_MODEL": "mistral.mistral-large-2402-v1:0",<br>  "OPENAI_IMAGE_MODEL": "dall-e-3",<br>  "OPENAI_STORY_MODEL": "gpt-4o",<br>  "STABLE_DIFFUSION_IMAGE_MODEL": "stability.stable-diffusion-xl-v1",<br>  "TITAN_IMAGE_MODEL": "amazon.titan-image-generator-v1"<br>}</pre> | no |
| <a name="input_projet"></a> [projet](#input\_projet) | Nom du projet | `string` | `"StoryPixAI"` | no |
| <a name="input_python_runtime"></a> [python\_runtime](#input\_python\_runtime) | Runtime pour la fonction Lambda | `string` | `"python3.10"` | no |
| <a name="input_region"></a> [region](#input\_region) | Région AWS | `string` | `"us-east-1"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudfront"></a> [cloudfront](#output\_cloudfront) | n/a |
| <a name="output_s3_bucket_website_url"></a> [s3\_bucket\_website\_url](#output\_s3\_bucket\_website\_url) | Définition de la sortie pour l'URL du site web hébergé sur S3 |
<!-- END_TF_DOCS -->

# Limitations

StoryPixAI est un projet qui peut présenter certaines limitations :

* **Qualité variable :** La qualité des histoires et des illustrations générées peut varier en fonction des modèles d'IA utilisés, des prompts fournis et d'autres facteurs.
* **Coût :** L'utilisation des API d'OpenAI et Bedrock peut entraîner des coûts, en fonction de votre utilisation.

# Contribuer

StoryPixAI est un projet open source. Si vous souhaitez contribuer, voici quelques pistes :

* **Signaler un bug :** Si vous rencontrez un problème ou un bug, veuillez l'ouvrir dans la section "Issues" du dépôt GitLab.
* **Proposer une amélioration :** Si vous avez une idée d'amélioration ou de nouvelle fonctionnalité, n'hésitez pas à soumettre une "Merge Request" avec votre code.
* **Améliorer la documentation :** Si vous trouvez des erreurs ou des imprécisions dans le README, n'hésitez pas à les corriger.

Merci pour votre intérêt et votre soutien à StoryPixAI !

# Auteur

Julien LE SAUX  
Site web : https://jls42.org  
Email : contact@jls42.org  

# Clause de non-responsabilité

L'utilisation de ce logiciel est à vos propres risques. L'auteur ne peut être tenu responsable des coûts, dommages ou pertes résultant de son utilisation. Veuillez vous assurer de comprendre les frais associés à l'utilisation des services AWS avant de déployer ce projet.

# Licence

Ce projet est sous licence MIT avec Common Clause. Cela signifie que vous êtes libre d'utiliser, de modifier et de distribuer le logiciel, mais vous ne pouvez pas le vendre ou l'utiliser pour offrir des services commerciaux. Pour plus de détails, veuillez consulter le fichier [LICENSE](LICENSE).

# Exemples d'histoires générées

## Français
### Zoé et Tom
![alt text](/img/zoe_tom_fr.png)

## Anglais
### Léa
![alt text](/img/lea_en.png)

## Espagnol
### Roger
![alt text](/img/roger_es.png)

