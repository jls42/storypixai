# Création du bucket S3 pour les images
resource "aws_s3_bucket" "storypixai" {
  bucket = var.bucket_name

  tags = {
    Name = "StoyPixAI"
  }
}

# Configuration de la propriété du bucket S3 pour que le propriétaire du bucket soit également le propriétaire des objets
resource "aws_s3_bucket_ownership_controls" "storypixai" {
  bucket = aws_s3_bucket.storypixai.id

  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

# Configuration du blocage de l'accès public pour le bucket S3
resource "aws_s3_bucket_public_access_block" "storypixai" {
  bucket = aws_s3_bucket.storypixai.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

# Définition des permissions ACL pour le bucket S3 afin de permettre l'accès en lecture publique
resource "aws_s3_bucket_acl" "storypixai_acl" {
  bucket = aws_s3_bucket.storypixai.id
  acl    = "public-read"

  depends_on = [
    aws_s3_bucket_public_access_block.storypixai,
    aws_s3_bucket_ownership_controls.storypixai
  ]
}

# Configuration du site web hébergé sur le bucket S3
resource "aws_s3_bucket_website_configuration" "storypixai_website_config" {
  bucket = aws_s3_bucket.storypixai.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  depends_on = [
    aws_s3_bucket_acl.storypixai_acl
  ]
}

# Définition de la politique du bucket S3 pour permettre l'accès public en lecture
resource "aws_s3_bucket_policy" "storypixai_policy" {
  bucket = aws_s3_bucket.storypixai.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = ["s3:GetObject"],
        Effect    = "Allow",
        Resource  = "arn:aws:s3:::${var.bucket_name}/*",
        Principal = "*"
      },
    ]
  })

  depends_on = [
    aws_s3_bucket_acl.storypixai_acl
  ]
}

# Ajout de la configuration CORS pour le bucket S3
resource "aws_s3_bucket_cors_configuration" "storypixai_cors" {
  bucket = aws_s3_bucket.storypixai.id

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }
}
