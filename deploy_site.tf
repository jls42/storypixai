# Utilisation du fichier de template pour générer le script JavaScript
data "template_file" "script_js" {
  template = file("${path.module}/site/storypixai.js.tpl")

  vars = {
    generate_image_url = "https://${aws_api_gateway_rest_api.api.id}.execute-api.${data.aws_region.current.name}.amazonaws.com/${aws_api_gateway_deployment.api_deployment.stage_name}/generate-image"
    check_status_url   = "https://${aws_api_gateway_rest_api.api.id}.execute-api.${data.aws_region.current.name}.amazonaws.com/${aws_api_gateway_deployment.api_deployment.stage_name}/check-status"
    user_pool_id       = aws_cognito_user_pool.user_pool.id
    client_id          = aws_cognito_user_pool_client.user_pool_client.id
  }
}

resource "aws_s3_object" "index_html" {
  count        = var.deploy_site ? 1 : 0
  bucket       = aws_s3_bucket.storypixai.bucket
  key          = "index.html"
  source       = "${path.module}/site/index.html"
  content_type = "text/html"
  etag         = filemd5("${path.module}/site/index.html")
}

resource "aws_s3_object" "script_js" {
  count        = var.deploy_site ? 1 : 0
  bucket       = aws_s3_bucket.storypixai.bucket
  key          = "storypixai.js"
  content      = data.template_file.script_js.rendered
  content_type = "application/javascript"
  etag         = md5(data.template_file.script_js.rendered)
}
