# Création d'un groupe de logs CloudWatch pour la fonction Lambda StoryPixAI
resource "aws_cloudwatch_log_group" "lambda_ia" {
  name              = "/aws/lambda/${aws_lambda_function.StoryPixAI.function_name}"
  retention_in_days = 1
}

# # Création d'un groupe de logs CloudWatch pour l'API Gateway Welcome
# resource "aws_cloudwatch_log_group" "welcome" {
#   name              = "/aws/apigateway/welcome"
#   retention_in_days = 1
# }

# Création d'un groupe de logs CloudWatch pour la fonction Lambda Status Checker
resource "aws_cloudwatch_log_group" "status_checker" {
  name              = "/aws/lambda/${aws_lambda_function.status_checker.function_name}"
  retention_in_days = 1
}

# Création d'un groupe de logs CloudWatch pour les logs d'exécution de l'API Gateway
resource "aws_cloudwatch_log_group" "api_gw_log_group" {
  name              = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.api.id}/prod"
  retention_in_days = 1
}

# Définition d'un document de politique IAM pour permettre à API Gateway d'assumer un rôle
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

# Création d'un rôle IAM pour permettre à API Gateway de publier des logs dans CloudWatch
resource "aws_iam_role" "api_cloudwatch" {
  name               = "api_gateway_cloudwatch_global"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# Définition d'un document de politique IAM pour autoriser l'accès à CloudWatch Logs
data "aws_iam_policy_document" "cloudwatch" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents",
    ]
    resources = ["*"]
  }
}

# Attachement de la politique CloudWatch au rôle IAM
resource "aws_iam_role_policy" "cloudwatch" {
  name   = "default"
  role   = aws_iam_role.api_cloudwatch.id
  policy = data.aws_iam_policy_document.cloudwatch.json
}
