[[_TOC_]]

# StoryPixAI: Von der KI generierte und illustrierte Gutenachtgeschichten für Kinder

StoryPixAI ist ein innovatives serverloses Projekt zur Erstellung magischer Gutenachtgeschichten, die von KI generiert werden. Geben Sie einfach ein paar Worte zu dem Thema ein, das sie gewählt haben, und lassen Sie die künstliche Intelligenz eine einzigartige und fesselnde Gutenachtgeschichte weben, begleitet von einer niedlichen Illustration. Alles wird automatisch in der gewünschten Sprache generiert, dank der fortschrittlichen KI-Modelle von OpenAI und Amazon Bedrock. Schenken Sie Ihren Kindern einen magischen Moment vor dem Einschlafen, während Sie eine reibungslose Benutzererfahrung dank der Leistungsfähigkeit der serverlosen AWS-Infrastruktur genießen.

## Funktionen

* **Von der KI generierte personalisierte Gutenachtgeschichten:** Verwandeln Sie die Ideen Ihrer Kinder in originelle Gutenachtgeschichten dank der Kraft der KI-Modelle von OpenAI und AWS Bedrock.
* **Niedliche, von der KI generierte Illustrationen:** Jede Geschichte erwacht mit süßen und farbenfrohen Bildern zum Leben, die perfekt dazu geeignet sind, die Fantasie Ihrer Kinder anzuregen.
* **Automatisches Speichern in der AWS-Cloud:** StoryPixAI speichert Ihre Geschichten und Illustrationen automatisch in einem S3-Bucket, der über einen einzigartigen Link jederzeit zugänglich ist.
* **Sicherer Zugang zum Generator:** Die Schnittstelle zur Erstellung von Geschichten ist durch eine Authentifizierung geschützt und gewährleistet so einen privaten und sicheren Kreativraum.
* **Mehrsprachig:** StoryPixAI unterstützt die folgenden Sprachen:
    * Französisch (hauptsächlich getestet)
    * Englisch
    * Spanisch
    * Deutsch
    * Italienisch
    * Portugiesisch

# Architektur

StoryPixAI basiert auf einer serverlosen Architektur:

## Architekturdiagramm der serverlosen Infrastruktur
![alt text](/img/storypixai-architecture.png)

## Frontend

* **Generierungsschnittstelle (`index.html`)**: Die Benutzeroberfläche von StoryPixAI ist eine Single Page Application (SPA), die in HTML, CSS und purem JavaScript (Vanilla JavaScript) entwickelt wurde. Sie bietet eine intuitive und interaktive Benutzererfahrung für:
    ![alt text](/img/generateur.png)

    * **Authentifizierung:** Benutzer müssen sich mit ihren Cognito-Anmeldeinformationen anmelden, um auf die Generierungsfunktionen zuzugreifen. Ein Anmeldedialogfenster wird bereitgestellt, um diesen Prozess zu erleichtern.

    * **Sprachauswahl:** Ein Dropdown-Menü ermöglicht die Auswahl der Sprache des Generators und der Geschichte aus den unterstützten Sprachen: Französisch, Englisch, Spanisch, Deutsch, Italienisch und Portugiesisch. Die ausgewählte Sprache wird für die Benutzeroberfläche und die Generierung der Geschichte verwendet.
        ![alt text](/img/langues.png)
    * **Eingabe des Prompts:** Ein Textfeld ermöglicht es dem Benutzer, die Idee oder das Thema der gewünschten Geschichte einzugeben.

    * **Personalisierung der Generierung (optional):** Die Schnittstelle bietet Optionen zur Personalisierung der Generierung:
        * **Auswahl des Textgenerierungsmodells:** OpenAI GPT-4-o, Anthropic Claude 3 oder Mistral AI.
            ![alt text](/img/texte_modele.png)
        * **Auswahl des Bildgenerierungsmodells:** OpenAI DALL-E 3 oder Stable Diffusion XL.
            ![alt text](/img/image_modele.png)
        * **Spezifische Parameter für das Bildmodell:** Größe, Stil (für Stable Diffusion XL) und Qualität (für DALL-E 3).
            ![alt text](/img/styles.png)
        * **Seed:** Ein numerisches Feld ermöglicht die Angabe eines Wertes zur Initialisierung des Zufallsgenerators.
        * **Aktivierung/Deaktivierung der Bildgenerierung.**

    * **Start der Generierung:** Eine "Generieren"-Schaltfläche startet den Prozess der Geschichte- und Bildgenerierung (falls aktiviert).

    * **Verfolgung des Fortschritts:** Eine Statusmeldung informiert den Benutzer über den Fortschritt der Generierung („Wird verarbeitet...“, „Fertig!“, etc.) und mögliche Fehler.

    * **Anzeige des Ergebnisses:** Ein einzigartiger Link zur generierten Geschichte und dem Bild wird angezeigt, sobald die Generierung abgeschlossen ist. Dieser Link kann kopiert und geteilt werden. * **Authentifizierungs- und Anfrageverwaltung:**
    * Das Skript `storypixai.js` verwaltet die Authentifizierung des Benutzers mit Amazon Cognito und nutzt das erhaltene Zugriffstoken, um Anfragen an das API-Gateway zu autorisieren.
    * Es verwaltet auch die Kommunikation mit den Endpunkten des API-Gateways (`/generate` und `/check_status/{task_id}`), um die Generierung auszulösen und den Fortschritt zu verfolgen.
    * Mechanismen zur Fehlerbehandlung und erneuten Versuchen sind ebenfalls implementiert, um die Zuverlässigkeit der Anwendung zu gewährleisten.

* **Hosting und Verteilung:**
    * Die Generierungsoberfläche (`index.html`) wird auf Amazon S3 gehostet und über Amazon CloudFront, ein Content Delivery Network (CDN), verteilt, das die Leistung und Verfügbarkeit der Anwendung für Benutzer weltweit verbessert.



## Backend

* **AWS Lambda-Funktionen (Python):**
    * **`StoryPixAI`:** Diese Funktion ist das Herzstück der Anwendung. Sie erhält einen vom Benutzer über die Generierungsoberfläche bereitgestellten Texteingabeaufforderung und orchestriert den Erstellungsprozess der Geschichte und Illustration. Sie ruft nacheinander die APIs von OpenAI und/oder Bedrock auf, um den Text der Geschichte und das zugehörige Bild zu generieren. Anschließend speichert sie das Bild im S3-Bucket und sendet dem Benutzer einen eindeutigen Link zur generierten Geschichte und dem Bild. Der Fortschrittsstatus der Generierung wird in DynamoDB gespeichert.

    * **`status_checker`:** Diese Funktion wird periodisch von der Generierungsoberfläche aufgerufen, um den Fortschrittsstatus der Erstellung der Geschichte und des Bildes zu überprüfen. Sie fragt DynamoDB ab, um den Status der laufenden Aufgabe abzurufen, und sendet diesen Status an das Frontend zurück. Dies ermöglicht der Generierungsoberfläche, den Benutzer über den Fortschritt zu informieren und ihm mitzuteilen, wann die Geschichte und das Bild fertig sind.

* **Amazon DynamoDB:** Diese NoSQL-Schlüsselwert-Datenbank wird verwendet, um Informationen zu Aufgaben zur Generierung von Geschichten und Bildern zu speichern. Jede Aufgabe wird durch eine eindeutige `requestId` identifiziert. Die DynamoDB-Tabelle enthält die folgenden Attribute:
    * **`requestId` (Hash-Schlüssel):** Eindeutige Kennung der Aufgabe.
    * **`status`:** Status der Aufgabe (in Bearbeitung, abgeschlossen, Fehler).
    * **`resultUrl`:** URL der generierten Geschichte (bei abgeschlossener Aufgabe).

    Die DynamoDB-Tabelle besitzt auch zwei globale Sekundärindizes:

    * **`StatusIndex`:** Ermöglicht die Suche nach Aufgaben basierend auf ihrem Status.
    * **`ResultUrlIndex`:** Ermöglicht die Suche nach Aufgaben basierend auf der URL der generierten Geschichte.

    Die Lambda-Funktion `status_checker` nutzt diese Indizes, um den Status und die URL der Geschichte einer bestimmten Aufgabe effizient abzurufen. Das JavaScript-Frontend-Skript fragt dann den Endpunkt `/check_status/{task_id}` des API-Gateways ab, um diese Informationen zu erhalten und die Benutzeroberfläche entsprechend zu aktualisieren.

* **Amazon S3:** Dieser Objektspeicherdienst hostet die statische Weboberfläche (`index.html`), die generierten Bilder und die generierten Geschichten. Die Geschichten und Bilder werden mit eindeutigen Schlüsseln, die von der Funktion `StoryPixAI` generiert wurden, gespeichert, und diese Schlüssel werden an den Benutzer zurückgegeben, damit er auf seine Kreationen zugreifen kann.

* **Amazon CloudWatch:** Dieser Überwachungsdienst sammelt die Protokolle und Metriken der Lambda-Funktionen, ermöglicht das Nachverfolgen der Leistung der Anwendung, das Erkennen von Fehlern und das Lösen von Problemen.

# Infrastructure as Code (IaC)

* **Terraform:** Die gesamte AWS-Infrastruktur (Lambda, API Gateway, S3, CloudFront, Cognito, DynamoDB, ...) wird mit Terraform definiert und bereitgestellt. Dies ermöglicht eine automatisierte, reproduzierbare und versionierte Verwaltung der Infrastruktur und erleichtert Aktualisierungen und Änderungen.

# Sicherheit und Authentifizierung

* **Amazon Cognito:** Dieser Dienst verwaltet die Authentifizierung des Benutzers. Die Generierungsoberfläche (`index.html`) verwendet die Cognito-API, um den Benutzer zu authentifizieren und ein Zugriffstoken zu erhalten. Dieser Token wird dann in die Anfragen an die Endpunkte `/generate` und `/check_status/{task_id}` eingefügt, um den Zugriff auf die Funktionen `StoryPixAI` und `status_checker` jeweils zu autorisieren.

# Inhaltsverteilung

* **Amazon CloudFront:** Dieser Content Delivery Network (CDN) Dienst wird verwendet, um die statische Webschnittstelle (`index.html`) schnell und zuverlässig an Nutzer weltweit zu verteilen. Es cached den Inhalt auf geografisch verteilten Servern, wodurch die Latenz reduziert und die Benutzererfahrung verbessert wird.

# KI-Modelle

StoryPixAI ist mit einer Kombination modernster KI-Modelle kompatibel, um hochwertige Geschichten und Illustrationen zu generieren:

* **OpenAI-Modelle:**
    * **Geschichtengenerierung:**
        * OpenAI GPT-4-o
    * **Bildgenerierung:**
        * OpenAI DALL-E 3

* **Modelle zugänglich über AWS Bedrock:**
    * **Geschichtengenerierung:**
        * Anthropic Claude 3 Sonnet (20240229-v1)
        * Mistral AI Mistral Large (2402-v1)
    * **Bildgenerierung:**
        * StabilityAI Stable Diffusion XL v1


# Voraussetzungen

Bevor Sie StoryPixAI verwenden können, stellen Sie sicher, dass Sie die folgenden Elemente haben:

* **AWS-Konto:** Sie benötigen ein aktives AWS-Konto, um die serverlose Infrastruktur zu erstellen und zu verwalten. Stellen Sie sicher, dass Ihr IAM-Benutzer über die folgenden Berechtigungen (oder gleichwertige) verfügt. **Beachten Sie, dass diese Richtlinie ein Beispiel ist und an Ihre spezifischen Sicherheitsanforderungen angepasst werden kann:**

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "cloudwatch:*",
            "ec2:*",
            "lambda:*",
            "s3:*",
            "ssm:*",
            "logs:*",
            "dynamodb:*",
            "cognito-idp:*",
            "cognito-identity:*",
            "apigateway:*",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:AttachRolePolicy",
            "iam:DetachRolePolicy",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:CreatePolicy",
            "iam:DeletePolicy",
            "iam:PassRole",
            "iam:Tag*",
            "iam:Get*",
            "iam:List*",
            "iam:TagRole",
            "kms:TagResource",
            "iam:CreateServiceLinkedRole",
            "budgets:*",
            "events:*"
          ],
          "Resource": "*"
        }
      ]
    }
    ```

* **Aktivierung der Bedrock-Modelle:** Stellen Sie sicher, dass die folgenden KI-Modelle in Ihrem AWS Bedrock Konto aktiviert sind:
    * Anthropic Claude 3 Sonnet (20240229-v1)
    * Mistral AI Mistral Large (2402-v1)
    * StabilityAI Stable Diffusion XL v1

[Zugriff auf Bedrock-Modelle – us-east-1 ](https://us-east-1.console.aws.amazon.com/bedrock/home?region=us-east-1#/modelaccess)
![alt text](/img/modele_1.png)
![alt text](/img/modele_2.png)


* **OpenAI API-Schlüssel (wenn Sie die OpenAI-Modelle verwenden möchten)** Um hochwertige Illustrationen mit DALL-E 3 zu generieren, wird dringend empfohlen, einen gültigen OpenAI API-Schlüssel zu haben. Sie können Ihren Schlüssel auf der OpenAI-Website erhalten. Beachten Sie, dass StoryPixAI auch Bilder mit Stable Diffusion XL v1 (über AWS Bedrock) generieren kann, aber DALL-E 3 bietet in der Regel überlegene Ergebnisse.

**Bereitstellung über GitLab CI/CD:**

* **GitLab-Konto:** Sie benötigen ein gültiges GitLab-Konto und Zugriff auf das StoryPixAI-Projekt auf GitLab.
* **GitLab-Umgebungsvariablen:** Sie müssen die erforderlichen Umgebungsvariablen in Ihrem GitLab-Projekt, einschließlich der AWS- und OpenAI-API-Schlüssel (wenn Sie DALL-E 3 verwenden), konfigurieren.

# Installation und Bereitstellung

## Bereitstellung über GitLab CI/CD (Empfohlen)

StoryPixAI ist so konzipiert, dass es einfach über eine integrierte GitLab CI/CD-Pipeline bereitgestellt und aktualisiert werden kann. Diese Bereitstellungsmethode wird **dringend empfohlen**, da sie automatisiert, zuverlässig und unabhängig von Ihrem Betriebssystem (Linux, macOS, Windows) ist. Die Pipeline automatisiert die Schritte zur Vorbereitung der Umgebung, zur Überprüfung der Konfiguration, zur Bereitstellung und zum Entfernen der Infrastruktur.

**Ausführen der Pipeline:**

1. **Projekt forken:** Erstellen Sie eine Kopie des Projekts in Ihrem eigenen GitLab-Bereich, indem Sie auf die folgende URL gehen: [https://gitlab.com/jls42/storypixai](https://gitlab.com/jls42/storypixai)
2. **Repository klonen:**
   ```bash
   git clone https://gitlab.com/ihr_benutzername/storypixai.git # Ersetzen Sie "ihr_benutzername" durch Ihren GitLab-Benutzernamen
   cd storypixai
   ```
3. **Umgebungsvariablen konfigurieren:**
   - Gehen Sie in die CI/CD-Einstellungen Ihres Projekts.
   - Definieren Sie die folgenden Variablen:
     * `AWS_ACCESS_KEY_ID`: Ihr AWS-Zugangsschlüssel.
     * `AWS_SECRET_ACCESS_KEY`: Ihr AWS-Geheimschlüssel.
     * `OPENAI_KEY` (empfohlen): Ihr OpenAI-API-Schlüssel (wenn Sie DALL-E 3 oder GPT4-o verwenden).
     * `TF_VAR_cognito_auth`: Eine Variable, die die Cognito-Konfiguration im JSON-Format enthält, zum Beispiel:
       ```json
       {"user": "ihr_benutzername", "password": "ihr_sicheres_passwort"}
       ```

4. **Skript `export.sh` anpassen:**
   * Öffnen Sie die Datei `export.sh` und ändern Sie die folgenden Variablen so, dass sie zu Ihrer Konfiguration passen:
      ```bash
      # Der Bucket wird automatisch erstellt, wenn er verfügbar ist; wählen Sie einen eindeutigen Namen
      BUCKET_STATE="ihr_bucket_name_für_den_terraform_status"
      AWS_DEFAULT_REGION="ihre_aws_region"
      ```

5. **Terraform konfigurieren:**
   * Erstellen Sie eine Datei `terraform.tfvars` im Stammverzeichnis des `terraform`-Verzeichnisses und definieren Sie die für Ihre Umgebung spezifischen Variablen (siehe den Abschnitt Terraform-docs-Dokumentation weiter unten für weitere Details):
      ```
      bucket_name = "ihr_s3_bucket_name"

6. **Pipeline auslösen:**
   - Gehen Sie in den CI/CD-Bereich Ihres GitLab-Projekts.
   - Wählen Sie den Schritt, den Sie ausführen möchten (z. B. `Terraform-Überprüfung` oder `Terraform-Bereitstellung`).
   - Klicken Sie auf die Schaltfläche "Play", um den ausgewählten Schritt zu starten.   
    ![alt text](/img/cicd.png)

**Hinweis:** Wenn Sie das mit dem Projekt gelieferte CI/CD verwenden, ist keine Installation von Terraform erforderlich. Die Bereitstellung erfolgt automatisch über GitLab CI/CD, nachdem Sie die erforderlichen Umgebungsvariablen konfiguriert haben.

## Installation auf einem Linux-Rechner (Ubuntu 22.04) (Optional)

Wenn Sie StoryPixAI manuell bereitstellen möchten, können Sie dies auf einem Linux-Rechner tun. **Beachten Sie, dass diese Methode nur auf Ubuntu 22.04 getestet wurde.** Es wird jedoch empfohlen, die Bereitstellungsmethode über GitLab CI/CD zu verwenden, da sie einfacher ist und keine lokale Installation von Terraform erfordert.

1. **Repository klonen:**

   ```bash
   git clone https://gitlab.com/jls42/storypixai.git
   cd storypixai
   ```

2. **Umgebungsvariablen konfigurieren:**

   * **AWS:**
        * Führen Sie `aws configure` aus und folgen Sie den Anweisungen, um Ihre AWS-Anmeldeinformationen (Zugangs- und Geheimschlüssel) und die Standardregion zu konfigurieren.
   * **OpenAI (Optional):**
        * Führen Sie `source export.sh` aus, um die Funktionen des Skripts zu laden.
        * Führen Sie `manage_openai_key put <ihr_openai_schlüssel>` aus, um Ihren OpenAI-Schlüssel im SSM Parameter Store zu speichern.

## Bereitstellung über Terraform

1. **AWS-Umgebung vorbereiten:**
    * Folgen Sie den gleichen Schritten wie oben zur Konfiguration der AWS-Umgebungsvariablen.

2. **Skript `export.sh` anpassen:**
   * Öffnen Sie die Datei `export.sh` und ändern Sie die folgenden Variablen so, dass sie zu Ihrer Konfiguration passen:
      ```bash
      # Der Bucket wird automatisch erstellt, wenn er verfügbar ist; wählen Sie einen eindeutigen Namen
      BUCKET_STATE="ihr_bucket_name_für_den_terraform_status"
      AWS_DEFAULT_REGION="ihre_aws_region"
      ``` **Terraform konfigurieren:**
   * Erstellen Sie eine Datei `terraform.tfvars` im Stammverzeichnis des `terraform`-Verzeichnisses und definieren Sie die spezifischen Variablen für Ihre Umgebung, insbesondere (siehe den Abschnitt "Terraform-docs Dokumentation" weiter unten für weitere Details):
      ```
      bucket_name = "ihr_s3_bucket_name"
      cognito_auth = {
        user     = "ihr_benutzername" 
        password = "ihr_sicheres_passwort"
      }
      ```

4. **Die Infrastruktur bereitstellen:**
   ```bash
   source export.sh
   terraform_plan
   terraform_apply
   ```

# Nutzung

1. **Authentifizierung:**
   - Öffnen Sie Ihren Webbrowser und gehen Sie zur URL Ihrer StoryPixAI-Anwendung.
   - Melden Sie sich mit dem Benutzernamen und dem Passwort an, das Sie in der Variable `cognito_auth` während der Bereitstellung definiert haben.

2. **Erstellung einer Geschichte:**
   - Sobald Sie angemeldet sind, gelangen Sie zur Schnittstelle zur Geschichtenerstellung.

   - **Wählen Sie die Sprache:** Wählen Sie die gewünschte Sprache im Dropdown-Menü oben rechts auf dem Bildschirm. Die verfügbaren Optionen sind: Französisch, Englisch, Spanisch, Deutsch, Italienisch und Portugiesisch.  

    
   - **Geben Sie Ihre Geschichtenidee ein:** Geben Sie ein paar Wörter oder einen Satz in das dafür vorgesehene Textfeld ein, die die Geschichte inspirieren sollen. Sie können Namen, Orte, Charaktere oder andere relevante Elemente einfügen.
   - **Personalisieren Sie die Generierung (optional):**
      - **Geschichtenmodell:** Wählen Sie aus den verfügbaren Modellen (OpenAI GPT-4-o, Anthropic Claude 3, Mistral AI).  
      - **Bildmodell:** Wählen Sie zwischen OpenAI DALL-E 3 oder Stable Diffusion XL.  
      - **Bildgröße:** Wählen Sie die gewünschte Größe für die Illustration (die verfügbaren Optionen hängen vom gewählten Bildmodell ab).
      - **Bildstil:** Wählen Sie einen Stil aus der vorgeschlagenen Liste, um die Ästhetik der Illustration zu personalisieren (nur für Stable Diffusion XL).  
      - **Bildqualität:** Wählen Sie die Qualität des von DALL-E 3 generierten Bildes (Standard oder HD).
      - **Seed:** Geben Sie eine Ganzzahl ein, um den Zufallsgenerator zu initialisieren und unterschiedliche Ergebnisse bei jeder Generierung zu erhalten.
      - **Bildgenerierung aktivieren/deaktivieren:** Aktivieren oder deaktivieren Sie das Kontrollkästchen, um die Generierung einer Illustration für die Geschichte zu aktivieren oder zu deaktivieren.
   - **Starten Sie die Generierung:** Klicken Sie auf die Schaltfläche "Generieren".

3. **Überwachung des Fortschritts:**
   - Die Benutzeroberfläche zeigt die Meldung "Wird bearbeitet..." an.

4. **Zugriff auf die generierte Geschichte:**
   - Sobald die Generierung abgeschlossen ist, wird die Meldung "Fertig!" angezeigt, gefolgt von einem eindeutigen Link zur generierten Geschichte und Illustration.
   - Klicken Sie auf den Link, um auf Ihre Kreation zuzugreifen.

5. **Ansehen und Teilen:**
   - Sie können die Geschichte und die Illustration direkt in Ihrem Browser ansehen.
   - Sie können den eindeutigen Link kopieren und mit anderen teilen, damit diese Ihre Kreation ebenfalls lesen können.

**Abmeldung:**

* Klicken Sie auf die Schaltfläche "Abmelden", um sich von der Anwendung abzumelden.

# Terraform-docs Dokumentation
<!-- BEGIN_TF_DOCS -->
## Anforderungen

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Anbieter

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.53.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.2 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |

## Module

Keine Module.

## Ressourcen | Name | Typ |
|------|------|
| [aws_api_gateway_account.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account) | Ressource |
| [aws_api_gateway_authorizer.cognito_authorizer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | Ressource |
| [aws_api_gateway_deployment.api_cors_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | Ressource |
| [aws_api_gateway_deployment.api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | Ressource |
| [aws_api_gateway_deployment.status_api_deployment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment) | Ressource |
| [aws_api_gateway_integration.api_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | Ressource |
| [aws_api_gateway_integration.lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | Ressource |
| [aws_api_gateway_integration.status_lambda_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | Ressource |
| [aws_api_gateway_integration.status_options_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | Ressource |
| [aws_api_gateway_integration_response.get_status_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | Ressource |
| [aws_api_gateway_integration_response.integration_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | Ressource |
| [aws_api_gateway_integration_response.integration_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | Ressource |
| [aws_api_gateway_integration_response.status_options_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | Ressource |
| [aws_api_gateway_method.api_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | Ressource |
| [aws_api_gateway_method.api_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | Ressource |
| [aws_api_gateway_method.status_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | Ressource |
| [aws_api_gateway_method.status_options_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | Ressource |
| [aws_api_gateway_method_response.get_status_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | Ressource |
| [aws_api_gateway_method_response.method_response_202](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | Ressource |
| [aws_api_gateway_method_response.method_response_options_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | Ressource |
| [aws_api_gateway_method_response.status_options_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | Ressource |
| [aws_api_gateway_resource.api_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | Ressource |
| [aws_api_gateway_resource.status_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | Ressource |
| [aws_api_gateway_rest_api.api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | Ressource | n.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | Ressource |
| [aws_cloudfront_response_headers_policy.cors_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_response_headers_policy) | Ressource |
| [aws_cloudwatch_log_group.api_gw_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | Ressource |
| [aws_cloudwatch_log_group.lambda_ia](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | Ressource |
| [aws_cloudwatch_log_group.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | Ressource |
| [aws_cognito_identity_pool.identity_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool) | Ressource |
| [aws_cognito_identity_pool_roles_attachment.identity_pool_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_identity_pool_roles_attachment) | Ressource |
| [aws_cognito_user.example_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user) | Ressource |
| [aws_cognito_user_pool.user_pool](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool) | Ressource |
| [aws_cognito_user_pool_client.user_pool_client](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool_client) | Ressource |
| [aws_dynamodb_table.task_status](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | Ressource |
| [aws_iam_policy.lambda_dynamodb_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | Ressource |
| [aws_iam_policy.lambda_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | Ressource |
| [aws_iam_role.api_cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | Ressource |
| [aws_iam_role.auth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | Ressource |
| [aws_iam_role.lambda_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | Ressource |
| [aws_iam_role.status_checker_exec_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | Ressource |
| [aws_iam_role.unauth_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | Ressource |
| [aws_iam_role_policy.auth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | Ressource |
| [aws_iam_role_policy.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | Ressource |
| [aws_iam_role_policy.unauth_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | Ressource |
| [aws_iam_role_policy_attachment.lambda_dynamodb_access_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | Ressource |
| [aws_iam_role_policy_attachment.lambda_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | Ressource |
| [aws_lambda_function.StoryPixAI](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | Ressource |
| [aws_lambda_function.status_checker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | Ressource |
| [aws_lambda_layer_version.openai_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version) | Ressource |
| [aws_lambda_permission.api_gateway_permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | Ressource |
| [aws_lambda_permission.status_api_gateway_permission] https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | Ressource |
| [aws_s3_bucket.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | Ressource |
| [aws_s3_bucket_acl.storypixai_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | Ressource |
| [aws_s3_bucket_cors_configuration.storypixai_cors](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | Ressource |
| [aws_s3_bucket_ownership_controls.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_ownership_controls) | Ressource |
| [aws_s3_bucket_policy.storypixai_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | Ressource |
| [aws_s3_bucket_public_access_block.storypixai](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | Ressource |
| [aws_s3_bucket_website_configuration.storypixai_website_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | Ressource |
| [aws_s3_object.index_html](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | Ressource |
| [aws_s3_object.script_js](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object) | Ressource |
| [null_resource.prepare_lambda_deployment](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | Ressource |
| [archive_file.StoryPixAI_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | Datenquelle |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | Datenquelle |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | Datenquelle |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | Datenquelle |
| [aws_iam_policy_document.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | Datenquelle |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | Datenquelle |
| [template_file.script_js](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | Datenquelle |

## Eingaben

| Name | Beschreibung | Typ | Standardwert | Erforderlich |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Name des S3-Buckets zum Speichern der Bilder | `string` | n/a | ja |
| <a name="input_cognito_auth"></a> [cognito\_auth](#input\_cognito\_auth) | Login und Passwort zur Authentifizierung beim Generator | `map(string)` | <pre>{<br>  "password": "_Ch4ng3M3_42",<br>  "user": "storypixia"<br>}</pre> | nein |
| <a name="input_ia_environment_variables"></a> [ia\_environment\_variables](#input\_ia\_environment\_variables) | Umgebung Variablen für die Lambda-Funktion | `map(string)` | <pre>{<br>  "ANTHROPIC_STORY_MODEL": "anthropic.claude-3-sonnet-20240229-v1:0",<br>  "META_STORY_MODEL": "meta.llama3-70b-instruct-v1:0",<br>  "MISTRAL_STORY_MODEL": "mistral.mistral-large-2402-v1:0",<br>  "OPENAI_IMAGE_MODEL": "dall-e-3",<br>  "OPENAI_STORY_MODEL": "gpt-4o",<br>  "STABLE_DIFFUSION_IMAGE_MODEL": "stability.stable-diffusion-xl-v1",<br>  "TITAN_IMAGE_MODEL": "amazon.titan-image-generator-v1"<br>}</pre> | nein |
| <a name="input_projet"></a> [projet](#input\_projet) | Projektname | `string` | `"StoryPixAI"` | nein |
| <a name="input_python_runtime"></a> [python\_runtime](#input\_python\_runtime) | Laufzeit für die Lambda-Funktion | `string` | `"python3.10"` | nein |
| <a name="input_region"></a> [region](#input\_region) | AWS-Region | `string` | `"us-east-1"` | nein |

## Ausgaben | Name | Description |
|------|-------------|
| <a name="output_cloudfront"></a> [cloudfront](#output\_cloudfront) | k. A. |
| <a name="output_s3_bucket_website_url"></a> [s3\_bucket\_website\_url](#output\_s3\_bucket\_website\_url) | Definition des Outputs für die URL der auf S3 gehosteten Website |
<!-- END_TF_DOCS -->

# Einschränkungen

StoryPixAI ist ein Projekt, das einige Einschränkungen aufweisen kann:

* **Variierende Qualität:** Die Qualität der generierten Geschichten und Illustrationen kann je nach verwendeten KI-Modellen, bereitgestellten Eingaben und anderen Faktoren variieren.
* **Kosten:** Die Nutzung der APIs von OpenAI und Bedrock kann je nach Nutzung Kosten verursachen.

# Beitrag leisten

StoryPixAI ist ein Open-Source-Projekt. Wenn Sie beitragen möchten, hier sind einige Möglichkeiten:

* **Fehler melden:** Wenn Sie ein Problem oder einen Fehler finden, melden Sie ihn bitte im "Issues"-Bereich des GitLab-Repositorys.
* **Verbesserung vorschlagen:** Wenn Sie eine Verbesserungsidee oder eine neue Funktion vorschlagen möchten, reichen Sie bitte eine "Merge Request" mit Ihrem Code ein.
* **Dokumentation verbessern:** Wenn Sie Fehler oder Unklarheiten im README finden, korrigieren Sie diese bitte.

Vielen Dank für Ihr Interesse und Ihre Unterstützung für StoryPixAI!

# Autor

Julien LE SAUX  
Website: https://jls42.org  
E-Mail: contact@jls42.org  

# Haftungsausschluss

Die Nutzung dieser Software erfolgt auf eigenes Risiko. Der Autor haftet nicht für Kosten, Schäden oder Verluste, die sich aus der Nutzung ergeben. Bitte stellen Sie sicher, dass Sie die mit der Nutzung der AWS-Dienste verbundenen Gebühren verstehen, bevor Sie dieses Projekt bereitstellen.

# Lizenz

Dieses Projekt steht unter der MIT-Lizenz mit Common Clause. Dies bedeutet, dass Sie die Software frei verwenden, ändern und verbreiten können, jedoch nicht verkaufen oder für kommerzielle Dienstleistungen nutzen dürfen. Weitere Details finden Sie in der Datei [LICENSE](LICENSE).

# Beispiele generierter Geschichten

## Französisch
### Zoé und Tom
![alt text](/img/zoe_tom_fr.png)

## Englisch
### Léa
![alt text](/img/lea_en.png)

## Spanisch
### Roger
![alt text](/img/roger_es.png)

**Dieses Dokument wurde von der Version fr in die Sprache de unter Nutzung des Modells gpt-4o übersetzt. Für weitere Informationen zum Übersetzungsprozess, besuchen Sie https://gitlab.com/jls42/ai-powered-markdown-translator**

